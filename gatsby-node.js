const path = require("path");

exports.onCreateWebpackConfig = ({ actions }) => {
	actions.setWebpackConfig({
		// module: {
		// 	rules: [
		// 		{
		// 			test: /\.html$/,
		// 			loader: require.resolve("html-loader"),
		// 			options: {
		// 				minimize: false,
		// 			},
		// 		},
		// 	],
		// },
		resolve: {
			alias: {
				"@src": path.resolve(__dirname, "src"),
				"@seo": path.resolve(__dirname, "src/seo"),
				"@query": path.resolve(__dirname, "src/query"),
				"@layout": path.resolve(__dirname, "src/layout"),
				"@components": path.resolve(__dirname, "src/components"),
				"@containers": path.resolve(__dirname, "src/containers"),
				"@modules": path.resolve(__dirname, "src/modules"),
				"@views": path.resolve(__dirname, "src/views"),
				"@assets": path.resolve(__dirname, "src/assets/images"),
				"@documents": path.resolve(__dirname, "src/assets/documents"),
				"@functions": path.resolve(__dirname, "src/functions"),
				"@storage": path.resolve(__dirname, "src/storage"),
				"@services": path.resolve(__dirname, "src/services"),
				"@constants": path.resolve(__dirname, "src/constants"),
				"@hooks": path.resolve(__dirname, "src/hooks"),
				"@utils": path.resolve(__dirname, "src/utils"),
				"@lang": path.resolve(__dirname, "src/lang"),
			},
		},
	});
};
