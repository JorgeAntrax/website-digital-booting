const dotenv = require("dotenv");
const path = require("path");
dotenv.config({
	path: path.join(__dirname, `.env.${process.env.NODE_ENV}`),
});

module.exports = {
	trailingSlash: "never",
	pathPrefix: process.env.REACT_APP_ROUTER_BASE || "",
	flags: {
		DEV_SSR: true,
	},
	siteMetadata: {
		typeContent: "website",
		title: `Agencia Creativa de Marketing Digital en México | Digital Booting`,
		agencyName:
			"Agencia Creativa de Marketing Digital en México | Digital Booting",
		description:
			"Digital Booting es una agencia creativa de marketing digital líder en México que aprovecha el poder de la inteligencia artificial para ofrecer soluciones innovadoras y mejores resultados para nuestros clientes.",
		siteUrl: `https://digitalbooting.com`,
		author: "Digital Booting LLC",
		image: `src/assets/icon.png`,
		keywords: [
			"Agencia Digital",
			"Inteligencia Artificial",
			"Soluciones Innovadoras",
			"México",
			"Creatividad",
			"Negocio",
			"Crecimiento",
			"Líder",
			"Posicionamiento",
			"Marketing Digital",
			"Marketing Emocional",
			"Marketing Inmersivo",
			"Marketing Social",
			"Marketing Experimental",
			"Marketing de analisis predictivo",
			"IA en Marketing",
			"Diseño gráfico",
			"Diseño web",
			"Automaticación de marketing",
			"Marketing de inteligencia artificial conversacional",
			"Marketing de inteligencia artificial creativa",
			"Marketing de neuromarketing",
			"Marketing de personalización avanzada",
			"Marketing de investigacion de mercado",
			"Marketing de bigdata",
		],
	},
	plugins: [
		"gatsby-plugin-sass",
		"gatsby-plugin-no-sourcemaps",
		"gatsby-plugin-styled-components",
		"gatsby-plugin-sitemap",
		{
			resolve: "gatsby-plugin-manifest",
			options: {
				icon: "src/assets/icon.png",
				display: "minimal-ui",
				start_url: "/",
				short_name: "Digital Booting",
				name: "Digital Booting",
				background_color: "#141D28",
				theme_color: "#02f8c0",
			},
		},
		{
			resolve: `gatsby-plugin-clarity`,
			options: {
			  // Valor de cadena para el identificador del proyecto de Clarity
			  clarity_project_id: 'puz24hi00g',
			  // Valor booleano para permitir Clarity durante el desarrollo
			  // true habilitará el código de seguimiento de Clarity en entornos de desarrollo y producción
			  // false habilitará el código de seguimiento de Clarity solo en entornos de producción
			  enable_on_dev_env: true
			},
		},
		{
			resolve: `gatsby-plugin-google-gtag`,
			options: {
				trackingIds: ["G-3HBQR421LY"],
				pluginConfig: {
					head: true,
					respectDNT: true,
					exclude: ["/gracias-por-contactarnos"],
				},
			},
		},
		{
			resolve: "gatsby-plugin-preconnect",
			options: {
				domains: ["https://www.google-analitics.com"],
			},
		},
		{
			resolve: "gatsby-plugin-remove-console",
			options: {
				exclude: ["error"],
			},
		},
		{
			resolve: "gatsby-plugin-robots-txt",
			options: {
				host: "https://digitalbooting.com",
				sitemap: "https://digitalbooting.com/sitemap.xml",
				policy: [{ userAgent: "*", allow: "/" }],
			},
		},
		{
			resolve: "gatsby-plugin-htaccess",
			options: {
				https: true,
				www: false,
				SymLinksIfOwnerMatch: true,
				host: "digitalbooting.com",
				ErrorDocument: `
				ErrorDocument 401 /error_pages/401.html
				ErrorDocument 404 /error_pages/404.html
				ErrorDocument 500 /error_pages/500.html
				`,
				custom: `
					# This is a custom rule!
					# This is a another custom rule!
					Redirect 301 /blog https://digitalbooting.com/blog-de-marketing-digital
					Redirect 301 /blog/beneficios-del-desarrollo-web-en-marketing-digital https://digitalbooting.com/blog-de-marketing-digital/beneficios-del-desarrollo-web-en-marketing-digital
					Redirect 301 /programa-afiliados https://digitalbooting.com/gana-comisiones-con-nuestro-programa-de-afiliados
					Redirect 301 /agencia-disenio-web https://digitalbooting.com/agencia-de-paginas-web
					Redirect 301 /branding-digital https://digitalbooting.com/creacion-de-marca-con-ia
					Redirect 301 /posicionamiento-web-seo https://digitalbooting.com/seo-con-inteligencia-artificial
					Redirect 301 /social-media https://digitalbooting.com/marketing-en-redes-sociales
			  `,
			},
		},
	],
};
