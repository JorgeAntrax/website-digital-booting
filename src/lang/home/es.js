const es = {
	labelShop: "COMPRA CON NOSOTROS",
	title: "NUEVOS PRODUCTOS TODOS LOS DÍAS",
	smallAbout:
		"Somos una pequeña empresa independiente creada por 2 mujeres que buscan romper los estereotipos de la moda femenina, trabajamos con las mejores marcas para traerte productos de la más alta calidad, directamente desde El Paso TX.",
	fashion: "Moda para toda Mujer Empoderada",
	moreanca: "más acerca de nosotros",
	shopcategory: "COMPRA POR CATEGORIA",
	offLabel: "OBTEN EL 50% DE DESCUENTO",
	offTitle: "NUEVA COLECCIÓN",
	offDate: "OTOÑO 2022",
	featuredcollections: "COLECCIONES DESTACADAS",
	viewallcollections: "ver todas las colecciones",
};

export default es;
