const en = {
	labelShop: "SHOP WITH US",
	title: "NEW ARRIVALS EVERY DAY",
	smallAbout: `We are a small independent company created by 2
	women looking to break the stereotypes of women's
	fashion, we work with the best brands to bring you
	the highest quality product, directly from El Paso
	TX.`,
	fashion: "Fashion for Empowered Woman",
	moreanca: "more about us",
	shopcategory: "SHOP BY CATEGORY",
	offLabel: "GET EXTRA 50% OFF",
	offTitle: "NEW COLLECTION",
	offDate: "FALL 2022",
	featuredcollections: "OUTSTANDING COLLECTIONS",
	viewallcollections: "view all collections",
};

export default en;
