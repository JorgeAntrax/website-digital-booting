const en = {
	addCart: "Ups! We have not been able to add this product to the cart",
	deleteCart: "Ups! We have not been able to eliminate this product from your purchase cart",
	addAllCart: "Ups! We have not been able to add your products to the purchase cart",
	clearCart: "We have not been able to clean the shopping cart",
	allCartLovelist:
		"We have not been able to add the products to the shopping cart",
	deleteLovelist:
		"We have not been able to clean this product from your favorite list",
	clearLovelist:
		"We have not been able to clean your favorite list, please try it later",
	loadcatalog: "An error has occurred when loading the catalog products",
	addLovelist: "We have not been able to add this product to favorites",
	suscribe: "An error has happened when you subscribe"
};

export default en;
