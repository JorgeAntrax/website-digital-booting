const es = {
	addCart: "Ups! No hemos podido agregar este producto al carrito",
	deleteCart:
		"Ups! No hemos podido eliminar este producto de tu carrito de compra",
	addAllCart:
		"Ups! No hemos podido agregar tus productos al carrito de compra",
	clearCart: "No hemos podido limpiar el carrito de compras",
	allCartLovelist:
		"No hemos podido agregar los productos al carrito de compras",
	deleteLovelist:
		"No hemos podido limpiar este producto de tu lista de favoritos",
	clearLovelist:
		"No hemos podido limpiar tu lista de favoritos, porfavor intentalo más tarde",
	loadcatalog: "Ha ocurrido un error al cargar los productos del catalogo",
	addLovelist: "No hemos podido agregar este producto a favoritos",
	suscribe: "Ha ocurrido un error al suscribirte"
};

export default es;
