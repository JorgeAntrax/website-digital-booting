import home_es from "./home/es";
import home_en from "./home/en";
import global_es from "./global/es";
import global_en from "./global/en";
import error_es from "./error/es";
import error_en from "./error/en";

const translators = {
	error: {
		es: error_es,
		en: error_en,
	},
	global: {
		es: global_es,
		en: global_en,
	},
	home: {
		es: home_es,
		en: home_en,
	},
};

export default translators;
