import { useStorage } from "@storage/store";
import translators from "./config";

const useTranslate = () => {
	const { lang, updateStore } = useStorage();

	const trans = (path) => {
		const [view, key] = path.split(".");
		return translators[view][lang][key];
	};

	const setLang = (lang) => {
		updateStore('lang', lang);
	}

	return { lang, setLang, trans };
};

export default useTranslate;
