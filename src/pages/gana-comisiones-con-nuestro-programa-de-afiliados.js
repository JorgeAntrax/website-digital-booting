import React, { useState, useEffect, memo } from "react";
import { Layout } from "@layout";
import {
	Box,
	Button,
	Center,
	Col,
	Flex,
	Seo,
	Text,
	Input,
	Image,
	Container,
	Svg,
} from "@components";
import { MarketingExpert } from "@containers/socios";

import { Banner, Card, SubTitle } from "@modules";
import useTranslate from "@lang";
import { socios } from "@seo";

import { Links } from "@constants";
import { createAfiliado } from "@services";
import { navigate } from "@utils";

import check from "@assets/check.svg";
import uncheck from "@assets/uncheck.svg";

import ingresos from "@assets/services/ingresos.png";
import inversion from "@assets/services/inversion.png";
import leads from "@assets/services/leads.png";
import ingresos2 from "@assets/services/ingresos2.png";
import asset from "@assets/programa2.jpg";

const defaultState = {
	name: "",
	lname: "",
	phone: "",
	email: "",
	password: "",
};

const defaultStateValidations = {
	number: false,
	mayuscula: false,
	minuscula: false,
	simbolo: false,
};

const basesPrograma = [
	{
		theme: "white",
		icon: leads,
		title: "Sin limite de alcance",
		description:
			"Llega a millones de personas compartiendo nuestras soluciones utilizando cualquier medio o plataforma digital como tus  redes sociales, páginas web, blogs, canales de youtube, Spotify, TikTok, etc.",
	},
	{
		theme: "dark",
		icon: inversion,
		title: "Sin inversión inicial",
		description:
			"No necesitas inversión inicial, te capacitaremos para crear estrategias de marketing orgánicas, recomendando nuestras soluciones digitales utilizando tu link único de asociado.",
	},
	{
		theme: "white",
		icon: ingresos,
		title: "Aumenta tus ingresos",
		description:
			"Con cada solución contratada tu ganas de un 5% a un 20% de comisión sobre el total facturado, entre mejor calidad tenga tu referido y dependiendo del servicio que contraté con nosotros, tu comisión es mayor.",
	},
];

//aplicar redireccion 301
const Page = () => {
	const { trans } = useTranslate();

	const [error, setError] = useState("");
	const [data, setData] = useState(defaultState);
	const [validations, setValidations] = useState(defaultStateValidations);

	const update = (key, value) => {
		setData({
			...data,
			[key]: value,
		});
	};

	useEffect(() => {
		let number = false;
		let mayuscula = false;
		let minuscula = false;
		let simbolo = false;

		if (/[0-9]/g.test(data.password)) {
			number = true;
		}
		if (/[A-Z]/g.test(data.password)) {
			mayuscula = true;
		}
		if (/[a-z]/g.test(data.password)) {
			minuscula = true;
		}
		if (/[#$@!*.+]/g.test(data.password)) {
			simbolo = true;
		}

		setValidations({
			...validations,
			number,
			mayuscula,
			minuscula,
			simbolo,
		});
	}, [data.password]);

	const handleRegisterAsociado = async () => {
		setError("");
		if (
			!data.name ||
			!data.lname ||
			!data.phone ||
			!data.email ||
			(!validations.number &&
				!validations.mayuscula &&
				!validations.minuscula &&
				!validations.simbolo)
		) {
			setError("Todos los campos son requeridos");
			return;
		}

		const request = {
			name: data.name,
			lname: data.lname,
			phone: data.phone,
			email: data.email,
			passwd: data.password,
			passwdc: data.password,
		};

		const [response, error] = await createAfiliado(request);

		if (error) {
			setError(
				"Ha ocurrido un error, no hemos podido completar tu registro al programa, porfavor vuelve a intentarlo."
			);
			return;
		}

		setError("");
		navigate(Links.$thanks);
	};

	return (
		<Layout>
			<Banner
				title="aumenta tus ingresos"
				description={
					<Text>
						Únete a nuestro{" "}
						<Text type="strong" color="context">
							programa de asociados
						</Text>{" "}
						y gana un porcentaje de cada uno de tus referidos que
						contrate{" "}
						<Text type="strong">nuestras soluciones digitales</Text>
						.
					</Text>
				}
				handleText="Quiero unirme al programa"
				url="#registroAsociados"
			/>

			<SubTitle items="center" mb={50} ph={20}>
				Conoce las bases del programa
			</SubTitle>

			<Center flexWrap="wrap" mb={50}>
				{basesPrograma.map((base, index) => (
					<Col width="100%" padding={20} key={index} mdMaxw={400}>
						<Card {...base} />
					</Col>
				))}
			</Center>

			<MarketingExpert />

			<Center mt={50} id="registroAsociados">
				<SubTitle items="center" mb={30} mdMaxw={600} ph={20}>
					Únete a nuestro programa de asociados
				</SubTitle>
			</Center>

			<Center>
				<Text align="center" mdMaxw={800} ph={20}>
					Forma parte de nuestros{" "}
					<Text type="strong">más de 1000 asociados</Text> que están
					generando las mejores comisiones por promocionar nuestras
					soluciones digitales
				</Text>
			</Center>

			<Container mv={50}>
				<Center width="100%" ph={20}>
					<Box
						flex={1}
						mdMaxw={800}
						xsPadding={20}
						mdPadding={40}
						boxShadow="0 6px 30px -8px rgba(0,0,0,0.1)"
					>
						<Center mb={10}>
							<Image url={ingresos2} dimentions={80} />
						</Center>

						<Text size={22} align="center" weight="400" xsMb={20}>
							Aumenta tus ingresos ya mismo
						</Text>

						<Flex flexWrap="wrap">
							<Col xsWidth="100%" mdWidth="50%" padding="0.5rem">
								<Input
									placeholder="Nombre(s)"
									value={data.name}
									getValue={(v) => update("name", v)}
								/>
							</Col>
							<Col xsWidth="100%" mdWidth="50%" padding="0.5rem">
								<Input
									placeholder="Apellido(s)"
									value={data.lname}
									getValue={(v) => update("lname", v)}
								/>
							</Col>
						</Flex>
						<Flex flexWrap="wrap">
							<Col xsWidth="100%" mdWidth="50%" padding="0.5rem">
								<Input
									onlyPhone
									placeholder="Tu celular"
									value={data.phone}
									getValue={(v) => update("phone", v)}
								/>
							</Col>
							<Col xsWidth="100%" mdWidth="50%" padding="0.5rem">
								<Input
									type="email"
									placeholder="Tu correo electrónico"
									value={data.email}
									getValue={(v) => update("email", v)}
								/>
							</Col>
						</Flex>
						<Flex>
							<Col xsWidth="100%" mdWidth="50%" padding="0.5rem">
								<Input
									type="password"
									placeholder="Tu contraseña"
									value={data.password}
									getValue={(v) => update("password", v)}
								/>
							</Col>
						</Flex>

						<Col padding="0.5rem">
							<Text display="flex" items="center" size={14}>
								<Svg
									icon={validations.number ? check : uncheck}
									wsvg={14}
									mr={10}
								/>
								Un número
							</Text>
							<Text display="flex" items="center" size={14}>
								<Svg
									icon={
										validations.mayuscula ? check : uncheck
									}
									wsvg={14}
									mr={10}
								/>
								Una mayuscula
							</Text>
							<Text display="flex" items="center" size={14}>
								<Svg
									icon={
										validations.minuscula ? check : uncheck
									}
									wsvg={14}
									mr={10}
								/>
								Una minuscula
							</Text>
							<Text display="flex" items="center" size={14}>
								<Svg
									icon={validations.simbolo ? check : uncheck}
									wsvg={14}
									mr={10}
								/>
								Un simbolo especial (#$@!*.+)
							</Text>
						</Col>

						{error && (
							<Text size={14} mt={10} align="center" color="red">
								{error}
							</Text>
						)}

						<Center mt={30}>
							<Button
								theme="context"
								onClick={() => handleRegisterAsociado()}
							>
								Registrarme como asociado
							</Button>
						</Center>
					</Box>
				</Center>
			</Container>

			<Center ph={20}>
				<Text mdMaxw={1000} align="center" color="grey" xsSize={12} mdSize={14}>
					Digital Booting entiende tu preocupación por el modo en que
					se utiliza y se comparte tu información personal, y te
					aseguramos que siempre trataremos tus datos de forma
					confidencial, no compartiremos tu información con terceras
					personas, tranquilo! tu privacidad es lo más importante para
					nosotros.
				</Text>
			</Center>

			<Center mb={50}>
				<Image url={asset} mdMaxw={900} />
			</Center>
		</Layout>
	);
};

export const Head = () => <Seo {...socios} />;
export default memo(Page);
