import React, { useEffect } from "react";
import { navigate } from "@utils";

const ResetArtisan = () => {
	useEffect(() => {
		navigate("/");
	}, []);
	return null;
};

export default ResetArtisan;
