import React, { useEffect, useState, memo } from "react";
import { Layout } from "@layout";
import { Seo } from "@components";

import { TemplateSolution } from "@modules";
import useTranslate from "@lang";
import { web } from "@seo";

import { WebDesign } from "@src/solutions";

//aplicar redireccion 301
const Page = () => {
	const { trans } = useTranslate();
	const [page, setPage] = useState(null);

	useEffect(() => {
		WebDesign && setPage(WebDesign);
	}, []);
	return (
		<Layout>
			<TemplateSolution data={page} />
		</Layout>
	);
};

export const Head = () => <Seo {...web} />;
export default memo(Page);
