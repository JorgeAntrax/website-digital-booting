import React, { memo } from "react";
import { Layout } from "@layout";
import { Text, Seo, Flex, Col } from "@components";

import { Banner } from "@modules";
import useTranslate from "@lang";
import { privacy } from "@seo";

const Page = () => {
	const { trans } = useTranslate();
	return (
		<Layout>
			<Banner
				height={300}
				title="Aviso de privacidad"
				description="Digital Booting entiende tu preocupación por el modo en que se utiliza y se comparte tu información personal, puedes estar tranquilo en que no difundiremos tus datos."
			/>

			<Flex justify="center">
				<Col pv={50} ph={20} mdMaxw={1000}>
					<Text mb={20} align="justify">
						En cumplimiento con lo establecido por la Ley Federal de
						Transparencia y Acceso a la Información Pública
						Gubernamental, su reglamento, así como lo señalado en
						los Lineamientos de Protección de Datos Personales, se
						establece el siguiente compromiso:
					</Text>
					<Text mb={20} align="justify">
						Los datos personales que se ingresen en el formulario de
						contacto, no serán difundidos, distribuidos o
						comercializados. Únicamente podrán ser proporcionados a
						terceros de acuerdo con lo estrictamente señalado en el
						Art. 22 de la Ley Federal de Transparencia y Acceso a la
						Información Pública Gubernamental, para lo cual,{" "}
						<Text type="strong">
							Digital Booting Agencia Creativa de Marketing
							Digital
						</Text>{" "}
						se compromete a tratar dicha información, de conformidad
						con los principios de licitud, calidad, acceso y
						corrección de información, seguridad, custodia, y
						consentimiento para su transmisión debiendo obedecer
						exclusivamente.
					</Text>
					<Text mb={20} align="justify">
						La solicitud de corrección parcial o total de datos
						personales, sólo podrá ser formulada por el interesado
						titular de los mismos o su representante legal
						debidamente acreditados, de conformidad con lo
						establecido en los Lineamientos de Protección de Datos
						Personales, mismos que deberán observar las dependencias
						y entidades de la Administración Pública Federal en la
						recepción, procesamiento, trámite, resolución y
						notificación de las solicitudes de corrección de datos
						personales que formulen los particulares.
					</Text>
					<Text mb={20} align="justify">
						La información que nos proporcione mediante el llenado
						de formularios publicados en esta página, puede ser
						incluida dentro de los informes que se elaboran para el
						seguimiento de avances de Digital Booting, los cuales
						serán meramente estadísticos y no incluirán información
						que permita identificarle en lo individual. Los datos
						personales que proporcione en los formularios del sitio
						web o Facebook Fanpage de{" "}
						<Text type="strong">
							Digital Booting Agencia Creativa de Marketing
							Digital
						</Text>{" "}
						, serán protegidos en términos de los artículos 3,
						fracción II, 18, fracción II, 19, 20, fracción VI, 21,
						22 y demás correlativos de la Ley Federal de
						Transparencia y Acceso a la Información Pública
						Gubernamental, así como los artículos 37, 38, 39, 40,
						41, 47 y 48 de su Reglamento, y los Lineamientos de
						Protección de Datos Personales.
					</Text>
					<Text mb={20} align="justify">
						<Text type="strong">
							Digital Booting Agencia Creativa de Marketing
							Digital
						</Text>{" "}
						compromete a su personal que tiene acceso a datos
						personales en el ejercicio de sus funciones o
						intervención en cualquier fase del tratamiento, a
						mantener confidencialidad respecto de dicha información.
						Nos reservamos el derecho de suscribir a todos los
						usuarios al newsletter. Digital Booting Agencia Creativa
						de Marketing Digital informará sobre cualquier cambio a
						la Política de Privacidad y Manejo de Datos Personales.
						Se sugiere visitar con frecuencia esta área del sitio, a
						fin de mantenerse oportunamente informado. Si hubiera
						algún cambio de material en el tipo de información que
						recogemos o en la manera que utilizamos tal información,
						obtendremos su permiso antes de realizar cualquier
						alteración.
					</Text>
				</Col>
			</Flex>
		</Layout>
	);
};

export const Head = () => <Seo {...privacy} />;
export default memo(Page);
