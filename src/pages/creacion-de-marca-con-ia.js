import React, { useEffect, useState, memo } from "react";
import { Layout } from "@layout";
import { Seo } from "@components";

import { TemplateSolution } from "@modules";
import useTranslate from "@lang";
import { brand } from "@seo";

import { CreacionMarca } from "@src/solutions";

//aplicar redireccion 301
const Page = () => {
	const { trans } = useTranslate();
	const [page, setPage] = useState(null);

	useEffect(() => {
		CreacionMarca && setPage(CreacionMarca);
	}, []);
	return (
		<Layout>
			<TemplateSolution data={page} />
		</Layout>
	);
};

export const Head = () => <Seo {...brand} />;
export default memo(Page);
