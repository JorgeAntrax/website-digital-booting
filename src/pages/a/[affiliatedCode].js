import React, { useEffect } from "react";
import { useStorage } from "@storage/store";
import { navigate } from "@utils";

import { validateCookie } from "@services";

const AffiliatedCodeCapture = ({ affiliatedCode }) => {
	const { user, updateStore } = useStorage();

	useEffect(() => {
		loadValidation();
	}, []);

	const loadValidation = async () => {
		const [resolve, reject] = await validateCookie(affiliatedCode);
		resolve &&
			updateStore("user", {
				...user,
				affiliatedCode,
			});
		navigate("/");
	};

	return null;
};

export default AffiliatedCodeCapture;
