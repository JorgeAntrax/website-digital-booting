import React, { memo } from "react";
import { Layout } from "@layout";
import { Seo } from "@components";
import {
	Slider,
	AgenciaCreativa,
	Clientes,
	Soluciones,
	Socios,
	Blogs,
	Asesoria,
} from "@containers/home";

import { Banner } from "@modules";
import useTranslate from "@lang";
import { home } from "@seo";

const Page = () => {
	const { trans } = useTranslate();
	return (
		<Layout>
			<Banner
				title="Agencia de Marketing Digital en México"
				description="Digital Booting es una agencia creativa de marketing digital que ofrece soluciones en publicidad y tecnología para empresas en México y Estados Unidos."
				handleText="Soluciones digitales"
				url="#soluciones"
			/>

			<Slider />

			<AgenciaCreativa />
			<Clientes />
			<Soluciones />
			<Socios />
			<Asesoria />
			<Blogs />
		</Layout>
	);
};

export const Head = () => <Seo {...home} />;
export default memo(Page);
