import * as React from "react";
import { Layout } from "@layout";
import { Text, Title, Button, Seo, Image, Center } from "@components";
import { error } from "@seo";

import errorImage from "@assets/404.jpg";

// markup
const NotFoundPage = () => {
	return (
		<Layout>
			<Center xsMt={100} mdMt={0}>
				<Image url={errorImage} xsDimentions={300} mdDimentions={500} />
			</Center>
			<Title align="center" mb={20}>
				Página no encontrada
			</Title>
			<Text ph={20} align="center">
				{error.description}
			</Text>

			<Center mt={30} pb={80}>
				<Button type="link" href="/" theme="context">
					Ir al inicio
				</Button>
			</Center>
		</Layout>
	);
};

export const Head = () => <Seo {...error} />;
export default NotFoundPage;
