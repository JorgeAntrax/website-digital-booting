import React, { useEffect, useState, memo } from "react";
import { Layout } from "@layout";
import { Seo } from "@components";

import { TemplateSolution } from "@modules";
import useTranslate from "@lang";
import { ia } from "@seo";

import { InteligenciaArtificial } from "@src/solutions";

const Page = () => {
	const { trans } = useTranslate();
	const [page, setPage] = useState(null);

	useEffect(() => {
		InteligenciaArtificial && setPage(InteligenciaArtificial);
	}, []);
	return (
		<Layout>
			<TemplateSolution data={page} />
		</Layout>
	);
};

export const Head = () => <Seo {...ia} />;
export default memo(Page);
