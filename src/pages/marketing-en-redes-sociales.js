import React, { useEffect, useState, memo } from "react";
import { Layout } from "@layout";
import { Seo } from "@components";

import { TemplateSolution } from "@modules";
import useTranslate from "@lang";
import { social } from "@seo";

import { SocialMedia } from "@src/solutions";

//aplicar redireccion 301
const Page = () => {
	const { trans } = useTranslate();
	const [page, setPage] = useState(null);

	useEffect(() => {
		SocialMedia && setPage(SocialMedia);
	}, []);
	return (
		<Layout>
			<TemplateSolution data={page} />
		</Layout>
	);
};

export const Head = () => <Seo {...social} />;
export default memo(Page);
