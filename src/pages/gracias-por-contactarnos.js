import React, { memo } from "react";
import { Layout } from "@layout";
import { Center, Seo, Svg } from "@components";

import { Banner } from "@modules";
import useTranslate from "@lang";
import { home } from "@seo";

import image from "@assets/thanks2.svg";

const Page = () => {
	const { trans } = useTranslate();
	return (
		<Layout>
			<Center pt={50}>
				<Svg icon={image} wsvg={300} />
			</Center>
			<Banner
				height={400}
				title="¡Vamos a lograr grandes cosas juntos!"
				description="Haz tomado una buena decisión para impulsar tu negocio, pronto uno de nuestros asesores se pondra en contacto contigo."
				handleText="volver al inicio"
				url="/"
			/>
		</Layout>
	);
};

export const Head = () => <Seo {...home} />;
export default memo(Page);
