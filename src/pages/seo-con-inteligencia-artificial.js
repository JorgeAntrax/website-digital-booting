import React, { useEffect, useState, memo } from "react";
import { Layout } from "@layout";
import { Seo } from "@components";

import { TemplateSolution } from "@modules";
import useTranslate from "@lang";
import { dataseo } from "@seo";

import { PosicionamientoSeo } from "@src/solutions";

//aplicar redireccion 301
const Page = () => {
	const { trans } = useTranslate();
	const [page, setPage] = useState(null);

	useEffect(() => {
		PosicionamientoSeo && setPage(PosicionamientoSeo);
	}, []);
	return (
		<Layout>
			<TemplateSolution data={page} />
		</Layout>
	);
};

export const Head = () => <Seo {...dataseo} />;
export default memo(Page);
