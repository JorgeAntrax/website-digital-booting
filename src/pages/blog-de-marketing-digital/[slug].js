import React, { useEffect, useState } from "react";
import { Layout } from "@layout";
import { Seo } from "@components";

import posts from "@src/blogs";
import { RenderPost, PostNotFound } from "@modules";
import { getBlogData } from "@services";

const defaultState = {
	exists: null,
	slug: "",
	seo: {},
};

const loadBlog = async (slug, setConfig) => {
	const [exists, seo] = await getBlogData("serverside", slug);
	setConfig({
		...defaultState,
		exists,
		seo,
		slug,
	});
};

const BlogPost = ({ params }) => {
	const { slug } = params;
	const CurrentPost = posts[slug];
	const [config, setConfig] = useState(defaultState);

	useEffect(() => {
		loadBlog(slug, setConfig);
	}, [slug]);

	if (config.exists === null) return null;
	return (
		<Layout>
			{config?.exists === false && <PostNotFound />}
			{config?.exists === true && (
				<RenderPost {...config} post={<CurrentPost />} />
			)}
		</Layout>
	);
};

export const Head = ({ params }) => {
	const { slug } = params;
	const [config, setConfig] = useState(defaultState);

	useEffect(() => {
		loadBlog(slug, setConfig);
	}, [slug]);

	if (config?.exists === false) return null;
	return <Seo {...config?.seo} />;
};

export default React.memo(BlogPost);
