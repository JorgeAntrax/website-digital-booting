import React, { useEffect, useState, memo } from "react";
import { Layout } from "@layout";
import { Seo, Text, Container, Flex, Col } from "@components";
import { Blogs, Socios, Suscribe } from "@containers/blog";
import { Banner, SubTitle, BlurCard } from "@modules";

import { useStorage } from "@storage";
import { getFeatureBlogs } from "@services";

import useTranslate from "@lang";
import { blog } from "@seo";

//aplicar redireccion 301
const Page = () => {
	const { trans } = useTranslate();
	const { fingerprint } = useStorage();
	const [blogs, setBlogs] = useState([]);

	useEffect(() => {
		loader();
	}, []);

	const loader = async () => {
		const resolve = await getFeatureBlogs(fingerprint);

		console.log(resolve);
		resolve.length && setBlogs(resolve);
	};
	return (
		<Layout>
			<Banner
				height={450}
				title="Blog de marketing digital"
				description={
					<>
						Mantente al día con las últimas{" "}
						<Text type="span" color="context">
							tendencias y estrategias
						</Text>{" "}
						de marketing.
					</>
				}
			/>

			<Container mdPh={20}>
				<SubTitle align="left" mb={20} xsPh={20}>
					Articulos destacados
				</SubTitle>

				<Flex flexWrap="wrap">
					{blogs.map((blog, index) => (
						<Col
							xsWidth="100%"
							mdWidth="50%"
							padding={20}
							key={index}
						>
							<BlurCard height={400} theme="dark" {...blog} />
						</Col>
					))}
				</Flex>
			</Container>

			<Blogs />
			<Socios />
			<Suscribe mb={30} />
		</Layout>
	);
};

export const Head = () => <Seo {...blog} />;
export default memo(Page);
