import React, { memo } from "react";
import { Layout } from "@layout";
import { Seo } from "@components";

import { Form } from "@containers/contact";

import { Banner } from "@modules";
import useTranslate from "@lang";
import { home } from "@seo";

const Page = () => {
	const { trans } = useTranslate();
	return (
		<Layout>
			<Banner
				height={300}
				title="tomémonos un café"
				description="¿Estas listo para impulsar tu negocio e incrementar tus ventas? Déjanos tus datos y nuestros expertos se pondrán en contacto contigo."
			/>

			<Form />
		</Layout>
	);
};

export const Head = () => <Seo {...home} />;
export default memo(Page);
