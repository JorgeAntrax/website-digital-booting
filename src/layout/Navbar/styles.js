import styled, { css } from "styled-components";
import { $black } from "@constants/colors";

const MenuContainer = styled.div`
	cursor: pointer;
	position: relative;
	display: inline-block;
	padding: 0.5rem 0;

	&:hover {
		#menu {
			opacity: 1;
			pointer-events: all;
			max-height: 100vh;
		}
	}
`;

const Menu = styled.div`
	display: flex;
	flex-direction: column;
	flex: 1 0 0;
	background-color: white;
	min-width: 260px;
	max-width: 500px;
	border-radius: 8px;
	padding: 0.5rem;

	box-shadow: 0 4px 15px -4px rgba(0, 0, 0, 0.2);
	z-index: 15;

	position: absolute;
	top: 90%;

	opacity: 0;
	max-height: 0;
	pointer-events: none;
	transition: all 200ms linear;
`;

const MenuItem = styled.a`
	padding: 0.75rem;
	transition: color 200ms ease;
	color: black;

	&:hover {
		color: var(--secondary, black);
	}
`;

const MobileMenu = styled.div`
	background-color: white;
	position: fixed;
	bottom: 0;
	left: 0;
	right: 0;
	padding: 0.5rem 2rem 2rem 2rem;
	z-index: 999999999;
	border-radius: 20px 20px 0 0;
	box-shadow: 0 -10px 25px -6px rgba(0, 0, 0, 0.2);
	transition: all 200ms linear;

	${({ show }) =>
		show
			? css`
					pointer-events: all;
					transform: translateY(0);
					opacity: 1;
				`
			: css`
					pointer-events: none;
					transform: translateY(110%);
					opacity: 0;
				`}
`;

const OverlayMenu = styled.div`
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	padding: 2rem;
	z-index: 9999999;
	background-color: rgba(0, 0, 0, 0.3);
	cursor: pointer;

	transition: all 200ms linear;
	transition-delay: 200ms;

	&:focus,
	&:hover {
		outline: 0;
	}

	${({ show }) =>
		show
			? css`
					pointer-events: all;
					opacity: 1;
				`
			: css`
					pointer-events: none;
					opacity: 0;
				`}
`;

export { Menu, MenuContainer, MenuItem, MobileMenu, OverlayMenu };
