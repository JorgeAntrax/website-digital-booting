import React, { memo } from "react";
import { useWidth } from "@hooks";

import DesktopNavigation from "./desktop";
import MobileNavigation from "./mobile";

const Navbar = () => {
	const { isMobile } = useWidth();
	return isMobile ? (
		<MobileNavigation />
	) : isMobile === false ? (
		<DesktopNavigation />
	) : null;
};

export default memo(Navbar);
