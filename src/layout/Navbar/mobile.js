import React, { memo } from "react";
import { Flex, Svg, Text, Button } from "@components";
import { MenuItem, MobileMenu, OverlayMenu } from "./styles";

import { Links } from "@constants";
import useNavigation from "./hooks";

import logo from "@assets/logo.svg";
import instagram from "@assets/instagram.svg";
import whatsapp from "@assets/whatsapp.svg";
import facebook from "@assets/facebook.svg";
import menu from "@assets/menu.svg";
import close from "@assets/close.svg";

const MobileNavigation = () => {
	const { showMenu, setShowMenu } = useNavigation();

	return (
		<>
			<Flex
				top={0}
				left={0}
				right={0}
				width="100%"
				zIndex="900"
				padding={20}
				position="fixed"
			>
				<Flex
					items="center"
					justify="between"
					backgroundColor="rgba(255,255,255,0.5)"
					backdropFilter="blur(6px)"
					boxShadow="0 6px 20px -10px rgba(0,0,0,0.1)"
					borderRadius={10}
					padding="8px 16px"
				>
					<a href={Links.$homepage}>
						<Flex display="inline-flex" items="center">
							<Svg icon={logo} wsvg={28} />
							<Text
								color="black"
								size={18}
								ml={10}
								whiteSpace="nowrap"
							>
								Digital <Text type="strong">Booting</Text>
							</Text>
						</Flex>
					</a>

					<Svg
						icon={menu}
						wsvg={30}
						onClick={() => setShowMenu(true)}
					/>
				</Flex>
			</Flex>

			<OverlayMenu show={showMenu} onClick={() => setShowMenu(false)} />
			<MobileMenu show={showMenu}>
				<Flex items="center" justify="end">
					<Svg
						icon={close}
						wsvg={30}
						style={{ marginRight: "-20px" }}
						onClick={() => setShowMenu(false)}
					/>
				</Flex>
				<Flex flexWrap="wrap" direction="column" justify="center">
					<MenuItem href={Links.$blog}>Blog</MenuItem>

					<MenuItem href={Links.$mkt}>Marketing Digital</MenuItem>
					<MenuItem href={Links.$seo}>SEO con IA</MenuItem>
					<MenuItem href={Links.$web}>Diseño de páginas web</MenuItem>
					<MenuItem href={Links.$branding}>
						Diseño de marca con IA
					</MenuItem>
					<MenuItem href={Links.$social}>
						Marketing en redes sociales
					</MenuItem>

					<MenuItem href={Links.$intelligence}>
						Inteligencia artificial
					</MenuItem>
					<MenuItem href={Links.$afiliateds}>
						Conviertete en socio
					</MenuItem>

					<Flex items="center" justify="between" pv={20}>
						<Button
							ml="1rem"
							type="link"
							href={Links.$contact}
							theme="context"
						>
							Contrátanos
						</Button>
						<Flex
							xsDisplay="inline-flex"
							xsWidth="auto"
							items="center"
						>
							<a
								title="instagram"
								href={Links.$igLink}
								target="blank"
							>
								<Svg icon={instagram} wsvg={24} mh="0.5rem" />
							</a>
							<a
								title="whatsapp"
								href={Links.$wpLink}
								target="blank"
							>
								<Svg icon={whatsapp} wsvg={24} mh="0.5rem" />
							</a>
							<a
								title="facebook"
								href={Links.$fbLink}
								target="blank"
							>
								<Svg icon={facebook} wsvg={24} mh="0.5rem" />
							</a>
						</Flex>
					</Flex>
				</Flex>
			</MobileMenu>
		</>
	);
};

export default memo(MobileNavigation);
