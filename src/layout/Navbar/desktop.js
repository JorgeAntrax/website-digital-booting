import React, { memo } from "react";
import { Container, Flex, Svg, Text, Button } from "@components";

import { Links } from "@constants";
import { Menu, MenuContainer, MenuItem } from "./styles";

import logo from "@assets/logo.svg";
import instagram from "@assets/instagram.svg";
import whatsapp from "@assets/whatsapp.svg";
import facebook from "@assets/facebook.svg";

const DesktopNavigation = () => {
	return (
		<Flex
			items="center"
			justify="center"
			backgroundColor="white"
			borderBottom="1px solid #f1f1f1"
		>
			<Container>
				<Flex items="center" justify="between" minHeight={80}>
					<a href={Links.$homepage}>
						<Flex display="inline-flex" items="center">
							<Svg icon={logo} wsvg={36} />
							<Text
								color="black"
								size={20}
								ml={10}
								whiteSpace="nowrap"
							>
								Digital{" "}
								<Text color="primary" type="strong">
									Booting
								</Text>
							</Text>
						</Flex>
					</a>

					<Flex items="center" justify="end">
						<Text href={Links.$blog} type="link" ph="1rem">
							Blog
						</Text>

						<MenuContainer>
							<Text ph="1rem">Soluciones digitales</Text>

							<Menu id="menu">
								<MenuItem href={Links.$mkt}>
									Marketing Digital
								</MenuItem>
								<MenuItem href={Links.$seo}>
									SEO con IA
								</MenuItem>
								<MenuItem href={Links.$web}>
									Diseño de páginas web
								</MenuItem>
								<MenuItem href={Links.$branding}>
									Diseño de marca con IA
								</MenuItem>
								<MenuItem href={Links.$social}>
									Marketing en redes sociales
								</MenuItem>
							</Menu>
						</MenuContainer>

						<Text href={Links.$intelligence} type="link" ph="1rem">
							Inteligencia artificial
						</Text>
						<Text href={Links.$afiliateds} type="link" ph="1rem">
							Conviertete en socio
						</Text>

						<a
							title="instagram"
							href={Links.$igLink}
							target="blank"
						>
							<Svg icon={instagram} wsvg={24} mh="0.5rem" />
						</a>
						<a title="whatsapp" href={Links.$wpLink} target="blank">
							<Svg icon={whatsapp} wsvg={24} mh="0.5rem" />
						</a>
						<a title="facebook" href={Links.$fbLink} target="blank">
							<Svg icon={facebook} wsvg={24} mh="0.5rem" />
						</a>

						<Button
							ml="1rem"
							type="link"
							href={Links.$contact}
							theme="context"
						>
							Contrátanos
						</Button>
					</Flex>
				</Flex>
			</Container>
		</Flex>
	);
};

export default memo(DesktopNavigation);
