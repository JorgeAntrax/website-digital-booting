import { useState, useEffect, useLayoutEffect } from "react";
import { useStorage } from "@storage/store";

const useNavigation = () => {
	const { clearStore } = useStorage();
	const [showMenu, setShowMenu] = useState(false);

	useEffect(() => {
		const load = async () => {};

		load();
	}, []);

	useLayoutEffect(() => {
		document.body.style.overflow = showMenu ? "hidden" : "visible";
	}, [showMenu]);

	return { clearStore, showMenu, setShowMenu };
};

export default useNavigation;
