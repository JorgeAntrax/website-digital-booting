import React from "react";
import { Container, Flex, Col, Svg, Text } from "@components";

import logo from "@assets/logo.svg";
import whatsapp from "@assets/whatsapp.svg";
import instagram from "@assets/instagram.svg";
import facebook from "@assets/facebook.svg";

import { Links } from "@constants";
import useTranslate from "@lang";

const Footer = () => {
	const { trans } = useTranslate();
	return (
		<Flex
			justify="center"
			backgroundColor="black"
			color="white"
			xsPadding="2rem"
			mdPadding="3rem 0 1rem 0"
		>
			<Container>
				<Flex flexWrap="wrap" items="start">
					<Col xsWidth="100%" mdWidth="25%" xsMb={20} mdMb={60}>
						<Flex display="inline-flex" items="center" mt="-8px">
							<Svg icon={logo} wsvg={36} />
							<Text size={20} ml={10} whiteSpace="nowrap">
								Digital{" "}
								<Text color="primary" type="strong">
									Booting
								</Text>
							</Text>
						</Flex>
					</Col>
					<Flex
						direction="column"
						xsWidth="100%"
						mdWidth="25%"
						xsMb={20}
						mdMb={60}
					>
						<Text xsMb={24}>Agencia creativa</Text>
						<Text
							size={14}
							xsHeight={35} items="center" display="flex"
							type="link"
							href={Links.$contact}
						>
							Contáctanos
						</Text>
						<Text size={14} xsHeight={35} items="center" display="flex" type="link" href={Links.$blog}>
							Blog de marketing digital
						</Text>
						<Text
							size={14}
							xsHeight={35} items="center" display="flex"
							type="link"
							href={Links.$afiliateds}
						>
							Programa de asociados
						</Text>
						{/* <Text size={14} xsHeight={35} items="center" display="flex" type="link" href={Links.$work}>
							Unete al equipo
						</Text> */}
					</Flex>
					<Flex
						direction="column"
						xsWidth="100%"
						mdWidth="25%"
						xsMb={20}
						mdMb={60}
					>
						<Text xsMb={24}>Soluciones digitales</Text>
						<Text size={14} xsHeight={35} items="center" display="flex" type="link" href={Links.$mkt}>
							Marketing digital
						</Text>
						<Text size={14} xsHeight={35} items="center" display="flex" type="link" href={Links.$web}>
							Diseño de páginas web
						</Text>
						<Text
							size={14}
							xsHeight={35} items="center" display="flex"
							type="link"
							href={Links.$branding}
						>
							Diseño de marca con IA
						</Text>
						<Text size={14} xsHeight={35} items="center" display="flex" type="link" href={Links.$seo}>
							SEO con IA
						</Text>
						<Text
							size={14}
							xsHeight={35} items="center" display="flex"
							type="link"
							href={Links.$social}
						>
							Marketing en redes sociales
						</Text>
						<Text
							size={14}
							xsHeight={35} items="center" display="flex"
							type="link"
							href={Links.$intelligence}
						>
							IA Marketing digital
						</Text>
					</Flex>
					<Flex
						direction="column"
						xsWidth="100%"
						mdWidth="25%"
						xsMb={20}
						mdMb={60}
					>
						<Text xsMb={24}>Contacto</Text>
						<Text
							type="link"
							href={`mailto:hola@digitalbooting.com`}
							size={14}
							xsHeight={35} items="center" display="flex"
							userSelect="none"
						>
							hola@digitalbooting.com
						</Text>
						<Text size={14} xsHeight={35} items="center" display="flex" userSelect="none">
							(55) 4806 7069 CDMX.
						</Text>
						<Text size={14} xsHeight={35} items="center" display="flex" userSelect="none">
							(656) 7881 950 CDJRZ.
						</Text>
					</Flex>
				</Flex>
				<Flex flexWrap="wrap" items="center" justify="between">
					<Text
						xsWidth="100%"
						mdWidth="50%"
						xsSize={10}
						mdSize={12}
						pb={8}
						userSelect="none"
						xsAlign="center"
						mdAlign="left"
					>
						Digital{" "}
						<Text color="primary" type="strong" size="inherit">
							Booting
						</Text>
						, Agencia Creativa de Marketing Digital
					</Text>

					<Flex
						xsWidth="100%"
						mdWidth="50%"
						xsJustify="center"
						mdJustify="end"
						items="center"
					>
						<Text
							xsSize={10}
							mdSize={12}
							ph={16}
							userSelect="none"
							borderRight="1px solid white"
							type="link"
							href={Links.$privacy}
						>
							Aviso de privacidad
						</Text>
						{/* <Text size={12} ph={16} userSelect="none" borderRight="1px solid white" type="link" href={Links.$terms}>
							Terminos y condiciones
						</Text> */}
						<Text xsSize={10} mdSize={12} ph={16} userSelect="none">
							Copyright &copy; {new Date().getFullYear()}
						</Text>
					</Flex>
				</Flex>
			</Container>
		</Flex>
	);
};

export default Footer;
