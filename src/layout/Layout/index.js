import React, { memo, useEffect } from "react";
import { Analytics } from "@vercel/analytics/react";
import { SpeedInsights } from "@vercel/speed-insights/react";

import { Navbar, Footer, ChatBot } from "@layout";
import { useSession, useWidth } from "@hooks";
import { getLocation } from "@utils";
import { useStorage } from "@storage/store";

import { getAffiliatedCode } from "@services";

const Layout = ({ children }) => {
	useSession();
	const { width } = useWidth();
	const { route, user, updateStore } = useStorage();

	useEffect(() => {
		const r = getLocation("pathname")
			.split("/")
			.filter((r) => !!r);
		r.length && updateStore('route', route);

		setTimeout(() => {
			document.body.classList.add("loaded");
		}, 1000);

		if (typeof window !== "undefined") {
			window.scrollTo(0, 0);
		}

		getCode();
	}, []);

	const getCode = async () => {
		const [resolve, reject] = await getAffiliatedCode();

		if (resolve) {
			updateStore('user', {
				...user,
				affiliatedCode: resolve.afiliatedCode,
			});
		}
	};

	return (
		<>
			<Navbar />
			<section id="main">{children}</section>
			<Footer />
			<ChatBot />
			<Analytics />
			<SpeedInsights />
		</>
	);
};

export default memo(Layout);
