import React, { useEffect, useState } from "react";
import { Links } from "@constants";
import styled from "styled-components";
import { Flex, Col, Text, Image } from "@components";
import chat from "@assets/chat.svg";

const Wrapper = styled.a`
	display: flex;
	position: fixed;
	bottom: 0.5rem;
	right: 0.5rem;
	z-index: 500;

	min-width: 100px;
	min-height: 100px;
`;

const Message = styled.div`
	display: inline-flex;
	background-color: white;
	border-radius: 20px 20px 8px 20px;
	padding: 0.5rem 1rem;
	z-index: 501;

	min-width: max-content;

	box-shadow: 0 2px 26px -6px rgba(0, 0, 0, 0.3);
	position: absolute;
	top: 26px;
	right: 100%;
`;

const ChatBot = () => {
	const [message, showMessage] = useState(true);

	useEffect(() => {
		setTimeout(() => {
			showMessage(false);
		}, 15000);
	}, []);
	return (
		<Wrapper href={Links.$wpLink} target="_blank">
			<Col position="relative">
				{message && (
					<Message>
						<Text color="black">Hablar con un asesor</Text>
					</Message>
				)}
				<Image url={chat} dimentions={100} />
			</Col>
		</Wrapper>
	);
};

export default ChatBot;
