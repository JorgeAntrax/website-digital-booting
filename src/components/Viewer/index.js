import React, { useState, useEffect } from "react";
import { Flex, Col, ButtonIcon, Text, Svg } from "@components";
import { Portal, PortalHeader, PortalButton } from "./styles";

import aleft from "@assets/icons/aleft.svg";
import aright from "@assets/icons/aright.svg";

const Viewer = ({ show, files, currentImg, onClose, hideInfo, typeURL }) => {
	const [active, setActive] = useState(0);
	const [limit, setLimit] = useState(0);

	const next = () => {
		const c = active + 1;
		setActive(c < limit ? c : limit);
	};

	const prev = () => {
		const c = active - 1 > 0 ? active - 1 : 0;
		setActive(c);
	};

	const handleClose = () => {
		setActive(0);
		onClose();
	};

	useEffect(() => {
		setLimit(files.length - 1);
	}, [files]);

	useEffect(() => {
		setActive(currentImg);
	}, [currentImg]);

	return (
		<Portal show={show} current={active}>
			<PortalHeader>
				<Flex items="center" justify="between">
					{!hideInfo && (
						<Col>
							<Text fs="18px" fw={500}>
								{files.length || 0} For upload files
							</Text>
						</Col>
					)}
					{hideInfo && <span></span>}
					<Col>
						<ButtonIcon
							tooltip="Close preview"
							direction="bottom"
							pill
							w={30}
							icon="close"
							square
							widthTooltip={80}
							onClick={() => handleClose()}
						/>
					</Col>
				</Flex>
			</PortalHeader>

			<Flex
				items="center"
				align="center"
				justify="center"
				wrap="wrap"
				max-w="100%"
				position="relative"
			>
				{active > 0 && (
					<PortalButton
						onClick={() => prev()}
						left="6%"
						lg-left="25%"
					>
						<Svg icon={aleft} w="100%" h="100%" />
					</PortalButton>
				)}

				<Col min-w="50%" w="90%" md-w="50%">
					<Flex
						align="center"
						items="center"
						content="center"
						w="100%"
					>
						<Flex
							id="slide"
							w={`${files.length}00%`}
							ml={`-${active}00%`}
							max-w="100%"
							wrap="no-wrap"
						>
							{!!files.length &&
								files.map(
									(
										{
											name,
											urlObject,
											url,
											fileSize,
											type,
										},
										index
									) => (
										<Flex
											key={index}
											justify="center"
											align="center"
											className="slide_item"
											style={{
												minWidth: "100%",
												opacity:
													active === index ? 1 : 0.5,
												transform:
													active === index
														? "scale(1)"
														: "scale(0.7)",
											}}
										>
											{[
												"video/mp4",
												"video/avi",
											].includes(type) && (
												<video
													style={{
														height: "450px",
														maxHeight: "450px",
														objectFit: "contain",
													}}
													controls
												>
													<source
														src={urlObject}
														type={type}
													/>
												</video>
											)}

											{[
												"image/jpeg",
												"image/jpg",
												"image/png",
												"image/webp",
											].includes(type) && (
												<img
													loading="lazy"
													src={urlObject}
													alt=""
													style={{
														width: "100%",
														height: "80vh",
														maxHeight: "700px",
														objectFit: "contain",
													}}
												/>
											)}

											{typeURL === "webp" && (
												<img
													loading="lazy"
													src={url}
													alt=""
													style={{
														width: "100%",
														height: "80vh",
														maxHeight: "700px",
														objectFit: "contain",
													}}
												/>
											)}

											{!hideInfo && (
												<Text
													fs={14}
													fw={500}
													align="center"
													className="p:1"
												>
													{name} -{" "}
													{fileSize.formatedSize}
												</Text>
											)}
										</Flex>
									)
								)}
						</Flex>
					</Flex>
				</Col>

				{active !== limit && (
					<PortalButton
						onClick={() => next()}
						right="6%"
						lg-right="25%"
					>
						<Svg icon={aright} w="100%" h="100%" />
					</PortalButton>
				)}
			</Flex>
		</Portal>
	);
};

export default Viewer;
