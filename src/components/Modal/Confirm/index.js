import React from "react";
import { Modal, Text, Flex, Col, Button } from "@components";
import secureAlert from "@assets/icons/secure-alert.svg";

const ModalConfirm = ({ show, onClose, onAccept, children }) => {
	return (
		<Modal
			width={500}
			show={show}
			onClose={onClose}
			icon={secureAlert}
			unClosed
			title="Please wait..."
		>
			<div className="ph:2 pt:2">
				<Text align="center" fs="16px" fw={300} >
					{children}
				</Text>
			</div>

			<Flex wrapper justify="center" className="ph:2 pv:2">
				<Col autofit className="ph:05">
					<Button square light onClick={() => onClose()}>Cancel</Button>
				</Col>
				<Col autofit className="ph:05">
					<Button square secondary onClick={() => onAccept()}>Confirm</Button>
				</Col>
			</Flex>

		</Modal>
	);
};


export default ModalConfirm
