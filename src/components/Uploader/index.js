import React, { useRef, useState, useEffect } from "react";
import { Uploader, File, Title, InfoMessage } from "./styles";
import {
	Flex,
	Col,
	Svg,
	Button,
	Text,
	ButtonIcon,
	Box,
	Viewer,
	Grid,
} from "@components";
import icon from "@assets/icons/files.svg";

const Upload = ({
	name,
	id,
	multiple,
	limit = 5,
	typeSize = "MB",
	type = "img",
	getValue,
	labelSize,
	title,
	getErrorSize,
	children,
	defaultData,
}) => {
	const _label = useRef(null);
	const [files, setFiles] = useState(defaultData || []);
	const [showPreview, setShowPreview] = useState(false);
	const [errorLimitFiles, setErrorLimitFiles] = useState(false);
	const [currentPreview, setCurrentPreview] = useState(0);
	const [acumultiveSize, setAcumultiveSize] = useState(0);
	const formats =
		type === "video"
			? "video/mp4, video/avi"
			: "image/png,image/jpeg,image/jpg,image/webp";

	useEffect(() => {
		getValue && getValue(multiple ? files : files[0]);
	}, [files]);

	useEffect(() => {
		if (acumultiveSize.size > 150 && acumultiveSize.unit === "MB") {
			getErrorSize && getErrorSize(true);
		} else {
			getErrorSize && getErrorSize(false);
		}
	}, [acumultiveSize]);

	const handleChange = ({ files }) => {
		const _files = [];
		let totalSize = 0;

		for (let f in files) {
			if (typeof files[f] === "object") {
				totalSize += files[f].size;
				files[f].fileSize = fileSize(files[f].size);
				files[f].validate = validateFile(files[f]);
				files[f].errorValidate = validateFile(files[f])
					? ""
					: `The size of this file exceeds ${limit} MB`;

				files[f].urlObject = URL.createObjectURL(files[f]);
				_files.push(files[f]);
			}
		}
		const filterFiles = reducerDFiles(_files);
		const tSize = fileSize(totalSize);
		setAcumultiveSize(tSize);

		if (tSize.size > 150 && tSize.unit === "MB") {
			setFiles([]);
			return;
		}

		let allFiles = [...filterFiles];

		if (multiple) {
			if (allFiles.length > 20) {
				setFiles(allFiles.slice(0, 20));
				setErrorLimitFiles(true);
			} else {
				setFiles(allFiles);
				setErrorLimitFiles(false);
			}
		} else {
			setFiles(allFiles);
		}
	};

	const handleSelectFiles = () => {
		_label.current.click();
	};

	const handleDelete = (key) => {
		const _files = files.filter((item, index) => index !== key);
		setFiles([..._files]);
	};

	const handleShowPreview = (index) => {
		setCurrentPreview(index);
		setShowPreview(true);
	};

	//drag events
	const dragOver = (e) => {
		e.preventDefault();
	};

	const dragEnter = (e) => {
		e.preventDefault();
	};

	const dragLeave = (e) => {
		e.preventDefault();
	};

	const fileDrop = (e) => {
		e.preventDefault();
		const files = e.dataTransfer;
		if (!!files.files.length) {
			handleChange(files);
		}
	};

	const validateFile = (file) => {
		const validTypes =
			type === "video"
				? ["video/mp4", "video/avi"]
				: ["image/jpeg", "image/jpg", "image/png", "image/webp"];
		if (validTypes.includes(file.type)) {
			return file.fileSize.unit === typeSize
				? file.fileSize.size < limit
				: true;
		}
		return false;
	};

	const fileSize = (size) => {
		if (size === 0) return "0 Bytes";
		const k = 1024;
		const sizes = ["Bytes", "KB", "MB", "GB", "TB"];
		const i = Math.floor(Math.log(size) / Math.log(k));

		const _size = parseFloat((size / Math.pow(k, i)).toFixed(2));
		return {
			size: _size,
			formatedSize: `${_size} ${sizes[i]}`,
			unit: sizes[i],
		};
	};

	const reducerDFiles = (files) => {
		//remove files duplicated
		let _files = [];
		let fileNames = [];
		if (files.length) {
			let filteredArray = files.map((file) => {
				if (!fileNames.includes(file.name)) {
					fileNames.push(file.name);
					return file;
				}
			});

			_files = [...filteredArray];
		}

		return _files.purge();
	};

	return (
		<>
			<Uploader
				name={name}
				htmlFor={id}
				onDragOver={dragOver}
				onDragEnter={dragEnter}
				onDragLeave={dragLeave}
				onDrop={fileDrop}
				className="mb:3"
			>
				<File
					id={id}
					type="file"
					accept={formats}
					multiple={multiple}
					ref={_label}
					onChange={({ target }) => handleChange(target)}
				/>

				<Col w="100%" items="center">
					<Title>{title || "File(s)"}</Title>
					{labelSize && (
						<Text fs="14px" fw={500} align="center">
							{labelSize}
						</Text>
					)}
					<Text fs="14px" fw={500} align="center">
						Drag and drop{" "}
						{type === "video"
							? multiple
								? "your videos"
								: "your video"
							: multiple
							? "your images"
							: "your image"}{" "}
						here or
					</Text>
					<Flex w="100%" pt="1rem" justify="center">
						<Col>
							<Button
								square
								secondary
								onClick={() => handleSelectFiles()}
							>
								<Svg icon={icon} w="18px" h="18px" />
								Select{" "}
								{type === "video"
									? multiple
										? "videos"
										: "video"
									: multiple
									? "images"
									: "image"}
							</Button>
						</Col>
					</Flex>
				</Col>
			</Uploader>

			{children}

			{errorLimitFiles && (
				<InfoMessage>
					Estas intentando cargar más de 20 archivos al mismo tiempo,
					el resto de los archivos han sido ignorados, porfavor carga
					de 20 en 20.
				</InfoMessage>
			)}

			{!!files.length &&
				files.map(
					({ name, type, fileSize, validate, errorValidate }, id) => (
						<Box
							key={id}
							inline
							ph="1rem"
							pv="0.5rem"
							className="mb:05"
						>
							<Grid
								cols="1"
								justify-items="start"
								md-cols="auto 1fr auto auto auto auto"
								md-items="center"
							>
								<Col>
									<Text fw={500} fs="14px">
										{id + 1}
									</Text>
									<Text fs="10px" opacity={0.2}>
										ID
									</Text>
								</Col>
								<Col>
									<Text fw={500} fs="14px">
										{name.length > 25
											? name.substring(0, 25) +
											  "..." +
											  type.replace(/image\//g, "")
											: name}
									</Text>
									<Text fs="10px" opacity={0.2}>
										{type}
									</Text>
								</Col>

								<Col>
									<Text fw={500} fs="14px">
										{fileSize.formatedSize}
									</Text>
									<Text fs="10px" opacity={0.2}>
										Size
									</Text>
								</Col>

								<Col>
									<ButtonIcon
										tooltip="View image"
										direction="bottom"
										pill
										w={18}
										icon="show"
										square
										widthTooltip={60}
										onClick={() => handleShowPreview(id)}
									/>
								</Col>
								<Col>
									<ButtonIcon
										tooltip="Delete"
										direction="bottom"
										pill
										w={18}
										icon="trash"
										square
										widthTooltip={50}
										onClick={() => handleDelete(id)}
									/>
								</Col>
							</Grid>
						</Box>
					)
				)}

			<Viewer
				files={files}
				show={showPreview}
				currentImg={currentPreview}
				onClose={() => setShowPreview(false)}
			/>
		</>
	);
};

export default Upload;
