import React from "react";
import { Flex, Col, Button } from "@components";

const UploaderControls = ({ type, requesting, items, error, onUpload, onContinue }) => {
	return (
		<Flex justify="end" className="mt:3">
			<Col autofit className="ph:05">
				<Button square secondary onClick={() => onContinue()}>
					Continuar en otro momento
				</Button>
			</Col>
			<Col autofit className="ph:05">
				<Button
					square
					primary
					disabled={requesting || (!requesting && items === 0) || error}
					onClick={() => onUpload()}
				>
					Subir {type}
				</Button>
			</Col>
		</Flex>
	);
};

export default UploaderControls;
