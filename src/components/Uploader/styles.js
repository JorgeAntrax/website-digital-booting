import styled, { css } from "styled-components";
import { SuperCSS } from "@utils";

const Uploader = styled.label`
	display: flex;
	align-items: center;
	align-content: center;
	justify-content: center;
	flex-wrap: wrap;
	width: 100%;
	min-height: 200px;
	height: 200px;
	border: 1px dashed #aeaeae;
	border-radius: 5px;
	position: relative;
	margin-top: 1rem;
	${(props) => SuperCSS.hydrate(props)}
`;

const Title = styled.span`
	display: inline-block;
	width: 100%;
	position: absolute;
	left: 0;
	top: -0.9rem;
	font-size: 10px;
	color: #888;
	font-family: "Poppins";
	user-select: none;
	${(props) => SuperCSS.hydrate(props)}
`;

const File = styled.input`
	display: none;
	opacity: 0;
	${(props) => SuperCSS.hydrate(props)}
`;

const message = css`
	border-radius: 5px;
	margin-bottom: 1rem;
	font-size: 12px;
	padding: 0.5rem 1rem;
	text-align: center;
	line-height: 1.5;
	display: block;
	${(props) => SuperCSS.hydrate(props)}
`;

const InfoMessage = styled.span`
	${message}
	background-color: rgba(0, 0, 255, 0.1);
	border: 1px solid rgba(0, 0, 255, 0.1);
	color: blue;
	${(props) => SuperCSS.hydrate(props)}
`;

const ErrorMessage = styled.span`
	${message}
	background-color: rgba(255, 0, 0, 0.1);
	border: 1px solid rgba(255, 0, 0, 0.1);
	color: red;
	${(props) => SuperCSS.hydrate(props)}
`;

export { Uploader, File, Title, InfoMessage, ErrorMessage };
