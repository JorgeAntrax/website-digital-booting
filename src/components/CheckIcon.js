import React from "react";

const CheckIcon = ({ w, color, ...props }) => {
	return (
		<div {...props}>
			<svg
				width={w || 32}
				height={w || 32}
				viewBox="0 0 32 32"
				fill="none"
				xmlns="http://www.w3.org/2000/svg"
			>
				<path
					d="M27 9L13 23L6 16"
					stroke={color || "#394CF8"}
					strokeWidth="3"
					strokeLinecap="round"
					strokeLinejoin="round"
				/>
			</svg>
		</div>
	);
};

export default CheckIcon;
