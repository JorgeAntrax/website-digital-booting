import React from "react";
import styled from "styled-components";
import { SuperCSS } from "@utils";

const Wrapper = styled.div`
	width: 100%;
	max-width: 100%;
	margin-left: auto;
	margin-right: auto;

	${(props) => SuperCSS.hydrate(props)}
`;

const Container = ({ children, smMaxw, lgMaxw, ...props }) => {
	return (
		<Wrapper smMaxw={smMaxw || "90%"} lgMaxw={lgMaxw || "80%"} {...props}>
			{children}
		</Wrapper>
	);
};

export default Container;
