import React from "react";
import styled from "styled-components";
import { SuperCSS } from "@utils";

const Wrapper = styled.div`
	display: flex;
	width: 100%;

	${(props) => SuperCSS.hydrate(props)}
`;

const Flex = ({ children, ...props }) => {
	return <Wrapper {...props}>{children}</Wrapper>;
};

export default Flex;
