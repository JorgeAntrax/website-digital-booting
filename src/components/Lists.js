import styled from "styled-components";
import { SuperCSS } from "@utils";

const List = styled.ul`
	display: flex;
	flex-direction: column;
	padding-left: 1.6rem;

	${(props) => SuperCSS.hydrate(props)}
`;

const ListItem = styled.li`
	line-height: 1.5;
	display: block;
	width: 100%;
	margin-bottom: 20px;
	list-style: circle;

	position: relative;

	&:before {
		content: "•";
		position: absolute;
		left: -16px;
	}

	${(props) => SuperCSS.hydrate(props)}
`;

export { List, ListItem };
