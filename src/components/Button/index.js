import React from "react";
import { Base, BaseLink } from "./styles";
import { Loader } from "@components";

const Button = ({
	children,
	type,
	forwardRef,
	mobile,
	loading,
	theme,
	...props
}) => {
	let styles = {};
	const Wrapper = type === "link" ? BaseLink : Base;

	switch (theme) {
		case "primary":
			styles = {
				backgroundColor: "primary",
				color: "black",
			};
			break;
		case "context":
			styles = {
				backgroundColor: "context",
				color: "white",
			};
			break;

		default:
			break;
	}

	return (
		<Wrapper
			{...props}
			{...styles}
			ref={forwardRef}
			mobile={mobile}
			loading={loading}
			centered
		>
			{loading && <Loader className="loader-button" w={22} whited />}
			{children}
		</Wrapper>
	);
};

export default Button;
