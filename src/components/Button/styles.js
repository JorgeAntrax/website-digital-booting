import styled, { css } from "styled-components";
import { SuperCSS } from "@utils";

const baseStyles = css`
	display: inline-flex;
	background-color: transparent;
	gap: calc(16px / 2);
	align-items: center;
	align-content: center;
	justify-content: center;
	text-align: center;
	position: relative;
	white-space: nowrap;
	transition: all 200ms ease;
	padding: 1.5rem 2rem;
	border: 1px solid transparent;
	border-radius: 16px;
	font-weight: 300;
	overflow: hidden;
	cursor: pointer;
	line-height: 1;

	* {
		margin: 0;
		padding: 0;
	}

	&:disabled,
	&:disabled:hover {
		box-shadow: none;
		background: transparent;
		border: 2px solid #e0e0e0;
		color: #e0e0e0;
		cursor: not-allowed;
	}

	${({ loading }) =>
		loading &&
		css`
			span {
				opacity: 0.5;
			}
			.loader-button {
				position: absolute;
				left: 40%;
				transform-origin: center center;
				transform: translateX(-50%);
				top: 6px;
				z-index: 2;
			}
		`}

	${({ theme }) => ["primary"].includes(theme) && primaryStyles}
	${({ theme }) => ["action"].includes(theme) && actionStyles}
	${(props) => SuperCSS.hydrate(props)}
`;

const IconCircle = styled.i`
	height: 25px;
	width: 25px;
	border-radius: 40px;
	background-color: #53565a;
	display: inline-block;
	vertical-align: middle;
	text-align: center;
	margin-right: 15px;
	transition: background 100ms linear;
	svg {
		fill: #fff;
		transition: color 100ms linear;
	}

	${(props) => SuperCSS.hydrate(props)}
`;

const Base = styled.button`
	${baseStyles}
`;
const BaseLink = styled.a`
	${baseStyles}
`;

export { Base, BaseLink, IconCircle };
