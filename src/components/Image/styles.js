import styled from "styled-components";
import { SuperCSS } from "@utils";

const Wrapper = styled.picture`
	overflow: hidden;
	position: relative;
	display: block;
	border: none;
	outline: 0;

	width: 100%;
	height: 100%;

	img {
		width: 100%;
		height: 100%;
	}

	${(props) => SuperCSS.hydrate(props)}
`;

export { Wrapper };
