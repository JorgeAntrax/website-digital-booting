import React, { useRef, useEffect } from "react";
import { Wrapper } from "./styles";

function Image({ url, name, fit = "contain", ...props }) {
	const ref = useRef();

	useEffect(() => {
		if (!ref.current) return;
		const observer = new IntersectionObserver(
			(entries) => {
				const entry = entries[0];
				if (entry.isIntersecting) {
					let lazyImage = entry.target;
					lazyImage.src = lazyImage.dataset.src;
					observer.unobserve(lazyImage);
				}
			},
			{
				threshold: 0.1,
			}
		);
		observer.observe(ref.current);
	}, [ref]);

	return (
		<Wrapper {...props}>
			<img
				ref={ref}
				className="lazy"
				loading="lazy"
				data-src={url}
				alt={name}
				style={{ objectFit: fit }}
			/>
		</Wrapper>
	);
}

export default Image;
