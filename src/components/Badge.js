import React from "react";
import styled from "styled-components";
import { SuperCSS } from "@utils";

const defaultColor = "#000000";

const Wrapper = styled.span`
	display: inline-flex;
	align-items: center;
	align-content: center;
	justify-content: center;
	padding: 3px 8px;
	margin: 2px 4px;
	min-width: 20px;
	border-radius: ${({ rounded }) => (rounded ? "10px" : "4px")};
	font-size: ${({ regular }) => (regular ? 12 : 14)}px;
	font-weight: ${({ regular }) => (regular ? 300 : 500)};
	background-color: ${({ color }) => {
		return color
			? SuperCSS.hexToRGB(color, 1)
			: SuperCSS.hexToRGB(defaultColor, 1);
	}};

	text-transform: capitalize;
	color: ${({ color }) => (color ? color : "#fff")};

	${(props) => SuperCSS.hydrate(props)}
`;

const Badge = ({ children, ...props }) => (
	<Wrapper {...props}>{children}</Wrapper>
);

export default Badge;
