import React from "react";

const Line = ({ color, width, height, stroke }) => {
	return (
		<svg
			width={width || 78}
			height={height || 13}
			viewBox="0 0 78 13"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
		>
			<path
				d="M1.5 6.5C4.69865 3.10419 13.0152 -1.64993 20.6919 6.5C28.3687 14.6499 36.3485 9.89581 39.3788 6.5C42.5774 3.26986 50.6919 -1.25233 57.5606 6.5C64.4293 14.2523 73.0488 9.73014 76.5 6.5"
				stroke={color || "#394CF8"}
				strokeWidth={stroke || 3}
			/>
		</svg>
	);
};

export default Line;
