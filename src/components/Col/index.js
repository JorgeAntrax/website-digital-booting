import React from "react";
import styled from "styled-components";
import { SuperCSS } from "@utils";

const ColumnWrap = styled.div`
	${(props) => SuperCSS.hydrate(props)}
`;

const Col = ({ children, ...props }) => {
	return <ColumnWrap {...props}>{children}</ColumnWrap>;
};

export default Col;
