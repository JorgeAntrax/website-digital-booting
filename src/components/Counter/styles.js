import styled from "styled-components";
import { SuperCSS } from "@utils";

const Button = styled.button`
	display: inline-flex;
	align-items: center;
	justify-content: center;
	width: 40px;
	height: 40px;
	min-width: 40px;
	min-height: 40px;
	cursor: pointer;
	transition: background-color 100ms linear;
	border: none;
	color: #707372;
	background-color: transparent;

	&:hover {
		background-color: rgba(0, 0, 0, 0.05);
	}
	${(props) => SuperCSS.hydrate(props)}
`;

const Quantity = styled.div`
	display: inline-flex;
	width: auto;
	flex-wrap: nowrap;
	color: #707372;
	justify-content: space-between;
	height: 40px;
	background-color: #f6f6f6;
	overflow: hidden;
	${(props) => SuperCSS.hydrate(props)}
`;

const QuantityWrapper = styled.div`
	display: inline-flex;
	width: auto;
	${(props) => SuperCSS.hydrate(props)}
`;

const Label = styled.label`
	user-select: none;
	pointer-events: none;
	color: #707372;
	font-size: inherit;
	font-family: inherit;
	min-width: 40px;
	height: 40px;
	display: flex;
	align-items: center;
	justify-content: center;
	${(props) => SuperCSS.hydrate(props)}
`;

export { Label, Button, Quantity, QuantityWrapper };
