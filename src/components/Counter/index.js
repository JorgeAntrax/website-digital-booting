import React, { useState, useEffect } from "react";
import { Svg } from "@components";
import { Label, Button, Quantity, QuantityWrapper } from "./styles";

import aleft from "@assets/icons/aleft.svg";
import aright from "@assets/icons/aright.svg";

const QuantityComponent = ({
	value = 1,
	step = 1,
	max = 10,
	children,
	getValue,
	...props
}) => {
	const [count, setCount] = useState(1);

	const rest = () => {
		const v = count - step > 0 ? count - step : step;
		setCount(v);
	};

	const add = () => {
		const v = count + step < max ? count + step : max;
		setCount(v);
	};

	const reset = () => setCount(1);
	useEffect(() => {
		getValue && getValue(count);
	}, [count]);

	useEffect(() => {
		if (!!value) {
			setCount(value);
		}
	}, [value]);

	return (
		<Quantity {...props}>
			<QuantityWrapper>
				<Button link onClick={() => rest()}>
					<Svg icon={aleft} w="20px" h="20px" />
				</Button>
				<Label>{count}</Label>
				<Button link onClick={() => add()}>
					<Svg icon={aright} w="20px" h="20px" />
				</Button>
			</QuantityWrapper>
		</Quantity>
	);
};

export default QuantityComponent;
