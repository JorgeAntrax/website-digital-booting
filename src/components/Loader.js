import React from "react";
import styled, { keyframes } from "styled-components";
import { SuperCSS } from "@utils";

const spin = keyframes`
	from {
		transform: rotate(270deg);
	}
	to{
		transform: rotate(630deg);
	}
	`;

const Spinner = styled.div`
	display: inline-block;
	border-radius: 100%;
	border: 3px solid
		rgba(${({ whited }) => (whited ? "255,255,255" : "0,0,0")}, 1);
	border-left-color: transparent;
	animation: ${spin} 1s linear infinite;

	${(props) => SuperCSS.hydrate(props)}
`;

const Loader = ({ ...props }) => <Spinner {...props}></Spinner>;
export default Loader;
