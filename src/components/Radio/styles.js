import styled, { css } from "styled-components";
import { SuperCSS } from "@utils";

const Radio = styled.input`
	display: block;
	height: 0px;
	margin: 0;
	opacity: 0;
	width: 0px;

	&:checked ~ #checked {
		display: inline-flex;
	}
	&:checked ~ #empty {
		display: none;
	}

	&:not(:checked) ~ #checked {
		display: none;
	}
	&:not(:checked) ~ #empty {
		display: inline-flex;
	}
	${(props) => SuperCSS.hydrate(props)}
`;

const WrapRadio = styled.span`
	display: flex;
	justify-content: start;
	align-items: center;
	${(props) => SuperCSS.hydrate(props)}
`;

const Label = styled.label`
	cursor: pointer;
	display: inline-flex;
	align-items: center;
	font-size: 14px;
	${(props) => SuperCSS.hydrate(props)}
`;

export { Label, Radio, WrapRadio };
