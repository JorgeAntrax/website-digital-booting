import styled from "styled-components";
import * as colors from "@constants/colors";

const WrappInput = styled.div`
	height: auto;
	width: 100%;
	border-radius: 0;
	background-color: ${({ hasError, hasWarning }) =>
		hasWarning ? "#fff3df" : hasError ? "#FFD8D9" : "#fff"};
	border-style: solid;
	border-width: ${({ hasError, hasWarning }) =>
		hasWarning ? "2px" : hasError ? "2px" : "1px"};
	border-color: ${({ hasError, hasWarning, hasFocus, disabled }) => {
		if (hasFocus) {
			return colors.$primary;
		}
		if (hasWarning) {
			return "#e48d00";
		}
		if (hasError) {
			return "#E82A4D";
		}
		if (disabled) {
			return "rgba(83,86,90,0.4)";
		} else {
			return "var(--context,#768692)";
		}
	}};
	position: relative;
	border-radius: 4px 4px 0 0;
	border-radius: 8px;
	textarea {
		padding-top: ${(props) => (props.small ? "4px" : "14px")};
	}
`;

const ItemInput = styled.textarea`
	border: none;
	outline: none;
	padding: 14px 3rem 4px 4px;
	background: transparent;
	resize: vertical;
	width: 100%;
	min-height: 150px;
	z-index: 2;
	font-size: 14px;
	font-family: "Poppins";
	font-weight: 300;
	color: #707372;
	border-radius: 8px;

	appearance: textfield;
	&::-webkit-outer-spin-button,
	&::-webkit-inner-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}

	&:disabled {
		& ~ .actionButton {
			display: none;
		}
	}
`;

const Label = styled.span`
	color: ${({ disabled }) => (disabled ? "rgba(83,86,90,0.5)" : "#A3AED0")};
	font-size: ${({ hasFocus }) => (hasFocus ? "11px" : "14px")};
	padding: ${({ hasFocus }) => (hasFocus ? "2px 4px" : "12px 12px")};
	line-height: 1;
	background: transparent;
	position: absolute;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;
	width: 100%;
	text-align: ${({ right }) => (right ? "right" : "left")};
	z-index: 2;
	transition:
		font-size 150ms ease-in-out,
		padding 100ms ease-in-out;
	pointer-events: none;
`;

const ErrorMessage = styled.em`
	color: #e4002b;
	font-size: 10px;
	line-height: 1.2;
	position: absolute;
	right: 0;
	bottom: -15px;
`;

const WarningMessage = styled.em`
	color: #e48d00;
	font-size: 10px;
	line-height: 1.2;
	position: absolute;
	right: 0;
	bottom: -15px;
`;

const IconFinger = styled.span`
	position: absolute;
	right: 0.5rem;
	top: 10px;
	opacity: 0.7;
	cursor: pointer;
	z-index: 2;
`;

export {
	WrappInput,
	ItemInput,
	Label,
	ErrorMessage,
	WarningMessage,
	IconFinger,
};
