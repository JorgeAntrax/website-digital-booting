import React, { useEffect, useState } from "react";
import { Svg } from "@components";
import { Check, Label, WrapCheck } from "./styles";

import checkEmpty from "@assets/icons/check-empty.svg";
import checkFill from "@assets/icons/check-fill.svg";
import { prop } from "styled-tools";

const CheckBox = ({
	children,
	name,
	id,
	getValue,
	value,
	disabled,
	selected = null,
	...props
}) => {
	const [checked, setChecked] = useState(null);

	useEffect(() => {
		selected !== null && setChecked(selected);
	}, [selected]);

	const handleOnChange = (checked) => {
		setChecked(checked);
		getValue && getValue({ checked, value });
	};

	return (
		<WrapCheck disabled={disabled} {...props}>
			<Label htmlFor={id}>
				<Check
					type="checkbox"
					id={id}
					name={name}
					disabled={disabled}
					onChange={({ target }) => handleOnChange(target.checked)}
					value={value}
					checked={checked}
				/>

				<Svg
					id="checked"
					icon={checkFill}
					w="18px"
					h="18px"
					className="ml:1 mr:05"
				/>
				<Svg
					id="empty"
					icon={checkEmpty}
					w="18px"
					h="18px"
					className="ml:1 mr:05"
				/>
				{children}
			</Label>
		</WrapCheck>
	);
};

export default CheckBox;
