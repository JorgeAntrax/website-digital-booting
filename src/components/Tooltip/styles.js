import styled, { css } from "styled-components";
import { SuperCSS } from "@utils";

const WrappTooltip = styled.span`
	display: inline-block;
	position: relative;
	cursor: pointer;

	&:hover .content {
		visibility: visible;
		opacity: 1;
	}
	${(props) => SuperCSS.hydrate(props)}
`;

const TooltipContent = styled.span`
	display: inline-block;
	padding: 4px;
	color: #fff;
	font-size: 14px;
	line-height: 1.2;
	font-weight: 500;
	min-width: ${({ w }) => w}px;
	width: ${({ w }) => w}px;
	position: absolute;
	background-color: #000;
	z-index: 5;
	cursor: initial;
	text-align: left;
	border-radius: 2px;
	visibility: hidden;
	transition: opacity ${({ delay }) => delay}ms linear;
	pointer-events: none;
	opacity: 0;

	&:before {
		content: "";
		display: inline-block;
		width: 0;
		height: 0;
		border: 6px solid transparent;
		position: absolute;
		pointer-events: none;
	}

	${({ direction }) => {
		return direction === "top"
			? cssTop
			: direction === "left"
			? cssLeft
			: direction === "bottom"
			? cssBottom
			: direction === "right"
			? cssRight
			: "";
	}}
	${(props) => SuperCSS.hydrate(props)}
`;

const cssTop = css`
	bottom: calc(100% + 10px);

	&,
	&:before {
		left: 50%;
		transform: translateX(-50%);
	}

	&:before {
		top: 100%;
		border-top-color: #000;
	}
	${(props) => SuperCSS.hydrate(props)}
`;

const cssBottom = css`
	top: calc(100% + 10px);

	&,
	&:before {
		@media screen and (min-width: 250px) {
			right: 1rem;
			transform: translateX(10%);
		}

		@media screen and (min-width: 768px) {
			left: 50%;
			transform: translateX(-50%);
		}
	}

	&:before {
		bottom: 100%;
		border-bottom-color: #000;
	}
	${(props) => SuperCSS.hydrate(props)}
`;

const cssLeft = css`
	right: calc(100% + 10px);

	&,
	&:before {
		top: 50%;
		transform: translateY(-50%);
	}

	&:before {
		left: 100%;
		border-left-color: #000;
	}
	${(props) => SuperCSS.hydrate(props)}
`;

const cssRight = css`
	left: calc(100% + 10px);

	&,
	&:before {
		top: 50%;
		transform: translateY(-50%);
	}

	&:before {
		right: 100%;
		border-right-color: #000;
	}
	${(props) => SuperCSS.hydrate(props)}
`;

export { WrappTooltip, TooltipContent };
