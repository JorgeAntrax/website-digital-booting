import React, { Component, useEffect, useState } from "react";
import "react-quill/dist/quill.snow.css";

class Editor extends Component {
	constructor(props) {
		super(props);
		this.enableDocument = typeof window !== "undefined";
		this.ReactQuill = this.enableDocument ? require("react-quill") : null;

		this.state = {
			value: "",
		};
	}

	get value() {
		return this.state.value;
	}

	componentDidMount() {
		this.setState({ value: this.props.value });
	}

	render() {
		const ReactQuill = this.ReactQuill;
		if (this.enableDocument && ReactQuill) {
			const modules = {
				toolbar: [
					[{ header: [2, false] }],
					["bold", "italic", "underline", "strike", "blockquote"],
					[
						{ list: "ordered" },
						{ list: "bullet" },
						{ indent: "-1" },
						{ indent: "+1" },
					],
					["link", "image"],
				],
			};

			const formats = [
				"header",
				"bold",
				"italic",
				"underline",
				"strike",
				"blockquote",
				"list",
				"bullet",
				"indent",
				"link",
				"image",
			];

			return (
				<ReactQuill
					placeholder={this.props.placeholder}
					theme="snow"
					value={this.value}
					onChange={(value) => {
						this.setState({ value });
						this.props.getValue(value);
					}}
					modules={modules}
					formats={formats}
				/>
			);
		}

		return null;
	}
}

export default Editor;
