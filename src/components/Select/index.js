import React, { useState, useEffect } from "react";
import { Select, SelectContain } from "./styles";

const change = (e) => {
	return e.target.value;
};

const customSelect = ({
	getValue,
	placeholder,
	options,
	value,
	id,
	disabled,
	defaultValue,
	otherLess,
	excludes,
	hasRequired,
}) => {
	const [exclusions, setExclusions] = useState([]);

	useEffect(() => {
		if (excludes) {
			const idsExcludes = excludes.map((item) => item.id);
			idsExcludes && setExclusions(idsExcludes);

			console.log(idsExcludes);
		}
	}, []);

	return (
		<SelectContain>
			<Select
				value={value}
				onChange={(e) => getValue(change(e))}
				disabled={disabled}
				required
				id={id}
				right={hasRequired ? true : false}
				defaultValue={defaultValue ? defaultValue : ""}
			>
				<option selected value=""></option>
				{options &&
					options.map((option, key) => (
						<option
							key={key}
							value={option.id}
							disabled={exclusions.includes(option.id)}
						>
							{option.name} {exclusions.includes(option.id) ? "(disabled)" : ""}
						</option>
					))}
				{!otherLess && <option value="9999">Other</option>}
			</Select>
			<label htmlFor={id}>{placeholder}</label>

			{hasRequired && <label className="hasRequired">Required</label>}
		</SelectContain>
	);
};

export default customSelect;
