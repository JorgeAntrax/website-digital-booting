import styled from "styled-components";
import { SuperCSS } from "@utils";

const PaginationWrapper = styled.div`
	display: flex;
	align-items: center;
	align-content: center;
	justify-content: center;
	width: 100%;
	padding: 1rem;
	margin-bottom: 2rem;
	${(props) => SuperCSS.hydrate(props)}
`;

const Button = styled.button`
	display: inline-flex;
	align-items: center;
	align-content: center;
	justify-content: center;
	border: none;
	background-color: ${({ active }) => (active ? "black" : "transparent")};
	color: ${({ disabled, active }) =>
		disabled ? "#bbb" : active ? "white" : "black"};
	font-size: inherit;
	width: 40px;
	height: 40px;
	min-width: 40px;
	min-height: 40px;
	cursor: ${({ disabled }) => (disabled ? "not-allowed" : "pointer")};

	&:not(:last-child) {
		margin-right: 0.5rem;
	}

	&:focus {
		outline: 0;
	}

	&:not([disabled]):hover {
		background-color: ${({ active }) => (active ? "black" : "#f6f6f6")};
		color: ${({ active }) => (active ? "white" : "black")};
	}
	${(props) => SuperCSS.hydrate(props)}
`;

export { PaginationWrapper, Button };
