import React, { useState, useEffect, Fragment } from "react";
import { PaginationWrapper, Button } from "./styles";

const Pagination = ({ getValue, total, current }) => {
	const [pages, setPages] = useState([]);

	useEffect(() => {
		const p = [];

		for (let i = 1; i < total; i++) {
			const newPage = i;
			const Page = (
				<Button
					key={i}
					active={current === newPage}
					onClick={() => getValue && getValue(newPage)}
				>
					{newPage}
				</Button>
			);

			p.push(Page);
		}

		const Indicators = <Button disabled>. . .</Button>;
		const LastPage = (
			<Button
				key={total}
				active={current === total}
				onClick={() => getValue && getValue(total)}
			>
				{total}
			</Button>
		);

		const totalPages = [...p, Indicators, LastPage];
		totalPages.length && setPages(totalPages);
	}, [total, current]);

	return (
		<PaginationWrapper>
			{!!pages.length &&
				pages.map((Page, index) => (
					<Fragment key={index}>{Page}</Fragment>
				))}
		</PaginationWrapper>
	);
};

export default Pagination;
