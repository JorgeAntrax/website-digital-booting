import React from "react";
import { Base } from "./styles";
import { Loader, Svg, Tooltip, Text } from "@components";

import search from "@assets/search.svg";
import menu from "@assets/icons/burguer.svg";
import home from "@assets/home.svg";
import mx from "@assets/mx.svg";
import us from "@assets/us.svg";
import account from "@assets/account.svg";
import lovelist from "@assets/lovelist.svg";
import lovelistHover from "@assets/lovelistHover.svg";
import cart from "@assets/cart.svg";
import cartHover from "@assets/cartHoverW.svg";
import close from "@assets/close.svg";
import closew from "@assets/icons/closew.svg";
import back from "@assets/back.svg";
import edit from "@assets/edit.svg";
import trash from "@assets/trash.svg";
import show from "@assets/eye.svg";
import backtitle from "@assets/backtitle.svg";

//social
import facebook from "@assets/social/FacebookLogo.svg";
import instagram from "@assets/social/InstagramLogo.svg";
import twitter from "@assets/social/TwitterLogo.svg";
import whatsapp from "@assets/social/WhatsappLogo.svg";
import youtube from "@assets/social/YoutubeLogo.svg";
import share from "@assets/social/ShareNetwork.svg";

//Methods
import amex from "@assets/methods/amex.svg";
import mastercard from "@assets/methods/mastercard.svg";
import paypal from "@assets/methods/paypal.svg";
import spei from "@assets/methods/spei.svg";
import visa from "@assets/methods/visa.svg";
import arrowl from "@assets/arrow-l.svg";
import arrowr from "@assets/arrow-r.svg";
import arrowLineR from "@assets/arrowLineR.svg";

const dataIcon = {
	menu,
	search,
	home,
	mx,
	us,
	account,
	lovelist,
	lovelistHover,
	cart,
	cartHover,
	close,
	closew,
	back,
	edit,
	trash,
	show,
	backtitle,
	facebook,
	instagram,
	twitter,
	whatsapp,
	youtube,
	share,
	amex,
	mastercard,
	paypal,
	spei,
	visa,
	arrowl,
	arrowr,
	arrowLineR,
};

const ButtonIcon = ({
	children,
	mobile,
	loading,
	icon,
	direction,
	w,
	widthTooltip,
	sizeIcon,
	tooltip,
	small,
	width,
	nohover,
	...props
}) => {
	return (
		<>
			{tooltip && (
				<Tooltip
					direction={direction}
					w={widthTooltip}
					element={
						<Base nohover={nohover} w={width} small={small} icon={icon} {...props}>
							{loading && <Loader w={20} />}
							{icon && (
								<Svg
									icon={dataIcon[icon]}
									w={`${w}px`}
									h={`${w}px`}
								/>
							)}
						</Base>
					}
				>
					<Text fs={10} fw={500} align="center">
						{tooltip}
					</Text>
				</Tooltip>
			)}

			{!tooltip && (
				<Base nohover={nohover} w={width} icon={icon} {...props}>
					{loading && <Loader w={w} />}
					{icon && (
						<Svg icon={dataIcon[icon]} w={`${w}px`} h={`${w}px`} />
					)}
				</Base>
			)}
		</>
	);
};

export default ButtonIcon;
