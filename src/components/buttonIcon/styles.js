import styled, { css } from "styled-components";
import { SuperCSS } from "@utils";

const ligthCss = css`
	background-color: #f6f6f6;
	color: #232525;

	&:hover {
		background-color: #f1f1f1;
	}
`;

const linkCss = css`
	background-color: transparent;
	color: #232525;

	&:hover {
		color: #000;
		background-color: #f8f8f8;
	}
`;

const primaryCSS = css`
	background-color: #006436;
	color: white;

	&:hover {
		background-color: #232525;
	}
`;

const secondaryCSS = css`
	background-color: black;
	color: #fff;
`;

const outlineCSS = css`
	background-color: transparent;
	color: #000;
	border: 1px solid black;

	&:hover {
		border-color: #cbcbcb;
	}
`;

const Base = styled.button`
	display: inline-flex;
	align-items: center;
	align-content: center;
	justify-content: center;
	background-color: transparent;
	border-radius: ${({ square, circle }) => (square ? 0 : circle ? 50 : 5)}px;
	border: 2px solid transparent;
	cursor: pointer;
	font-family: "Poppins";
	font-size: 14px;
	width: ${({ w, small }) => (w ? w : small ? 30 : 40)}px;
	height: ${({ w, small }) => (w ? w : small ? 30 : 40)}px;
	min-width: ${({ w, small }) => (w ? w : small ? 30 : 40)}px;
	min-height: ${({ w, small }) => (w ? w : small ? 30 : 40)}px;
	text-align: center;
	transition: background 200ms ease-in-out;

	svg {
		display: inline-block;
		vertical-align: middle;
	}

	&:disabled,
	&:disabled:hover {
		box-shadow: none;
		background: transparent;
		border: 2px solid #e0e0e0;
		color: #e0e0e0;
		cursor: not-allowed;
	}

	${({ nohover }) =>
		!nohover &&
		css`
			&:hover,
			&:focus,
			&:active {
				background-color: #f6f6f6;
				border: 2px solid transparent;
				color: white;
			}
		`}

	${({ light, primary, secondary, link, outline }) =>
		(link && linkCss) ||
		(light && ligthCss) ||
		(primary && primaryCSS) ||
		(outline && outlineCSS) ||
		(secondary && secondaryCSS)}
		
${(props) => SuperCSS.hydrate(props)}
`;

export { Base };
