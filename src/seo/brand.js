export default {
	title: "Diseño y Creación de Marca con IA en México | Digital Booting",
	description:
		"En Digital Booting utilizamos tecnologías de IA avanzadas para crear una identidad de marca única y atractiva que conecte con tu público objetivo. Solicita una auditoría gratis.",
	siteUrl: "https://digitalbooting.com/creacion-de-marca-con-ia",
	keywords: [
		"diseño de marca con IA",
		"creación de marca con IA",
		"identidad visual",
		"branding profersional",
		"branding profersional con IA",
		"creación de marca con IA",
		"IA en diseño grafico",
		"IA en diseño de marcas",
		"IA en diseño páginas web",
		"inteligencia artificial y branding corporativo",
		"agencia de diseño en México",
		"agencia de diseño con IA en México",
		"asesoria de marca gratis.",
		"asesoria de marca con IA.",
	],
};
