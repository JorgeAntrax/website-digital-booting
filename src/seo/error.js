export default {
	title: "404 | Página no encontrada",
	description: "No hemos encontrado el contenido que buscabas",
	siteUrl: "http://digitalbooting.com/404",
};
