export default {
	title: "Digital Booting - Marketing para Redes Sociales en México",
	description:
		"En Digital Booting somos expertos en marketing para redes sociales en México, Utilizamos soluciones innovadoras que aprovechan todo el potencial de la inteligencia artificial para convertir a desconocidos en clientes. ¡Solicita una auditoría gratis!",
	siteUrl: "https://digitalbooting.com/marketing-en-redes-sociales",
	keywords: [
		"marketing de redes sociales",
		"agencia de marketing en México",
		"inteligencia artificial",
		"redes sociales con IA",
		"creacion de contenido con ia",
		"auditoría gratis",
	],
};
