export default {
	title: "Inteligencia Artificial en Marketing, Mejora tus estrategias con IA | Digital Booting",
	description:
		"Mejora tus estrategias de marketing con IA, te mostramos cómo la integración de tecnología de inteligencia artificial puede mejorar tus estrategias de marketing. Automatiza procesos, personaliza contenido y mejora la experiencia del usuario con IA. ¡Solicita una auditoría gratis y aprende más!",
	siteUrl: "https://digitalbooting.com/marketing-inteligencia-artificial",
	keywords: [
		"inteligencia artificial",
		"marketing + IA",
		"IA méxico",
		"marketing méxico",
		"IA para marketing",
		"automatización",
		"personalización",
		"experiencia del usuario",
		"auditoría gratis",
	],
};
