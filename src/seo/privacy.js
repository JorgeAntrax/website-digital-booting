export default {
	title: "Digital Booting - Aviso de privacidad",
	siteUrl: "http://digitalbooting.com/aviso-de-privacidad",
	description:
		"Digital Booting entiende tu preocupación por el modo en que se utiliza y se comparte tu información personal, y te aseguramos que siempre trataremos tus datos de forma confidencial",
	keywords: [
		"digital booting aviso de privacidad",
		"privacidad de información digital",
		"protegemos tus datos en digital booting",
		"agencia creativa y politicas de datos",
		"agencia de marketing y privacidad de los datos",
	],
};
