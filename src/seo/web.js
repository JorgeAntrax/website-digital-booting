export default {
	title: "Diseño de Páginas Web en México | Digital Booting",
	description:
		"Somo una agencia de diseño de páginas web en méxico, que tiene como objetivo brindar páginas web atractivas y adaptables las tendencias del mercado.",
	siteUrl: "https://digitalbooting.com/agencia-de-paginas-web",
	keywords: [
		"diseño web en méxico",
		"agencia de diseño web en México",
		"agencia de páginas web en México",
		"diseño de páginas web",
		"diseño web profesional",
		"desarrollo de páginas web",
		"desarrollo de páginas web en méxico",
		"desarrollo de páginas web en ciudad juarez",
		"desarrollo de páginas web en cdmx",
		"páginas web en cdmx",
		"páginas web en ciudad juarez",
		"agencia de páginas web en ciudad juarez",
		"auditoría gratis",
		"presencia en línea.",
	],
};
