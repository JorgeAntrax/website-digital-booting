export default {
	title: "Agencia IA, SEO con Inteligencia Artificial IA en México",
	description:
		"Integramos estrategias de posicionamiento SEO basadas en inteligencia artificial para colocar tu negocio en las primeras posiciones de Google. Solicita una auditoría gratis.",
	siteUrl: "https://digitalbooting.com/seo-con-inteligencia-artificial",
	keywords: [
		"SEO",
		"IA + SEO",
		"Seo con IA",
		"Seo con tecnologia de IA",
		"inteligencia artificial en SEO",
		"posicionamiento web",
		"posicionamiento web en méxico",
		"agencia de SEO en México",
		"agencia de SEO con IA en México",
		"auditoría gratis de seo en mexico.",
	],
};
