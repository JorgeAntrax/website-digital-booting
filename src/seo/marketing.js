export default {
	title: "Agencia IA Marketing con Inteligencia Artificial en México",
	description:
		"Digital Booting es una agencia de marketing digital en México. Utilizamos soluciones innovadoras y la inteligencia artificial (IA) para aumentar tu presencia en línea y convertir a desconocidos en clientes cualificados. ¡Haz crecer tu negocio con nosotros!",
	siteUrl: "https://digitalbooting.com/agencia-marketing-digital",
	keywords: [
		"Marketing digital",
		"Marketing digital IA",
		"Marketing IA",
		"Agencia IA Marketing",
		"Agencia IA méxico",
		"Agencia de marketing",
		"México IA",
		"Inteligencia artificial",
		"Presencia en línea",
		"Clientes cualificados",
		"Crecimiento de negocio",
		"Soluciones innovadoras",
	],
};
