String.prototype.toTitleCase = function () {
	return (
		this.replace(/\w\S*/g, function (txt) {
			return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
		}) || ""
	);
};

Array.prototype.purge = function () {
	let filterArray = [];
	for (var i = 0; i < this.length; i++) {
		if (!!this[i]) {
			filterArray.push(this[i]);
		}
	}
	return filterArray;
};

String.prototype.toKebabCase = function () {
	return this.replace(/([a-z0-9]|(?=[A-Z]))([A-Z])/g, "$1-$2").toLowerCase();
};
