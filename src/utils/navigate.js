import { navigate } from "gatsby";

const hasWindow = typeof window !== "undefined";

export default (url) => {
	hasWindow && navigate(url);
};

export const reload = () => {
	hasWindow && window.location.reload();
};

export const openUrl = (url) => {
	hasWindow && window.open(url, "_blank");
};
