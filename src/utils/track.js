export const track = ({ event, category, label, value }) => {
	if (typeof window === "undefined") return;
	window.gtag &&
		window.gtag(event, "click", {
			event_category: category,
			event_label: label,
			value,
		});
};
