const hasWindow = typeof window !== "undefined";

const parseUrl = (key) => {
	if (!hasWindow) return null;
	return new URLSearchParams(window.location.search).get(key);
};

const getLocation = (key) => {
	if (!hasWindow) return null;
	return window.location[key] || null;
};

const getRoutes = () => {
	if (!hasWindow) return null;
	return window.location.pathname.split("/").filter((path) => !!path);
};

export { parseUrl, getLocation, getRoutes };
