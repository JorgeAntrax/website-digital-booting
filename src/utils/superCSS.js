import SuperCSSProps from "@digitalbooting/react-super-css";
import { BREAKPOINTS, BREAKPOINTS_PREFIX } from "@constants";

import values from "@constants/values";
import alias from "@constants/alias";

const SuperCSS = new SuperCSSProps({
	alias,
	values,
	breakpoints: BREAKPOINTS,
	breakpointsPrefix: BREAKPOINTS_PREFIX,
	supportCSSProps: ["backdrop-filter"],
});

export default SuperCSS;
