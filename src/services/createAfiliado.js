import Api from "@services/api";
import { URLS } from "@constants/urls";

const createAfiliado = async (data) => {
	Api.setHeaders({ "Content-Type": "application/json" });
	const response = await Api.post(URLS.CREATE_AFILIADO, data);
	return response;
};

export default createAfiliado;
