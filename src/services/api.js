import { BASE_URL, URLS, ENABLE_ENCRYPTION } from "@constants";

class ApiClient {
	constructor() {
		this._token = null;
		this._options = {};
		this._headers = {
			Accept: "*/*",
			"Content-Type": "application/json",
		};

		this._secure = {
			auth: false,
		};

		this.$headers = {
			Accept: "*/*",
			"Content-Type": "application/json",
		};

		this.$options = {};

		return this;
	}

	get options() {
		return this._options;
	}

	set options(options) {
		this._options = options;
	}

	get secure() {
		return this._secure;
	}

	set secure(secures) {
		this._secure = secures;
	}

	static setOptions(options) {
		this.options = { ...this.options, ...options };
		return this;
	}

	get headers() {
		return this._headers;
	}

	set headers(headers) {
		this._headers = headers;
	}

	static setDefaultHeaders() {
		this.headers = this.$headers;
		this.options = this.$options;
	}

	static middleware(middleware) {
		this.secure = {
			...this.secure,
			[middleware]: true,
		};
		return this;
	}

	static setHeaders(headers) {
		this.headers = { ...this.headers, ...headers };
		return this;
	}

	static deleteHeader(key) {
		const newHeaders = this.headers;
		delete newHeaders[key];
		this.headers = newHeaders;
		return this;
	}

	static setBearerAuth(bearer) {
		this.setHeaders({
			Authorization: `Bearer ${bearer}`,
		});
		return this;
	}

	static setBasicAuth(credentials) {
		this.setHeaders({
			Authorization: `Basic ${btoa(credentials)}`,
		});
		return this;
	}

	static setAuth(token) {
		this.setHeaders({
			Authorization: token,
		});
		return this;
	}

	static async execMiddlewares() {
		for (const middleware in this.secure) {
			middleware === "auth" && this.checkJWToken();
		}
	}

	static async request(path, data = {}) {
		let resolve = null,
			reject = null,
			status = null;

		try {
			const response = await fetch(path, {
				...this.options,
				headers: { ...this.headers },
				body: data instanceof FormData ? data : JSON.stringify(data),
			});

			status = response.status;
			const payload = await response.json();

			if (payload?.status === "Token is Expired") {
				this.destroySession();
				reject = {
					...payload,
					msg: "the sessionn is expired",
					message: null,
				};
			} else {
				if (response.ok) {
					resolve = { ...payload, status };
				} else {
					reject = {
						...payload,
						msg: payload.message,
						message: null,
					};
				}
			}
		} catch (error) {
			reject = { error };
		}

		if (reject) {
			console.error(`Error Consumer Api`, {
				reject,
				resolve,
				data,
				status,
				request: path,
			});
		}

		this.setDefaultHeaders();
		return [resolve, reject];
	}

	static async get(path, options = {}) {
		this.setOptions({
			method: "GET",
			...options,
		});

		this.execMiddlewares();

		const response = await this.request(path);
		return response;
	}

	static async post(path = null, data = {}, options = {}) {
		if (!path) {
			console.error("The request path is invalid");
			return [null, { message: "Invalid request path" }];
		}
		this.setOptions({
			method: "POST",
			...options,
		});

		this.execMiddlewares();

		const response = await this.request(path, data);
		return response;
	}

	static createHashResponse(str, encryptionNumber = 3) {
		if (!ENABLE_ENCRYPTION) {
			console.log(
				"%c The encryption GraphQl-API is disabled",
				"background: red; color: white",
			);
			return str;
		}

		let hash = str;
		for (let i = 0; i < encryptionNumber; i++) {
			hash = btoa(hash);
		}
		return `${hash}${encryptionNumber}`;
	}

	static async requestIp() {
		const response = await fetch("https://api.ipify.org?format=json");
		const res = await response.json();
		return res;
	}

	static async checkJWToken() {
		console.log("middleware AUTH");
		const response = await fetch(URLS.CHECKJWT, {
			method: "GET",
			headers: { ...this.headers },
		});

		if (!response.ok) {
			this.destroySession();
			return;
		}

		const res = await response.json();
		if (res.status !== "Token is valid") {
			this.destroySession();
			return;
		}

		return [true, null];
	}

	static redirect(path) {
		if (typeof window === "undefined") return;
		window.location.href = `${window.location.origin}/${path}`;
	}

	static async logout(email) {
		this.secure = {
			...this.secure,
			auth: false,
		};

		const query = `
			mutation{
				logoutUser(email: "${email}")
			}
		`;

		const [resolve, reject] = await this.post(BASE_URL, { query });
		if (reject) {
			return [null, reject];
		}

		if (!resolve.data.logoutUser) {
			return [null, resolve.errors];
		}

		return [resolve, null];
	}

	static async destroySession(email = "admin@ancaoutfit.com") {
		const [logout, errorLogout] = await this.logout(email);

		if (errorLogout) {
			return [null, errorLogout];
		}

		const error = btoa(
			btoa("your session has expired, for security log in again."),
		);

		this.redirect(`dbadmin?logout=true&error=${error}`);
	}
}

export default ApiClient;
