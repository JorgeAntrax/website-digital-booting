import getBlogBySlug from "./getBlogBySlug";

const getBlogData = async (fingerprint, slug) => {
	const [resolve] = await getBlogBySlug(fingerprint, slug);

	if (resolve.data) {
		const data = {
			...resolve.data,
			image: resolve.data.thumbnail,
		};

		return [true, data];
	}

	return [false, {}];
};

export default getBlogData;
