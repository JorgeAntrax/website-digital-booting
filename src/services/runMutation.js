import Api from "@services/api";
import { URLS } from "@constants";

/** @params
 * query: String
 * token: BearerAuth token
 */
const runMutation = async (data, token) => {
	if(!token || !data.hasOwnProperty("query")) {
		return [null, { message: "The request parameters is invalid" }];
	}

	Api.setBearerAuth(token).middleware("auth");
	Api.setHeaders({ "Content-Type": "application/json" });

	data.query = Api.createHashResponse(data.query);
	if (!data.query) {
		return [null, { message: "invalid encripted request" }];
	}

	const [resolve, reject] = await Api.post(URLS.ADMIN, { ...data });

	if (reject) {
		return [null, reject];
	}

	if (resolve?.errors) {
		return [null, { ...resolve.errors }];
	}

	return [resolve.data, null];
};

export default runMutation;
