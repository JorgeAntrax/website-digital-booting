import Api from "@services/api";
import { URLS } from "@constants/urls";

const sendUTM = async (data) => {
	Api.setHeaders({ "Content-Type": "application/json" });
	const response = await Api.post(URLS.UTM, data);
	return response;
};

export default sendUTM;
