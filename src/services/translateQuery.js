// import * as deepl from "deepl-node";
import Api from "@services/api";
import { DEEPL_AUTH, URLS } from "@constants";

// const translator = new deepl.Translator(DEEPL_AUTH);

const translateQuery = async (text) => {
	const context = JSON.parse(sessionStorage.ancastore) || {};

	if (context.hasOwnProperty("lang")) {
		if (context.lang === "en") {
			console.warn("Translation target is equal to language website");
		}

		Api.setAuth(`DeepL-Auth-Key ${DEEPL_AUTH}`).setHeaders({
			"Content-Type": "application/json",
		});
		const [resolve, reject] = await Api.post(URLS.TRANSLATOR, {
			text,
			target_lang: "en-US",
		});

		if (reject) {
			console.error(reject);
			return null;
		}

		return resolve.translate;
	}

	console.warn("Translation language is not defined");
	return str;
};

export default translateQuery;
