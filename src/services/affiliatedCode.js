import Api from "@services/api";
import { URLS } from "@constants/urls";

const validateCookie = async (data) => {
	Api.setHeaders({ "Content-Type": "application/json" });
	const response = await Api.post(URLS.VALIDATE_AFILIATED_CODE, {
		afiliatedCode: data,
	});
	return response;
};

const getAffiliatedCode = async () => {
	Api.setHeaders({ "Content-Type": "application/json" });
	const response = await Api.post(URLS.GET_AFFILIATED_CODE, {});
	return response;
};

export { validateCookie, getAffiliatedCode };
