import Api from "@services/api";
import { URLS } from "@constants/urls";

const getBlogs = async () => {
	Api.setHeaders({ "Content-Type": "application/json" });
	const response = await Api.get(URLS.BLOGS);
	return response;
};

export default getBlogs;
