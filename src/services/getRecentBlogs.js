import Api from "@services/api";
import { URLS } from "@constants/urls";

const getRecentBlogs = async (fingerprint) => {
	Api.setHeaders({ "Content-Type": "application/json" });
	const response = await Api.post(URLS.RECENT_BLOGS, { fingerprint });
	return response;
};


const getFeatureBlogs = async (fingerprint) => {
	Api.setHeaders({ "Content-Type": "application/json" });
	const [resolve, reject] = await Api.post(URLS.FEATURE_BLOGS, {
		fingerprint,
	});

	if (reject) return [];
	const response = resolve.data.map((blog) => {
		return {
			source: blog.cover,
			title: blog.title,
			description: blog.description,
			slug: blog.slug,
			url: `/blog-de-marketing-digital/${blog.slug}`,
			tag: blog.category,
			label: `${blog.timereading} minutos de lectura`,
		};
	});

	return response;
};

export { getRecentBlogs, getFeatureBlogs };
