import Api from "@services/api";
import { URLS } from "@constants/urls";

const getRecentBlogsBy = async (fingerprint, category) => {
	Api.setHeaders({ "Content-Type": "application/json" });
	const response = await Api.post(URLS.RECENT_BLOGS_BY, {
		fingerprint,
		category,
	});
	return response;
};

export default getRecentBlogsBy;
