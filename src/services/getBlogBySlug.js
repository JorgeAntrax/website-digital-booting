import Api from "@services/api";
import { URLS } from "@constants/urls";

const getBlogBySlug = async (fingerprint, slug) => {
	Api.setHeaders({ "Content-Type": "application/json" });
	const response = await Api.post(URLS.BLOG_BY_SLUG, { fingerprint, slug });
	return response;
};

export default getBlogBySlug;
