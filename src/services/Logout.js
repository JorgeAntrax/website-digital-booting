import Api from "@services/api";

const Logout = async (email) => {
	Api.setHeaders({ "Content-Type": "application/json" });
	const response = await Api.logout(email);
	return response
};

export default Logout;
