import { URLS } from "@constants";

async function loadMarkdown(slug, name) {
	try {
		const res = await fetch(URLS.GET_MARKDOWN_BLOG(slug, name));

		if (!res.ok) {
			throw new Error(`Response failed`);
		}

		return {
			props: {
				markdown: await res.text(),
			},
		};
	} catch (error) {
		return {
			status: 500,
			headers: {},
			props: {},
		};
	}
}

export default loadMarkdown;
