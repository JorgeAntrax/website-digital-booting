import Api from "@services/api";

const NEW_ARRAY_SIZE = 20;

class Upload extends Api {
	constructor() {
		super();
		this._endpoint = null;
	}

	static get endpoint() {
		return this._endpoint;
	}

	static set endpoint(path) {
		this._endpoint = path;
	}

	static setUrl(path) {
		this.endpoint = path;
		return this;
	}

	static async uploadFiles(data) {
		if (data?.files?.length > 20) {
			let filesParts = this.getFilesByParts(data?.files);

			for (let i = 0; i < filesParts.length; i++) {
				const files = filesParts[i];
				const [resolve, reject] = await this.upload({
					...data,
					files,
				});

				if (reject) {
					return [
						null,
						{
							error: 400,
							message: "Error upload file",
							file: files[i],
						},
					];
				}
			}

			return [true, null];
		}

		const [resolve, reject] = await this.upload(data);

		if (reject) {
			return [
				null,
				{ error: 400, message: "Error upload file", file: data.files },
			];
		}

		return [true, null];
	}


	static async upload(data) {
		const formData = new FormData();
		const { files } = data;

		for (let key in data) {
			if (key !== "files") {
				formData.append(key, data[key]);
			}
		}

		files.forEach(async file => {
			formData.append("files[]", file);
		})

		const response = await this.post(this.endpoint, formData);
		return response;
	}

	static getFilesByParts(files) {
		let $array = [];

		for (let i = 0; i < files.length; i += NEW_ARRAY_SIZE) {
			let elements = files.slice(i, i + NEW_ARRAY_SIZE);

			elements = elements.filter(
				(el) =>
					el !== undefined && el !== null && typeof el === "object"
			);

			$array.push(elements);
		}

		return $array;
	}
}

export default Upload;
