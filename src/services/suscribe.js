import Api from "@services/api";
import { URLS } from "@constants/urls";

const suscribe = async (data) => {
	Api.setHeaders({ "Content-Type": "application/json" });
	const response = await Api.post(URLS.SUSCRIBE, data);
	return response;
};

export default suscribe;
