import React, { useEffect, useState, useContext, useMemo } from "react";
import defaultState, { defaultData } from "./defaultState";
import { CONTEXT } from "@constants/config";

const StorageContext = React.createContext(defaultState);

const StorageProvider = ({ children }) => {
	// Estados del contexto
	const [state, setState] = useState(defaultState);

	// Función para actualizar un estado específico
	const updateStore = (key, value) => {
		setState((prevState) => ({ ...prevState, [key]: value }));
	};

	// Función para limpiar el almacenamiento
	const clearStore = () => setState(defaultData);

	// Memorizar el `value` del contexto
	const contextValue = useMemo(
		() => ({ ...state, updateStore, clearStore }),
		[state], // Se actualizará solo cuando `state` cambie
	);

	// Guardar en sessionStorage cuando cambia el estado
	useEffect(() => {
		if (typeof window !== "undefined") {
			sessionStorage.setItem(CONTEXT, JSON.stringify(state));
		}
	}, [state]);

	// Auto-limpiar notificaciones después de 5 segundos
	useEffect(() => {
		if (state.notify?.show) {
			const timer = setTimeout(
				() => updateStore("notify", defaultData.notify),
				5000,
			);
			return () => clearTimeout(timer);
		}
	}, [state.notify]);

	return (
		<StorageContext.Provider value={contextValue}>
			{children}
		</StorageContext.Provider>
	);
};

// Hook personalizado para usar el contexto
const useStorage = () => useContext(StorageContext);

export { StorageProvider, useStorage };
