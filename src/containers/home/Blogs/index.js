import React, { useState, useEffect } from "react";
import { Col, Container, Flex, Text } from "@components";
import { SubTitle, CardBlog } from "@modules";

import { useStorage } from "@storage/store";
import { getRecentBlogs } from "@services";

const Blogs = () => {
	const { fingerprint } = useStorage();
	const [blogs, setBlogs] = useState([]);

	useEffect(() => {
		loadBlogs();
	}, []);

	const loadBlogs = async () => {
		const [resolve, reject] = await getRecentBlogs(fingerprint);
		resolve && setBlogs(resolve.data);
	};

	return (
		<Container pv={50}>
			<Flex justify="center">
				<SubTitle
					items="center"
					justify="center"
					mb={20}
					mdMaxw={800}
					ph={10}
				>
					Mantente al día con las{" "}
					<Text type="strong" color="context">
						últimas tendencias y estrategias
					</Text>{" "}
					de marketing
				</SubTitle>
			</Flex>

			<Flex justify="center" mt={40}>
				{!!blogs.length &&
					blogs.map((blog, key) => (
						<Col key={key} mdMaxw={460} padding={20}>
							<CardBlog {...blog} />
						</Col>
					))}
			</Flex>
		</Container>
	);
};

export default Blogs;
