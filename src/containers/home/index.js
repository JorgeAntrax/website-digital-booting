export { default as Slider } from "./Slider";
export { default as AgenciaCreativa } from "./AgenciaCreativa";
export { default as Soluciones } from "./Soluciones";
export { default as Clientes } from "./Clientes";
export { default as Socios } from "./Socios";
export { default as Asesoria } from "./Asesoria";
export { default as Blogs } from "./Blogs";
