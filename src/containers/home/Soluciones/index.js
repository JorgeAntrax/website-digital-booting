import React from "react";
import { Flex, Col, Text, Button, Container } from "@components";
import { Banner, SubTitle, Card } from "@modules";
import useTranslate from "@src/lang";

import auditorias from "@assets/services/auditorias.png";
import branding from "@assets/services/branding.png";
import coaching from "@assets/services/coaching.png";
import content from "@assets/services/content.png";
import ecommerce from "@assets/services/ecommerce.png";
import estrategias from "@assets/services/estrategias.png";
import graphicdesign from "@assets/services/graphicdesign.png";
import leads from "@assets/services/leads.png";
import movilapps from "@assets/services/movilapps.png";
import paginasweb from "@assets/services/paginasweb.png";
import scrum from "@assets/services/scrum.png";
import seoysem from "@assets/services/seoysem.png";
import sistemas from "@assets/services/sistemas.png";
import socialmedia from "@assets/services/socialmedia.png";
import webdesign from "@assets/services/webdesign.png";

const services = [
	{
		theme: "white",
		icon: webdesign,
		title: "Diseño Web",
		description:
			"Te ayudamos a crear una presencia en línea personalizada, atractiva y adaptable a tus necesidades y a las tendencias del mercado.",
		url: "/agencia-de-paginas-web",
	},
	{
		theme: "dark",
		icon: graphicdesign,
		title: "Diseño Gráfico",
		description:
			"Te ayudamos a comunicar de manera asertiva y visualmente atractiva los valores y objetivos de tu marca.",
		url: "/creacion-de-marca",
	},
	{
		theme: "white",
		icon: branding,
		title: "Creación de marca",
		description:
			"Un aspecto importante para diferenciarte de la competencia y aumentar el valor de tu empresa en el mercado es una buena marca.",
		url: "/creacion-de-marca",
	},
	{
		theme: "dark",
		icon: content,
		title: "Marketing de contenidos",
		description:
			"Te ayudamos a crear y compartir contenido valioso y relevante con el fin de atraer y retener clientes potenciales.",
		url: "/marketing-en-redes-sociales",
	},
	{
		theme: "white",
		icon: leads,
		title: "Generación de leads",
		description:
			"Nos encargamos de aumentar la cantidad de oportunidades de venta que tiene tu negocio, ofreciéndote un retorno de inversión tangible y medible.",
		url: "/agencia-marketing-digital",
	},
	{
		theme: "dark",
		icon: socialmedia,
		title: "Medios Sociales",
		description:
			"Aprovechando las redes sociales podemos recopilar y analizar datos sobre posibles clientes con la finalidad de convertirlos en clientes cualificados.",
		url: "/marketing-en-redes-sociales",
	},
	{
		theme: "white",
		icon: paginasweb,
		title: "Páginas Web",
		description:
			"Una de las principales y más efectivas estrategias de captación de clientes es tener una página web atractiva para dar a conocer tu negocio.",
		url: "/agencia-de-paginas-web",
	},
	{
		theme: "dark",
		icon: ecommerce,
		title: "Comercio electrónico",
		description:
			"Sin duda la mejor manera de expandir tu cartera de clientes actual es mediante una tienda en línea que maximice tu alcance en internet.",
		url: "/agencia-de-paginas-web",
	},
	{
		theme: "white",
		icon: sistemas,
		title: "Sistemas Integrales",
		description:
			"Si lo que buscas es un sistema de auto-gestión ya sea, escolar, empresarial, CRM CMS contáctanos.",
		url: "/agencia-de-paginas-web",
	},
	{
		theme: "dark",
		icon: estrategias,
		title: "Estrategias de Marketing",
		description:
			"Nuestro equipo de expertos en marketing trabaja en estrecha colaboración con nuestros clientes para comprender sus necesidades y desarrollar estrategias efectivas.",
		url: "/agencia-marketing-digital",
	},
	{
		theme: "white",
		icon: auditorias,
		title: "Auditorias",
		description:
			"Te asesoramos en tus procesos empresariales realizando auditorias exhaustivas con el objetivo de incrementar tus activos de negocio.",
		url: "/contacto",
	},
	{
		theme: "dark",
		icon: seoysem,
		title: "SEO & SEM",
		description:
			"¿Tu negocio no aparece en la primer página de Google, nosotros conocemos las estrategias necesarias para impulsarte al primer puesto.",
		url: "/posicionamiento-web-seo",
	},
	{
		theme: "white",
		icon: movilapps,
		title: "Apps Móviles",
		description:
			"Una forma sencilla de hacer sentir a tu cliente parte de tu empresa es con una aplicación móvil, ya sea un tienda en línea, gestionar su información, etc",
		url: "/",
	},
	{
		theme: "dark",
		icon: coaching,
		title: "Coaching empresarial",
		description:
			"Te ayudamos a optimizar y automatizar tus procesos de negocio desde la raíz, tomémonos un café",
		url: "/contacto",
	},
	{
		theme: "white",
		icon: scrum,
		title: "Metodologías ágiles",
		description:
			"Comprendemos la demanda y evolución de los mercados digitales, así que nos adaptamos rápidamente a tus necesidades y las de tu negocio.",
		url: "/agencia-de-paginas-web",
	},
];

const Soluciones = () => {
	const { trans } = useTranslate();

	return (
		<Container pv={100} id="soluciones">
			<Flex justify="center" mb={40} ph={10}>
				<SubTitle items="center" justify="center" mb={20} mdMaxw={900}>
					Acelera tu crecimiento en línea con{" "}
					<Text type="span" color="context">
						nuestras soluciones
					</Text>{" "}
					en marketing digital + IA
				</SubTitle>
			</Flex>

			<Flex flexWrap="wrap" justify="center">
				{services.map((service, key) => (
					<Col padding={20} key={key}>
						<Card {...service} mdMaxw={400} />
					</Col>
				))}
			</Flex>
		</Container>
	);
};

export default Soluciones;
