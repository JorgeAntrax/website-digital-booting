import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { SuperCSS } from "@utils";

import { Col, Container, Flex } from "@components";
import { useWidth } from "@hooks";

import toluca from "@assets/landing/toluca.jpg";
import burdeos from "@assets/landing/burdeos.jpg";
import cis from "@assets/landing/cis.jpg";
import fersa from "@assets/landing/fersa.jpg";
import kohchelli from "@assets/landing/kohchelli.jpg";
import legna from "@assets/landing/legna.jpg";
import getdhub from "@assets/landing/getdhub.jpg";
import leyendas from "@assets/landing/leyendas.jpg";
import mascotas from "@assets/landing/mascotas.jpg";
import tossa from "@assets/landing/tossa.jpg";
import valpa from "@assets/landing/valpa.jpg";
import vinas from "@assets/landing/vinas.jpg";
import wald from "@assets/landing/wald.jpg";
import angelcohweb from "@assets/landing/angelcohweb.jpg";
import novo from "@assets/landing/novo.jpg";
import hoteles from "@assets/landing/hoteles.jpg";
import vita from "@assets/landing/vita.jpg";

const slides = [
	{ image: toluca, url: "https://www.haciendadelasfuentes.ruba.com.mx/" },
	{ image: burdeos, url: "https://burdeosresidencial.ruba.com.mx/" },
	{ image: leyendas, url: "https://www.leyendaslegendarias.com/" },
	{ image: hoteles, url: "https://hotelesenjuarez.com/" },
	{ image: novo, url: "https://novovission.com/" },
	{ image: vita, url: "https://vitasantispa.com/" },
	{ image: kohchelli, url: "https://kohchellisaddles.com/" },
	{ image: tossa, url: "https://tossaresidencial.ruba.com.mx/" },
	{ image: valpa, url: "https://valparaisoresidencial.com/" },
	{ image: vinas, url: "https://vinasdelmarresidencial.com/" },
	{ image: cis, url: "#" },
	{ image: mascotas, url: "#" },
	{ image: angelcohweb, url: "#" },
	{ image: fersa, url: "#" },
	{ image: legna, url: "https://legnainternational.com/" },
	{ image: getdhub, url: "#" },
	{ image: wald, url: "https://wald.com.mx/" },
];

const SliderWrapper = styled.div`
	position: relative;
	display: flex;
	min-width: ${({ slides }) => slides * 100}%;
	height: 100%;

	transition: margin-left 400ms ease;

	${(props) => SuperCSS.hydrate(props)}
`;

const Slideritem = styled.div`
	position: relative;
	overflow: hidden;
	border-radius: 20px;
	box-shadow: 0 6px 30px -8px rgba(0, 0, 0, 0.3);
	height: 100%;
	width: 100%;

	background: url(${({ source }) => source}) no-repeat center;
	background-size: cover;

	transition: all 200ms linear;

	&:hover {
		transform: translateY(-20px);
	}

	${(props) => SuperCSS.hydrate(props)}
`;

const Slider = () => {
	const [active, setActive] = useState(0);
	const { isMobile } = useWidth();

	useEffect(() => {
		const totalSlides = slides.length - 1;
		const timer = setInterval(() => {
			setActive((prevActive) =>
				prevActive >= totalSlides ? 0 : prevActive + 1,
			);
		}, 5000);
		return () => clearInterval(timer);
	}, []);

	return (
		<Container pb={50} ph={50}>
			<Flex items="center" justify="center">
				<Flex
					items="center"
					pb={40}
					mdMaxw={900}
					height={isMobile ? 300 : 600}
				>
					<SliderWrapper
						slides={slides?.length || 1}
						marginLeft={`-${active}00%`}
					>
						{slides.map((slide, key) => (
							<Col
								key={key}
								transform={active !== key ? "scale(0.8)" : ""}
								pointerEvents={active !== key ? "none" : ""}
								flex={1}
							>
								<a href={slide.url} target="_blank">
									<Slideritem
										source={slide.image}
									></Slideritem>
								</a>
							</Col>
						))}
					</SliderWrapper>
				</Flex>
			</Flex>
		</Container>
	);
};

export default Slider;
