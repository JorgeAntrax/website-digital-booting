import React, { memo } from "react";
import { Container, Flex, Col, Text } from "@components";

import { SubTitle } from "@modules";
import useTranslate from "@src/lang";

import dots from "@assets/dots.webp";

const AgenciaCreativa = () => {
	const { trans } = useTranslate();

	return (
		<>
			<SubTitle items="center" justify="center" mb={20} ph={20}>
				<Text type="span">Tu Agencia Creativa en México</Text>
			</SubTitle>

			<Container mb={50}>
				<Flex flexWrap="wrap" items="center">
					<Col xsWidth="100%" mdWidth="50%">
						<Flex
							direction="column"
							items="center"
							justify="center"
							ph={20}
						>
							<Text size={16} mb={40} maxw={500}>
								Como{" "}
								<Text type="strong" color="context">
									agencia creativa de marketing digital
								</Text>{" "}
								nuestro principal servicio es el{" "}
								<Text type="strong" color="context">
									incremento exponencial de nuevos clientes
									para su negocio
								</Text>
								, ayudamos a las empresas a diseñar campañas
								publicitarias en internet y medios sociales
								mediante estrategias de marketing potenciadas
								con inteligencia articial (IA) que si funcionan.
							</Text>
							<Text size={16} maxw={500}>
								Somos una{" "}
								<Text type="strong" color="context">
									iniciativa disruptiva de marketing para
									empresas en México
								</Text>
								, nuestro objetivo es ofrecer estrategias y
								servicios de excelencia priorizando la calidad
								vs cantidad, con más de 100 proyectos exitosos
								en las principales ciudades de México y Estados
								Unidos.
							</Text>
						</Flex>
					</Col>
					<Col
						xsWidth="100%"
						mdWidth="50%"
						padding="4rem 1rem"
						background={`url(${dots}) no-repeat center/100%`}
						backgroundSize="contain"
						backgroundPosition="-10% center"
					>
						<Flex items="center" justify="center" pv="2rem">
							<Col width="150px">
								<Text
									size={40}
									family="Oxygen"
									align="center"
									weight="bold"
								>
									5
								</Text>
								<Text align="center" size={14}>
									Años
								</Text>
							</Col>
							<Col width="150px">
								<Text
									size={40}
									family="Oxygen"
									align="center"
									weight="bold"
								>
									2
								</Text>
								<Text align="center" size={14}>
									Paises
								</Text>
							</Col>
							<Col width="150px">
								<Text
									size={40}
									family="Oxygen"
									align="center"
									weight="bold"
								>
									300
								</Text>
								<Text align="center" size={14}>
									Tazas de café
								</Text>
							</Col>
						</Flex>
						<Flex items="center" justify="center" pv="2rem">
							<Col width="150px">
								<Text
									size={40}
									family="Oxygen"
									align="center"
									weight="bold"
								>
									100
								</Text>
								<Text align="center" size={14}>
									Clientes
								</Text>
							</Col>
							<Col width="150px">
								<Text
									size={40}
									family="Oxygen"
									align="center"
									weight="bold"
								>
									150
								</Text>
								<Text align="center" size={14}>
									Proyectos
								</Text>
							</Col>
							<Col width="150px">
								<Text
									size={40}
									family="Oxygen"
									align="center"
									weight="bold"
								>
									1,350
								</Text>
								<Text align="center" size={14}>
									Horas de soporte
								</Text>
							</Col>
						</Flex>
					</Col>
				</Flex>
			</Container>
		</>
	);
};

export default memo(AgenciaCreativa);
