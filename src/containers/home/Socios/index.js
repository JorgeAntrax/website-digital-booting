import React from "react";
import { Flex, Col, Text, Button, Container, Image } from "@components";
import { Banner, SubTitle, Card } from "@modules";

import { Links } from "@constants";
import useTranslate from "@lang";
import { useWidth } from "@hooks";

import agility from "@assets/services/agility.png";
import results from "@assets/services/results.png";
import strategy from "@assets/services/strategy.png";

const features = [
	{
		icon: agility,
		title: "Agilidad",
		description: (
			<>
				Las necesidades del mercado cambian constantemente y como
				agencia creativa{" "}
				<Text type="strong">nos adaptamos rápidamente</Text>, lo que nos
				permite ofrecer soluciones eficaces para las necesidades de
				nuestros clientes.
			</>
		),
	},
	{
		icon: results,
		title: "Enfocados en resultados",
		description: (
			<>
				Nuestras soluciones funcionan y reflejan un{" "}
				<Text type="strong">retorno de inversión tangible</Text>, no te
				vendemos soluciones nada más por llenar nuestros bolsillos,
				nuestras estrategias están pensadas para que funcionen{" "}
				<Text type="strong">
					en base a tus necesidades reales de negocio
				</Text>
				.
			</>
		),
	},
	{
		icon: strategy,
		title: "Soluciones Estratégicas",
		description: (
			<>
				Mucho más que una agencia creativa en{" "}
				<Text type="strong">Digital Booting</Text> encontrarás un aliado
				digital capaz de brindarle a tu empresa{" "}
				<Text type="strong">
					un amplio abanico de soluciones creativas
				</Text>{" "}
				en marketing digital.
			</>
		),
	},
];

const Socios = () => {
	const { isMobile } = useWidth();
	const { trans } = useTranslate();

	return (
		<Container xsPadding={30} mdPadding={40}>
			<Flex items="center" flexWrap="wrap">
				<Col xsWidth="100%" mdWidth="50%" pv={30}>
					<Flex items="center" justify="center">
						<Col mdMaxw={520}>
							<SubTitle
								mb={20}
								items={isMobile ? "center" : "start"}
								justify="center"
								align={isMobile ? "center" : "left"}
							>
								¿Qué hace diferente a{" "}
								<Text type="span" color="context">
									Digital Booting
								</Text>{" "}
								de otras agencias creativas en méxico?
							</SubTitle>

							<Text align={isMobile ? "center" : ""}>
								Nuestro principal diferenciador es la
								integración de tecnologías de IA{" "}
								{/* <Text color="context" type="strong">
									inteligencia artificial en nuestras
									soluciones digitales
								</Text> */}
								, lo que nos permite ofrecer resultados más
								precisos y efectivos a nuestros clientes.
							</Text>

							<Flex justify={isMobile ? "center" : ""}>
								<Button
									mt="3rem"
									type="link"
									href={Links.$contact}
									theme="context"
								>
									Quiero comenzar
								</Button>
							</Flex>
						</Col>
					</Flex>
				</Col>
				<Col xsWidth="100%" mdWidth="50%" pv={30}>
					<Flex items="center" justify="center">
						<Col mdMaxw={560}>
							{!isMobile &&
								features.map(
									({ title, icon, description }, key) => (
										<Flex
											width="100%"
											padding={"1rem"}
											key={key}
										>
											<Col width={50}>
												<Image
													name={title}
													url={icon}
													dimentions={50}
												/>
											</Col>
											<Col flex={1} pl={30}>
												<Text mdSize={18} mb={20}>
													{title}
												</Text>
												<Col mb={20}>{description}</Col>
											</Col>
										</Flex>
									)
								)}
							{isMobile &&
								features.map(
									({ title, icon, description }, key) => (
										<Flex
											flexWrap="wrap"
											width="100%"
											padding={"1rem"}
											key={key}
										>
											<Flex flexalign="center" md={20}>
												<Image
													name={title}
													url={icon}
													dimentions={60}
												/>
											</Flex>
											<Col width="100%" mt={20}>
												<Text
													mdSize={20}
													mb={20}
													align="center"
												>
													{title}
												</Text>
												<Text align="justify" mb={20}>
													{description}
												</Text>
											</Col>
										</Flex>
									)
								)}
						</Col>
					</Flex>
				</Col>
			</Flex>
		</Container>
	);
};

export default Socios;
