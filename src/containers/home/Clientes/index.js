import React, { memo } from "react";
import { Container, Flex, Col, Text, Box, Image } from "@components";

import { SubTitle } from "@modules";
import useTranslate from "@lang";
import { useWidth } from "@hooks";

import novovission from "@assets/landing/novovission.jpg";
import angelcoh from "@assets/landing/angelcoh.jpg";

import angelcohc from "@assets/customers/angelcoh.png";
import chingon from "@assets/customers/chingon.png";
import cis from "@assets/customers/cis.png";
import crm from "@assets/customers/crm.svg";
import expomascotas from "@assets/customers/expomascotas.svg";
import hoteles from "@assets/customers/hoteles.svg";
import kohchelli from "@assets/customers/kohchelli.png";
import legna from "@assets/customers/legna.png";
import novo from "@assets/customers/novo2.png";
import opticas from "@assets/customers/opticas.svg";
import vita from "@assets/customers/vita.png";
import anca from "@assets/customers/anca-logo-footer.svg";
import drive from "@assets/customers/logo_drive.svg";
import leyendas from "@assets/customers/leyendas.webp";
import natura from "@assets/customers/natura.png";
import ruba from "@assets/customers/ruba.png";
import toluca from "@assets/customers/toluca.png";
import valpa from "@assets/customers/valpa.png";
import tossa from "@assets/customers/tossa.png";
import vinas from "@assets/customers/vinas.png";

const clients = [
	angelcohc,
	chingon,
	cis,
	crm,
	expomascotas,
	hoteles,
	kohchelli,
	legna,
	novo,
	opticas,
	vita,
	anca,
	drive,
	leyendas,
	natura,
	ruba,
	toluca,
	valpa,
	tossa,
	vinas,
];

const Clientes = () => {
	const { trans } = useTranslate();
	const { isMobile } = useWidth();
	return (
		<>
			<Flex background="light" pv="5rem">
				<Container>
					<Flex flexWrap="wrap">
						<Col xsWidth="100%" mdWidth="50%" padding={20}>
							<Flex items="center" justify="center">
								<Col mdMaxw={520}>
									<SubTitle
										mb={20}
										items="start"
										justify="center"
										align="left"
									>
										Clientes que confían en Digital{" "}
										<Text type="span" color="primary">
											Booting
										</Text>
										.
									</SubTitle>

									<Text>
										Así como nuestros clientes tu{" "}
										<Text type="strong">
											puedes confiar en que trabajaremos
											de manera eficiente y efectiva
										</Text>{" "}
										para impulsar tu negocio y que puedas
										lograr tus objetivos.
									</Text>
								</Col>
							</Flex>
						</Col>
						<Col xsWidth="100%" mdWidth="50%" padding={20}>
							<Flex items="center" justify="center">
								<Col mdMaxw={520}>
									<Box
										padding="1rem"
										mb={20}
										background="white"
									>
										<Flex>
											<Col width={60}>
												<Image
													name="luis eddie meneses"
													url={novovission}
													dimentions={60}
													fit="cover"
												/>
											</Col>
											<Col flex={1} pl={20}>
												<Text mb={20}>
													Digital Booting me ha
													ayudado a crecer mi negocio
													de una manera rápida y
													eficiente, siempre dándome
													soluciones y resultados.
												</Text>
												<Text
													align="right"
													size={12}
													color="grey"
													fontStyle="italic"
												>
													E. Meneses - CEO NOVOVISSION
												</Text>
											</Col>
										</Flex>
									</Box>
									<Box
										padding="1rem"
										mb={20}
										background="white"
									>
										<Flex>
											<Col width={60}>
												<Image
													name="angel corral hernandez"
													url={angelcoh}
													dimentions={60}
													fit="cover"
												/>
											</Col>
											<Col flex={1} pl={20}>
												<Text mb={20}>
													Me han ayudado mucho con
													algunos proyectos, me gusta
													la calidad de su trabajo, no
													cambiaría mi agencia de
													marketing por otra.
												</Text>
												<Text
													align="right"
													size={12}
													color="grey"
													fontStyle="italic"
												>
													Angel Corral - CEO
													MAGNETICOH
												</Text>
											</Col>
										</Flex>
									</Box>
								</Col>
							</Flex>
						</Col>
					</Flex>

					<Flex mt={20} ph="4rem" flexWrap="wrap">
						{clients.map((client, index) => (
							<Col
								key={index}
								padding="1rem"
								xsWidth="50%"
								mdFlex="1"
							>
								<Image
									name={`logotipo de cliente ${index}`}
									url={client}
									width={isMobile ? 110 : 150}
									height={isMobile ? 40 : 50}
								/>
							</Col>
						))}
					</Flex>
				</Container>
			</Flex>
		</>
	);
};

export default memo(Clientes);
