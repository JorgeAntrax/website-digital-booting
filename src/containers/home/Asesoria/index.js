import React, { useState } from "react";
import { navigate } from "@utils";
import { useStorage } from "@storage/store";
import { Suscriptor } from "@modules";
import {
	Button,
	Col,
	Container,
	Flex,
	Image,
	Input,
	Title,
	Text,
} from "@components";

import { suscribe } from "@services";
import image from "@assets/suscribe.png";

const Asesoria = () => {
	const { fingerprint, user, location } = useStorage();
	const [email, setEmail] = useState("");
	const [name, setName] = useState("");

	const handleSubmit = async () => {
		const { apellidos, device } = user;
		const { ip, ciudad, codigoPostal, idioma, pais } = location;

		const data = {
			ip: ip,
			country: idioma,
			localty: ciudad,
			postalcode: codigoPostal,
			state: pais,
			name: name || "Prospecto anonimo",
			lname: apellidos,
			email: email,
			phone: "",
			message: `Hola buenas tardes me gustaría una asesoría personalizada para mi negocio`,
			action: "Suscripción de asesría",
			request: `Suscripción de asesoria profesional`,
			fingerprint,
			device,
		};

		const [resolve] = await suscribe(data);
		if (resolve) {
			setEmail("");
			navigate("/gracias-por-contactarnos");
		}
	};

	return (
		<Container ph={20}>
			<Flex justify="center" pv={40}>
				<Suscriptor mdMaxw="90%">
					<Flex flexWrap="wrap" items="center">
						<Col xsWidth="100%" mdWidth="50%" mdPh={20}>
							<Title
								type="h2"
								mb={40}
								weight="normal"
								mdSize={35}
							>
								¡Recibe una asesoría <br />
								<Text
									type="strong"
									mdSize={60}
									color="context"
									lineHeight={1}
								>
									GRATIS!
								</Text>
							</Title>
							<Text mb={30}>
								Mejora tu negocio con nuestra asesoría gratuita
								para empresas y emprendedores ¡Agenda tu
								consulta hoy mismo y{" "}
								<Text type="strong">
									empieza a ver resultados positivos en tu
									empresa!
								</Text>
							</Text>
							<Col pb={20}>
								<Input
									value={name}
									getValue={(v) => setName(v)}
									placeholder="Tu nombre"
								/>
							</Col>
							<Input
								type="email"
								value={email}
								getValue={(v) => setEmail(v)}
								placeholder="Tu correo electrónico"
							/>
							<Button
								mt={30}
								theme="context"
								onClick={() => handleSubmit()}
							>
								Suscribirme
							</Button>
						</Col>
						<Col xsWidth="100%" mdWidth="50%">
							<Image
								url={image}
								xsWidth={300}
								xsHeight={280}
								mdWidth={580}
								mdHeight={480}
								name="asesoria gratuita"
							/>
						</Col>
					</Flex>
				</Suscriptor>
			</Flex>
		</Container>
	);
};

export default Asesoria;
