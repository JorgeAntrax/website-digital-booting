import React from "react";
import { Center, Col, Flex, Image, Svg, Text } from "@components";
import useTranslate from "@lang";

import twitter from "@assets/twittersvg.svg";
import whatsapp from "@assets/whatsapp.svg";
import facebook from "@assets/facebook.svg";

import leo from "@assets/leo.jpg";
import oscar from "@assets/oscar.jpg";
import franz from "@assets/franz.jpg";
import { useWidth } from "@hooks";

const authors = {
	1: {
		image: leo,
		name: "Leonardo Quintana",
		position: "CEO & Socio Fundador",
		biography: `Soy un profesional en tecnología con más de 10 años
							de experiencia aplicando estrategias de marketing
							innovadoras para impulsar el crecimiento de PyMES y
							grandes empresas. En 2017, junto con mis amigos y
							ex-compañeros de trabajo, fundé Digital Booting, una
							empresa que se ha convertido en un proyecto
							emocionante y exitoso.`,
	},
	2: {
		image: franz,
		name: "Francisco Rodriguez",
		position: "CEO & Socio Fundador",
		biography: `Soy un profesional en tecnología con más de 10 años
							de experiencia aplicando estrategias de marketing
							innovadoras para impulsar el crecimiento de PyMES y
							grandes empresas. En 2017, junto con mis amigos y
							ex-compañeros de trabajo, fundé Digital Booting, una
							empresa que se ha convertido en un proyecto
							emocionante y exitoso.`,
	},
	3: {
		image: oscar,
		name: "Oscar Lopez",
		position: "CEO & Socio Fundador",
		biography: `Soy un profesional en tecnología con más de 10 años
							de experiencia aplicando estrategias de marketing
							innovadoras para impulsar el crecimiento de PyMES y
							grandes empresas. En 2017, junto con mis amigos y
							ex-compañeros de trabajo, fundé Digital Booting, una
							empresa que se ha convertido en un proyecto
							emocionante y exitoso.`,
	},
};

const Author = ({ author = 1, blog }) => {
	const { isMobile } = useWidth();
	const user = authors[author];

	const handleShare = (type) => {
		if (typeof window !== "undefined") {
			switch (type) {
				case "whatsapp":
					window.open(
						`https://api.whatsapp.com/send?text=https://digitalbooting.com/blog-de-marketing-digital/${blog}`,
					);
					break;
				case "twitter":
					window.open(
						`https://twitter.com/intent/tweet?url==https://digitalbooting.com/blog-de-marketing-digital/${blog}`,
					);
					break;
				case "facebook":
					window.open(
						`https://www.facebook.com/sharer/sharer.php?u=https://digitalbooting.com/blog-de-marketing-digital/${blog}`,
					);
					break;
			}
		}
	};

	if (isMobile) {
		return (
			<Flex
				items="center"
				flexWrap="wrap"
				direction="column"
				justify="start"
				mt={60}
				pv={30}
				borderTop="1px solid #e3e3e3"
			>
				<Center>
					<Image url={user.image} dimentions={60} borderRadius={60} />
				</Center>

				<Text type="strong" size={16} align="center">
					{user.name}
				</Text>
				<Text size={14} align="center">
					{user.position}
				</Text>

				<Text size={14} mt={20} align="justify">
					{user.biography}
				</Text>

				<Flex items="center" justify="between" mt={30} mb={10}>
					<Text width="50%" size={16}>
						Compartir blog:
					</Text>

					<Flex width="50%" items="center" justify="end">
						<Svg
							ml={20}
							icon={whatsapp}
							wsvg={24}
							onClick={() => handleShare("whatsapp")}
							cursor="pointer"
						/>
						<Svg
							ml={20}
							icon={twitter}
							wsvg={24}
							onClick={() => handleShare("twitter")}
							cursor="pointer"
						/>
						<Svg
							ml={20}
							icon={facebook}
							wsvg={24}
							onClick={() => handleShare("facebook")}
							cursor="pointer"
						/>
					</Flex>
				</Flex>
			</Flex>
		);
	}
	return (
		<Flex
			items="start"
			justify="between"
			mt={60}
			pv={30}
			borderTop="1px solid #e3e3e3"
		>
			<Col flex="2">
				<Flex items="start" display="inline-flex" flexWrap="nowrap">
					<Col pr={20}>
						<Image
							url={user.image}
							dimentions={60}
							borderRadius={60}
						/>
					</Col>
					<Col flex={1}>
						<Text type="strong" size={16}>
							{user.name}
						</Text>
						<Text size={14}>{user.position}</Text>

						<Text size={14} mt={20} align="justify">
							{user.biography}
						</Text>
					</Col>
				</Flex>
			</Col>
			<Col flex="1" textAlign="right">
				<Text
					display="inline-flex"
					items="center"
					justify="end"
					size={16}
					mb={10}
				>
					Compartir blog:{" "}
					<Svg
						ml={20}
						icon={whatsapp}
						wsvg={24}
						onClick={() => handleShare("whatsapp")}
						cursor="pointer"
					/>
					<Svg
						ml={20}
						icon={twitter}
						wsvg={24}
						onClick={() => handleShare("twitter")}
						cursor="pointer"
					/>
					<Svg
						ml={20}
						icon={facebook}
						wsvg={24}
						onClick={() => handleShare("facebook")}
						cursor="pointer"
					/>
				</Text>
			</Col>
		</Flex>
	);
};

export default Author;
