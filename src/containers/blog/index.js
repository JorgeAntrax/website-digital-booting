export { default as Blogs } from "./Blogs";
export { default as Socios } from "./Socios";
export { default as Suscribe } from "./Suscribe";
export { default as RelatedPosts } from "./RelatedPosts";
export { default as Author } from "./Author";
