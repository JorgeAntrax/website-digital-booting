import { useState, useEffect } from "react";
import { getBlogBySlug } from "@services";
import { useStorage } from "@storage/store";

const usePost = () => {
	const { fingerprint } = useStorage();
	const [blogSeo, setBlogSEO] = useState({});
	const [existPost, setExistPost] = useState(null);

	useEffect(() => {
		loadBlog();
	}, []);

	const loadBlog = async () => {
		const [exist, data] = await getBlogData(fingerprint);
		setExistPost(exist);
		exist && setBlogSEO(data);
	};

	return { blogSeo, existPost, slug: blogSeo.slug };
};

const getBlogData = async (fingerprint, slug) => {
	// const [path, slug] = getRoutes();
	const [resolve] = await getBlogBySlug(fingerprint, slug);

	if (resolve.data) {
		const siteUrl =
			typeof window !== "undefined" ? window.location.href : null;

		const data = {
			...resolve.data,
			image: resolve.data.thumbnail,
			siteUrl,
		};

		return [true, data];
	}

	return [false, {}];
};

export default usePost;
export { getBlogData };
