import React, { useEffect, useState } from "react";
import { Col, Flex } from "@components";
import { SubTitle, CardBlog } from "@modules";
import { useStorage } from "@storage/store";
import { getRecentBlogs } from "@services";

const Blogs = () => {
	const { fingerprint } = useStorage();
	const [blogs, setBlogs] = useState([]);

	useEffect(() => {
		loadBlogs();
	}, []);

	const loadBlogs = async () => {
		const [resolve] = await getRecentBlogs(fingerprint);
		resolve && setBlogs(resolve.data);
	};

	return (
		<>
			{!!blogs.length && (
				<>
					<Col pv={50}>
						<Flex justify="center">
							<SubTitle
								items="center"
								justify="center"
								mb={20}
								mdMaxw={800}
							>
								Artículos que podrían interesarte
							</SubTitle>
						</Flex>

						<Flex flexWrap="wrap" justify="center" mt={40}>
							{!!blogs.length &&
								blogs.map((blog, key) => (
									<Col key={key} mdMaxw={460} padding={20}>
										<CardBlog {...blog} />
									</Col>
								))}
						</Flex>
					</Col>
				</>
			)}
		</>
	);
};

export default Blogs;
