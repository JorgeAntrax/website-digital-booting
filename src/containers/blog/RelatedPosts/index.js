import React, { useEffect, useState } from "react";
import { Col, Container, Flex, Text } from "@components";
import { SubTitle, CardBlog } from "@modules";
import { getRecentBlogsBy } from "@services";
import { useStorage } from "@storage/store";

const RelatedPosts = ({ exclude, tag, interested = false }) => {
	const { fingerprint } = useStorage();
	const [blogs, setBlogs] = useState([]);

	useEffect(() => {
		loadBlogs();
	}, []);

	const loadBlogs = async () => {
		const [resolve, reject] = await getRecentBlogsBy(fingerprint, tag);
		resolve && setBlogs(resolve.data.filter((blog) => blog.id !== exclude));
	};

	if (!blogs.length) return null;

	return (
		<Col background="#f6f6f6" pv={40}>
			<Col pv={50}>
				<Flex justify="center">
					<SubTitle
						items="center"
						justify="center"
						mb={20}
						mdMaxw={800}
					>
						Artículos{" "}
						{interested
							? "que podrían interesarte"
							: "relacionados con"}{" "}
						{tag}
					</SubTitle>
				</Flex>

				<Flex flexWrap="wrap" justify="center" mt={40}>
					{blogs.map((blog, key) => (
						<Col key={key} mdMaxw={460} padding={20}>
							<CardBlog {...blog} />
						</Col>
					))}
				</Flex>
			</Col>
		</Col>
	);
};

export default RelatedPosts;
