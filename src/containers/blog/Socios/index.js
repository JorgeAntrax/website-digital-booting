import React from "react";
import { Flex, Col, Text, Button, Container, Image } from "@components";
import { Banner, SubTitle, Card } from "@modules";

import { Links } from "@constants";
import useTranslate from "@lang";
import { useWidth } from "@hooks";

import agility from "@assets/services/agility.png";
import results from "@assets/services/results.png";
import strategy from "@assets/services/strategy.png";

const features = [
	{
		icon: agility,
		title: "Optimización IA",
		description: (
			<>
				Las necesidades del mercado cambian constantemente y como
				agencia creativa{" "}
				<Text type="strong">nos adaptamos rápidamente</Text>, Te
				capacitamos para el{" "}
				<Text type="strong">uso de inteligencia artificial</Text>{" "}
				totalmente gratis, un potenciador más para tu perfil profesional
			</>
		),
	},
	{
		icon: strategy,
		title: "Conviertete en líder de opinión",
		description: (
			<>
				Al escribir sobre temas relevantes y compartir tus ideas con el
				mundo,{" "}
				<Text type="strong">
					puedes convertirte en un líder de opinión
				</Text>{" "}
				en tu campo, si no cuentas con portafolio ahora ya puedes contar
				con uno y respaldado por una{" "}
				<Text type="strong">agencia creativa líder en México</Text>.
			</>
		),
	},
	{
		icon: results,
		title: "Recibe ingresos extra",
		description: (
			<>
				Si tienes habilidades para escribir y quieres compartir tus
				ideas con el mundo mientras generas ingresos extra, da click en
				el botón <Text type="strong">"Quiero unirme al equipo"</Text>.
			</>
		),
	},
];

const Socios = () => {
	const { isMobile } = useWidth();
	const { trans } = useTranslate();

	return (
		<Container xsPadding={30} mdPadding={40}>
			<Flex items="center" flexWrap="wrap">
				<Col xsWidth="100%" mdWidth="50%" pv={30}>
					<Flex items="center" justify="center">
						<Col mdMaxw={520}>
							<SubTitle
								mb={20}
								items={isMobile ? "center" : "start"}
								justify="center"
								align={isMobile ? "center" : "left"}
							>
								Expresa tus ideas de la mano de una{" "}
								<Text type="span" color="context">
									agencia creativa
								</Text>{" "}
							</SubTitle>

							<Text align={isMobile ? "center" : ""}>
								Comparte tus ideas con todo el mundo, si tienes{" "}
								<Text type="strong" color="context">
									habilidades de redacción
								</Text>{" "}
								y te gustaría generar ingresos extra, tenemos el
								lugar perfecto para que puedas expresarte.
							</Text>

							<Flex justify={isMobile ? "center" : ""}>
								<Button
									mt="3rem"
									type="link"
									href={Links.$contact}
									theme="context"
								>
									Quiero unirme al equipo.
								</Button>
							</Flex>
						</Col>
					</Flex>
				</Col>
				<Col xsWidth="100%" mdWidth="50%" pv={30}>
					<Flex items="center" justify="center">
						<Col mdMaxw={560}>
							{!isMobile &&
								features.map(
									({ title, icon, description }, key) => (
										<Flex
											width="100%"
											padding="1rem"
											key={key}
										>
											<Col width={50}>
												<Image
													url={icon}
													dimentions={50}
												/>
											</Col>
											<Col flex={1} pl={30}>
												<Text mdSize={18} mb={20}>
													{title}
												</Text>
												<Col mb={20}>{description}</Col>
											</Col>
										</Flex>
									)
								)}
							{isMobile &&
								features.map(
									({ title, icon, description }, key) => (
										<Flex
											flexWrap="wrap"
											width="100%"
											padding={"1rem"}
											key={key}
										>
											<Flex flexalign="center" md={20}>
												<Image
													url={icon}
													dimentions={60}
												/>
											</Flex>
											<Col width="100%" mt={20}>
												<Text
													mdSize={20}
													mb={20}
													align="center"
												>
													{title}
												</Text>
												<Text align="justify" mb={20}>
													{description}
												</Text>
											</Col>
										</Flex>
									)
								)}
						</Col>
					</Flex>
				</Col>
			</Flex>
		</Container>
	);
};

export default Socios;
