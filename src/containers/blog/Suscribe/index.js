import React, { useState } from "react";
import { navigate } from "@utils";
import { useStorage } from "@storage/store";
import { Suscriptor } from "@modules";
import {
	Button,
	Col,
	Container,
	Flex,
	Image,
	Input,
	Title,
	Text,
} from "@components";

import { suscribe } from "@services";
import image from "@assets/suscribe.png";

const Suscribe = () => {
	const { fingerprint, user, location } = useStorage();
	const [email, setEmail] = useState("");
	const [name, setName] = useState("");

	const handleSubmit = async () => {
		const { apellidos, device } = user;
		const { ip, ciudad, codigoPostal, idioma, pais } = location;

		const data = {
			ip: ip,
			country: idioma,
			localty: ciudad,
			postalcode: codigoPostal,
			state: pais,
			name: name || "Prospecto anonimo",
			lname: apellidos,
			email: email,
			phone: "",
			message: `Hola buenas tardes me gustaría recibir correos electrónicos de su newsletter`,
			action: "Suscripción de email",
			request: `Suscripción de email en blog ${document.title}`,
			fingerprint,
			device,
		};

		const [resolve, error] = await suscribe(data);
		if (resolve) {
			setEmail("");
			navigate("/gracias-por-contactarnos");
		}
	};

	return (
		<Container ph={20}>
			<Flex justify="center" pv={40}>
				<Suscriptor mdMaxw="90%">
					<Flex flexWrap="wrap" items="center">
						<Col xsWidth="100%" mdWidth="50%" mdPh={20}>
							<Title
								type="h2"
								mb={40}
								weight="normal"
								mdSize={38}
							>
								¡Recibe nuestro Newsletter en tu email
							</Title>
							<Text mb={30}>
								Te enviaremos completamente gratis todos
								nuestros blogs directamente a tu correo
								electrónico, puede estar tranquilo(a), no
								llenarémos tu bandeja con SPAM.
							</Text>
							<Col pb={20}>
								<Input
									value={name}
									getValue={(v) => setName(v)}
									placeholder="Tu nombre"
								/>
							</Col>
							<Input
								type="email"
								value={email}
								getValue={(v) => setEmail(v)}
								placeholder="Déjanos tu correo"
							/>
							<Button
								mt={30}
								theme="context"
								onClick={() => handleSubmit()}
							>
								Suscribirme
							</Button>
						</Col>
						<Col xsWidth="100%" mdWidth="50%">
							<Image
								url={image}
								xsWidth={300}
								xsHeight={280}
								mdWidth={580}
								mdHeight={480}
							/>
						</Col>
					</Flex>
				</Suscriptor>
			</Flex>
		</Container>
	);
};

export default Suscribe;
