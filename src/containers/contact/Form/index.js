import React, { useState } from "react";
import { useStorage } from "@storage/store";
import { navigate } from "@utils";

import {
	Box,
	Button,
	Center,
	Col,
	Container,
	Flex,
	Image,
	Input,
	Text,
	Textarea,
} from "@components";

import { Links } from "@constants";
import { REGEX } from "@constants";
import { suscribe } from "@services";

import image from "@assets/contacto.png";
import contact from "@assets/services/contact.png";
import { useWidth } from "@hooks";

const defaultState = {
	name: "",
	email: "",
	message: "",
};

const Form = () => {
	const { isMobile } = useWidth()
	const { fingerprint, user, updateStore, location } = useStorage();

	const [error, setError] = useState(false);
	const [data, setData] = useState(defaultState);

	const update = (key, value) => {
		setData({
			...data,
			[key]: value,
		});
	};

	const handleContact = () => {
		error && setError(false);
		if (!data.name && !REGEX.emailRgx.test(data.email) && !data.message) {
			setError(true);

			setTimeout(() => {
				setError(false);
			}, 8000);
			return;
		}

		handleSubmit();
	};

	const handleSubmit = async () => {
		const { name, email, message } = data;
		const { device } = user;
		const { ip, ciudad, codigoPostal, idioma, pais } = location;

		const formData = {
			ip: ip,
			country: idioma,
			localty: ciudad,
			postalcode: codigoPostal,
			state: pais,
			name: name,
			lname: "",
			email: email,
			phone: "",
			message,
			action: "Envio de formulario",
			request: `se envio el formulario de contacto`,
			fingerprint,
			device,
		};

		const [resolve, error] = await suscribe(formData);
		if (resolve) {
			setData(defaultState);
			updateStore('user', {
				...user,
				nombre: name,
				email,
			});

			navigate("/gracias-por-contactarnos");
		}
	};

	return (
		<Container pb={50} ph={20}>
			<Flex flexWrap="wrap" items="center">
				<Col xsWidth="100%" mdWidth="50%" pv={30} style={{ order: isMobile ? 5 : -1 }}>
					<Center>
						<Image url={image} dimentions="70%" />
					</Center>
					<Center>
						<Text xsSize={12} mdSize={14} align="center" mdMaxw={500}>
							Al llenar este formulario estás de acuerdo con{" "}
							<Text
								href={Links.$privacy}
								type="link"
								color="context"
							>
								nuestra política de privacidad
							</Text>{" "}
							y protección de datos personales.
						</Text>
					</Center>
				</Col>
				<Col xsWidth="100%" mdWidth="50%" pv={30} style={{ order: isMobile ? -1 : 5 }}>
					<Center>
						<Box
							mdMaxw={600}
							xsPadding="1rem"
							mdPadding="2rem"
							boxShadow="0 6px 30px -10px rgba(0,0,0,0.1)"
						>
							<Container>
								<Center mdMb={20}>
									<Image url={contact} dimentions={70} />
								</Center>

								<Text
									size={22}
									align="center"
									weight="400"
									xsMb={20}
								>
									¿Cómo te contactamos?
								</Text>

								<Col padding="0.5rem">
									<Input
										value={data.name}
										placeholder="Tu nombre"
										getValue={(v) => update("name", v)}
									/>
								</Col>
								<Col padding="0.5rem">
									<Input
										type="email"
										value={data.email}
										placeholder="Tu correo electrónico"
										getValue={(v) => update("email", v)}
									/>
								</Col>
								<Col padding="0.5rem">
									<Textarea
										value={data.message}
										placeholder="¿Cómo te podemos ayudar?"
										getValue={(v) => update("message", v)}
									/>
								</Col>

								{error && (
									<Text size={12} align="center" color="red">
										todos los campos son obligatorios,
										porfavor completa el formulario y vuelve
										a intentarlo.
									</Text>
								)}

								<Center mt={20}>
									<Button
										theme="context"
										onClick={() => handleContact()}
									>
										Enviar mensaje
									</Button>
								</Center>
							</Container>
						</Box>
					</Center>
				</Col>
			</Flex>
		</Container>
	);
};

export default Form;
