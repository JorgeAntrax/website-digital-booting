import React from "react";
import { Flex, Col, Text, Button, Container, Image } from "@components";
import { Banner, SubTitle, Card } from "@modules";

import { Links } from "@constants";
import useTranslate from "@lang";

import agility from "@assets/services/agility.png";
import results from "@assets/services/results.png";
import strategy from "@assets/services/strategy.png";
import { useWidth } from "@hooks";

const features = [
	{
		icon: agility,
		title: "Capacitación agil",
		description: (
			<Text>
				Te capacitamos en <Text type="strong">marketing digital</Text> e{" "}
				<Text type="strong">inteligencia artifical</Text>
				para que no sólo des a conocer nuestras soluciones con tus
				familiares, nuestro objetivo es atraer a clientes nacionales e
				internacionales.
			</Text>
		),
	},
	{
		icon: strategy,
		title: "Estrategias de Contenido ",
		description: (
			<Text>
				En <Text type="strong">Digital Booting</Text> te capacitamos en
				la{" "}
				<Text type="strong" color="context">
					generación de contenido
				</Text>{" "}
				para que generes alto impacto en tus estrategias orientando la
				<Text type="strong">
					comunicación al perfil de clientes que estamos buscando
				</Text>
				.
			</Text>
		),
	},
	{
		icon: results,
		title: "Ganas tú y ganamos nosotros",
		description: (
			<Text>
				Por cada contrato cerrado usando tu{" "}
				<Text type="strong">código único de asociado</Text>, tu te
				llevas del <Text type="strong">5%</Text> al{" "}
				<Text type="strong">20%</Text> del total de la factura, podrás
				visualizar tus comisiones directamente en tu perfil y cobrarlas
				cuando quieras.
			</Text>
		),
	},
];

const MarketingExpert = () => {
	const { isMobile } = useWidth();
	const { trans } = useTranslate();

	return (
		<Container xsPadding={30} mdPadding={40}>
			<Flex items="center" flexWrap="wrap">
				<Col xsWidth="100%" mdWidth="50%" pv={30}>
					<Flex items="center" justify="center">
						<Col mdMaxw={520}>
							<SubTitle
								mb={20}
								items={isMobile ? "center" : "start"}
								justify="center"
								align={isMobile ? "center" : "left"}
							>
								No necesitas ser un experto en marketing
							</SubTitle>

							<Text align={isMobile ? "center" : ""}>
								Te capacitamos para que aumentes tus ingresos
								sin ninguna inversión inicial.
							</Text>

							<Flex justify={isMobile ? "center" : ""}>
								<Button
									mt="3rem"
									type="link"
									href="#registroAsociados"
									theme="context"
								>
									Quiero comenzar
								</Button>
							</Flex>
						</Col>
					</Flex>
				</Col>
				<Col xsWidth="100%" mdWidth="50%" pv={30}>
					<Flex items="center" justify="center">
						<Col mdMaxw={560}>
							{!isMobile &&
								features.map(
									({ title, icon, description }, key) => (
										<Flex
											width="100%"
											padding="1rem"
											key={key}
										>
											<Col width={50}>
												<Image
													name={title}
													url={icon}
													dimentions={50}
												/>
											</Col>
											<Col flex={1} pl={30}>
												<Text mdSize={18} mb={20}>
													{title}
												</Text>
												<Col mb={20}>{description}</Col>
											</Col>
										</Flex>
									)
								)}

							{isMobile &&
								features.map(
									({ title, icon, description }, key) => (
										<Flex
											flexWrap="wrap"
											width="100%"
											padding={"1rem"}
											key={key}
										>
											<Flex flexalign="center" md={20}>
												<Image
													name={title}
													url={icon}
													dimentions={60}
												/>
											</Flex>
											<Col width="100%" mt={20}>
												<Text
													mdSize={20}
													mb={20}
													align="center"
												>
													{title}
												</Text>
												<Text align="justify" mb={20}>
													{description}
												</Text>
											</Col>
										</Flex>
									)
								)}
						</Col>
					</Flex>
				</Col>
			</Flex>
		</Container>
	);
};

export default MarketingExpert;
