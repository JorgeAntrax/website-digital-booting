function useNumberFormat() {
	function formatCurrency(number) {
		const formatter = new Intl.NumberFormat("es-MX", {
			style: "currency",
			currency: "MXN",
		});

		return formatter.format(number);
	}

	return { formatCurrency };
}

export default useNumberFormat;
