import { useEffect } from "react";
import { v4 as uuidv4 } from "uuid";
import { useStorage } from "@storage/store";
import { getUserIp } from "@services";
import { useLocation, useUTM } from "@hooks";
import { sha256 } from "@utils";

const useSession = () => {
	const {
		user,
		location,
		fingerprint,
		updateStore,
	} = useStorage();

	useUTM();
	const getIpAddress = async () => {
		const uuid = uuidv4();
		const ip = await getUserIp();
		return sha256(`${uuid}::${ip}`);
	};

	useEffect(() => {
		if (typeof window === "undefined") return;
		const device = window.navigator.userAgent.split(")")[0];

		const loadInitialData = async () => {
			const ubicacion = await useLocation();
			device && updateStore('user', { ...user, device });

			ubicacion &&
				updateStore('location', {
					...location,
					...ubicacion,
				});
		};

		const load = async () => {
			const hash = await getIpAddress();
			updateStore('fingerprint', hash);
		};

		!fingerprint && load();
		loadInitialData();
	}, []);

	return { fingerprint };
};

export default useSession;
