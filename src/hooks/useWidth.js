import { useEffect, useState, useCallback, useMemo } from "react";

const useWidth = () => {
	const hasWindow = typeof window !== "undefined";
	const [dimentions, setDimentions] = useState({
		width: 0,
		height: 0,
		isMobile: null,
	});

	const getDimensions = useCallback(() => {
		const width = hasWindow ? window?.innerWidth : 0;
		const height = hasWindow ? window?.innerHeight : 0;

		return {
			width,
			height,
			isMobile: width < 768,
		};
	}, []);

	const memoizedDimensions = useMemo(() => getDimensions(), [getDimensions]);

	useEffect(() => {
		function handleResize() {
			const dimentions = getDimensions();
			setDimentions(dimentions);
		}

		handleResize();
		hasWindow && window.addEventListener("resize", handleResize);
		return () =>
			hasWindow && window.removeEventListener("resize", handleResize);
	}, [memoizedDimensions]);

	return dimentions;
};

export default useWidth;
