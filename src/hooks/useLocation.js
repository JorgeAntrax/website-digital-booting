export const useLocation = () =>
	new Promise((resolve, reject) => {
		fetch("https://api.ipify.org/?format=json")
			.then((r) => r.json())
			.then(({ ip }) => {
				fetch("https://ipinfo.io/".concat(ip, "/json"))
					.then((r) => r.json())
					.then(({ city, country, postal, loc, region }) => {
						const [lat, lng] = loc?.split(",") || [0, 0];
						resolve({
							ip,
							ciudad: city,
							idioma: country,
							pais: region,
							codigoPostal: postal,
							google: { lat, lng },
						});
					})
					.catch((err) => {
						console.error(err);
						resolve({ ip });
					});
			})
			.catch((err) => {
				console.error(err);
				resolve(null);
			});
	});
export default useLocation;
