import { graphql, useStaticQuery } from "gatsby";

const defaultJsonld = {
	"@context": "https://schema.org",
	"@type": ["Corporation", "Website"],
	name: "Digital Booting LLC",
	alternateName: "Digital Booting Agencia Creativa",
	url: "https://digitalbooting.com",
	logo: "https://digitalbooting.com/static/logo.png",
	sameAs: [
		"https://www.facebook.com/DigitalBootingOficial",
		"https:/instagram.com/digitalbootingoficial",
	],
	potentialAction: {
		"@type": "SearchAction",
		target: "https://digitalbooting.com/search?q={search_term_string}",
		"query-input": "required name=search_term_string",
	},
};

const useSiteMetadata = () => {
	const data = useStaticQuery(graphql`
		query {
			site {
				siteMetadata {
					author
					description
					image
					keywords
					agencyName
					siteUrl
					title
					typeContent
				}
			}
		}
	`);

	data.site.siteMetadata.jsonld = defaultJsonld;
	return data.site.siteMetadata;
};

export default useSiteMetadata;
