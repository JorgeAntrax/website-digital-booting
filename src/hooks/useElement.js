import React, { useEffect, useState } from "react";
import useWidth from "./useWidth";

const useElement = (ref) => {
	const [hasScroll, setHasScroll] = useState(false);
	const { width } = useWidth();

	useEffect(() => {
		if (ref && ref.current) {
			const isScroll = ref.current.scrollWidth > ref.current.clientWidth;
			setHasScroll(isScroll);
		}
	}, [ref, width]);

	return { hasScroll };
};

export default useElement;
