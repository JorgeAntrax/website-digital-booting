import { useState } from "react";

const existDocument = typeof document !== "undefined"

const getItem = (key) => {
	if(!existDocument) return;
	const cookies = document.cookie.split(";").map((cookie) => cookie.trim());
	for (const cookie of cookies) {
		const [cookieKey, cookieValue] = cookie.split("=");
		if (cookieKey === key) {
			return decodeURIComponent(cookieValue);
		}
	}
	return null;
};

const setItem = (key, value, numberOfDays) => {
	if(!existDocument) return;
	const now = new Date();
	now.setTime(now.getTime() + numberOfDays * 60 * 60 * 24 * 1000);
	document.cookie = `${encodeURIComponent(key)}=${encodeURIComponent(
		value
	)}; expires=${now.toUTCString()}; path=/; domain=digitalbooting.com; samesite=strict; secure;`;
};

const removeItem = (key) => {
	if(!existDocument) return;
	document.cookie = `${key}=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/`;
};

const useCookie = (key, defaultValue = 30) => {
	const getCookie = () => getItem(key) || defaultValue;
	const [cookie, setCookie] = useState(getCookie());

	const updateCookie = (value, numberOfDays) => {
		if (value === null) {
			removeItem(key);
			setCookie(null);
		} else {
			setCookie(value);
			setItem(key, value, numberOfDays);
		}
	};

	return [cookie, updateCookie];
};

export default useCookie
