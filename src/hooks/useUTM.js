import { useEffect } from "react";
import { useStorage } from "@storage/store";
import { sendUTM } from "@services";

const useUTM = () => {
	const { utm, updateStore } = useStorage();

	useEffect(() => {
		if (typeof window === "undefined") return;
		let utmData = {
			url: window.location.pathname,
		};

		if (window.location.search) {
			const UTM = new URLSearchParams(window.location.search);

			utmData = {
				...utmData,
				utm_id: UTM.get("utm_id") || utmData.utm_id,
				utm_name: UTM.get("utm_name") || utmData.utm_name,
				utm_source: UTM.get("utm_source") || utmData.utm_source,
				utm_content: UTM.get("utm_content") || utmData.utm_content,
				utm_keywords: UTM.get("utm_keywords") || utmData.utm_keywords,
				utm_term: UTM.get("utm_term") || utmData.utm_term,
				utm_campaign: UTM.get("utm_campaign") || utmData.utm_campaign,
				utm_medium: UTM.get("utm_medium") || utmData.utm_medium,
			};
		}

		updateStore("utm", { ...utm, ...utmData });
	}, []);

	useEffect(() => {
		if (utm.sender) return;
		const load = async () => {
			if (typeof window === "undefined") return;
			const response = await sendUTM(utm);
			console.log(response);
			setUTM({
				...utm,
				sender: true,
			});

			window.history.pushState(
				{},
				"Agencia de Marketing Digital en México 🚀 Digital Booting",
				window.location.origin,
			);
		};
		load();
	}, [utm]);

	return utm;
};

export default useUTM;
