const lazyImages = () => {
	setTimeout(() => {
		if (typeof window !== "undefined") return false;

		const lazyImages = document.querySelectorAll("img.lazy");

		let lazyImageObserver = new IntersectionObserver(function (
			entries,
			observer
		) {
			entries.forEach(function (entry) {
				console.log(entry);
				if (entry.isIntersecting) {
					let lazyImage = entry.target;
					lazyImage.src = lazyImage.dataset.src;
					lazyImage.srcset = lazyImage.dataset.srcset;
					lazyImage.classList.remove("lazy");
					lazyImageObserver.unobserve(lazyImage);
				}
			});
		});

		lazyImages.forEach((lazyImage) => {
			lazyImageObserver.observe(lazyImage);
			console.log("entro");
		});
	}, 200);
};

export default lazyImages;
