import React from "react";
import { Text, Title, Center, Image, Svg, Flex, Col } from "@components";

import codigo from "./assets/codigo.jpg";
import codigo3 from "./assets/codigo3.jpg";
import inversion from "./assets/inversion.jpg";
import seo from "./assets/seo.jpg";
import ux from "./assets/ux.jpg";

const RenderPost = () => {
	return (
		<>
			<Text mb={20} align="justify">
				Si en estos momentos tuvieras la oportunidad de navegar por la
				primera página web desarrollada en el{" "}
				<Text type="strong">CERN</Text>, el Centro Europeo de Física
				Nuclear ubicado en Ginebra (Suiza) que vio la luz por primera
				vez en agosto de 1991 de la mano del ingeniero y físico
				británico <Text type="strong">Tim Berners-Lee</Text> como un
				sistema de intercambio de datos entre los 10.000 científicos que
				trabajaban en la institución.
			</Text>

			<Text mb={20}>
				Puedo asegurarte que estarías completamente decepcionado(a).
			</Text>

			<Text mb={20} align="justify">
				Han pasado más de 30 años desde aquel icónico día, actualmente
				las páginas web han evolucionado tanto que pasaron de ser un
				sencillo sistema para el intercambio de datos entre científicos
				a convertirse en complejos sistemas almacenados en la nube
				utilizados para todo tipo de actividades alrededor de todo el
				mundo.
			</Text>

			<Text mb={20} align="justify">
				Los sitios web se han convertido en un elemento esencial para la
				operación de marcas y empresas, en estos momentos podemos
				afirmar que un sitio web es uno de los pilares que cualquier
				negocio debe de tomar en cuenta antes de incursionar en el mundo
				digital, iniciar en internet sin un sitio web es como lanzarse
				al vacío con los ojos vendados.
			</Text>

			<Text mb={20} align="justify">
				Debido a la inmensa demanda que las páginas web han representado
				en la industria desde hace más de una década, han surgido
				diversas áreas de estudio y aplicación, una de las principales y
				más mencionadas es el <Text type="strong">desarrollo web</Text>,
				pero{" "}
				<Text type="strong">
					¿Por qué es tan importante el desarrollo web para las
					empresas de hoy en día?
				</Text>
			</Text>

			<Col xsHeight={270} mdHeight={700} mdPh={20}>
				<Image
					borderRadius={10}
					fit="cover"
					url={codigo}
					name="código de programación"
					width="100%"
				/>
			</Col>

			<Text xsSize={12} opacity={0.5} pt={10}>
				Imagen utilizada de{" "}
				<Text
					type="link"
					href="https://www.pexels.com/es-es/foto/ordenador-portatil-oficina-internet-tecnologia-177598/"
					color="context"
				>
					pexels.com
				</Text>
			</Text>

			<Title type="h2" mv={20}>
				¿Qué es el Desarrollo Web?
			</Title>

			<Text mb={20} align="justify">
				Desarrollo web significa construir o mejor dicho codificar
				aplicaciones y sitios basados en tecnologías web, así como
				brandar el mantenimiento y funcionalidad adecuada, es a partir
				de esta área de conocimiento que surgen nuevas ramas de estudio
				como el <b>diseño web</b>, la UX (Experiencia de usuario), la UI
				(diseño de interfaces de usuario), El posicionamiento SEO entre
				muchas otras, de las cuales un programador o desarrollador web
				se hace cargo.
			</Text>

			<Title type="h2" mv={20}>
				¿Qué es un desarrollador Web?
			</Title>

			<Text mb={20}>
				tomando en cuenta la definición oficial podemos decir que:
			</Text>

			<Col padding={20} borderRadius={10} mb={30} background="#f6f6f6">
				<i>
					Un desarrollador web es alguien que a partir de un análisis
					previo y un diseño web creado por un equipo de diseño o por
					el mismo escribe miles y miles de líneas de código complejo,
					utilizando una variedad de lenguajes de programación,
					construyendo así un sitio web, aplicación o sistema.
				</i>
			</Col>

			<Text mb={20} align="justify">
				Por lo general un usuario desconoce que detrás de ese sitio web
				bonito y atractivo visualmente existe un grupo de personas con
				habilidades en desarrollo y diseño muy específicas, que son
				responsables de uno o varios proyectos y hacen posible todo lo
				que hoy en día existe en internet.
			</Text>

			<Text mb={20} align="justify">
				Los desarrolladores construyen infinidad sitios web en una gran
				variedad de lenguajes de programación tanto del lado del cliente
				(Frontend), como del lado del servidor (Backend).
				<br />
				<br />
				Sus responsabilidades incluyen el diseño y el desarrollo de
				cualquier aplicativo o producto de software para satisfacer las
				necesidades de los usuarios finales. Una vez que se completa el
				proyecto, es posible que el desarrollador deba mantener,
				actualizar y verificar problemas técnicos en el sitio con el fin
				de asegurar su correcto funcionamiento.
			</Text>

			<Col xsHeight={270} mdHeight={700} mdPh={20}>
				<Image
					borderRadius={10}
					fit="cover"
					url={codigo3}
					name="tipos de desarroladores"
					width="100%"
				/>
			</Col>

			<Text xsSize={12} opacity={0.5} pt={10}>
				Imagen utilizada de{" "}
				<Text
					type="link"
					href="https://www.pexels.com/es-es/foto/hombre-gente-mujer-creativo-10050661/"
					color="context"
				>
					pexels.com
				</Text>
			</Text>

			<Title type="h3" mv={20}>
				Tipos de Desarrolladores Web
			</Title>

			<Text mb={20} align="justify">
				Existen claramente diferentes tipos de desarrolladores que se
				especializan en diferentes disciplinas. En resumen, los grandes
				proyectos web suelen ser colaboraciones entre varios
				desarrolladores profesionales. Algunos manejan todos los
				aspectos de la creación de sitios web, mientras que otros se
				especializan en aspectos específicos.
			</Text>

			<Text mb={20}>Entre los más importantes tenemos:</Text>

			<ul>
				<li>
					<Text type="strong">Front-end developer:</Text> El
					desarrollador web front-end es responsable de la apariencia
					del sitio web y la forma en que los usuarios ven e
					interactúan con la página. Crean diseños de sitios web e
					integran características, aplicaciones y contenido.
				</li>
				<li>
					<Text type="strong">Back-end developer:</Text> El
					desarrollador web back-end es responsable de la construcción
					técnica general del sitio web. Crean el marco básico para un
					sitio web y se aseguran de que funcione correctamente.
				</li>
				<li>
					<Text type="strong">Full Stack developer:</Text> El Full
					Stack Developer se encarga de trabajar tanto en la parte
					visual que utiliza el usuario final como en el back-end del
					sitio web o aplicación.
				</li>
				<li>
					<Text type="strong">Webmasters:</Text> Los webmasters
					mantienen sus sitios web actualizados. Se aseguran de que
					funcionen correctamente y verifican errores como enlaces
					rotos y problemas de seguridad.
				</li>
			</ul>

			<Text mb={20} align="justify">
				Pero <Text type="strong">¡basta ya de tanta historia!</Text>{" "}
				veamos realmente cual es el papel que desempeña el desarrollo
				web dentro de este nuevo mundo digital lleno de publicidad en el
				que tantas empresas hoy en pleno verano del 2022 buscan su
				crecimiento, exacto estoy hablando del Marketing Digital.
			</Text>

			<Title type="h2" mv={20}>
				El papel del Desarrollo Web en el Marketing digital
			</Title>

			<Text mb={20} align="justify">
				Antes que nada, te recomiendo leer nuestro post que habla de
				<Text type="link" href="">
					que es el marketing digital
				</Text>
				, para retroalimentar un poco más lo que vas a leer a
				continuación.
			</Text>

			<Text mb={20} align="justify">
				El desarrollo web juega unos de los papeles más importantes hoy
				en día en cualquier estrategia de Marketing, pues de cara a
				internet (entrando un poco en contexto y refiriéndonos a
				términos marketeros) El desarrollo web nos provee una
				herramienta por la que tus posibles clientes pueden entrar a
				consumir tus productos o servicios, ¡así es! estamos hablándote
				de un sitio web, aplicación o sistema digital.
			</Text>

			<Text mb={20} align="justify">
				Dentro de la jerga de Marketing existen diferentes productos
				(llamémosle producto al software creado por alguna persona o
				Agencia creativa) en los que el desarrollo web tiene su
				increíble participación, sin dejar de mencionarte que es casi
				del 100%:
			</Text>

			<ul>
				<li>
					<Text type="strong">Aplicaciones Web:</Text> Son programas
					que se ejecutan en el navegador y tienen como objetivo
					solucionar problemáticas específicas Ej. Asana (gestión de
					tareas), Gmail (gestionar correos electrónicos), etc.
				</li>
				<li>
					<Text type="strong">Landing Pages:</Text> Son páginas web
					estáticas que por lo general presentan algún producto o
					servicio, su principal función es la de obtener información
					de posibles clientes (Leads cualificados), como su nombre,
					correo electrónico, teléfono, etc. Para posteriormente
					iniciar una labor de venta.
				</li>
				<li>
					<Text type="strong">E-Commerce:</Text> Nos referimos a una
					tienda virtual, que tiene como principal objetivo ser un
					canal de ventas digitales, Ej. Amazon, Mercado libre, Ebay,
					entre otros.
				</li>
				<li>
					<Text type="strong">Plataformas a la medida:</Text>{" "}
					Categorizo dentro de este grupo a todos aquellos proyectos
					como sitios web educativos, sistemas de gestión,
					automatización de procesos, servicios, blogs, etc.
				</li>
			</ul>

			<Text mb={20} align="justify">
				Sin duda el desarrollo web está en todas partes directa o
				indirectamente y no podemos dejar a un lado todos los beneficios
				que aporta a las estrategias de Marketing Digital.
			</Text>

			<Title type="h2" mv={20}>
				¿Cuáles son los beneficios que aporta el Desarrollo Web en
				Marketing Digital?
			</Title>

			<Text mb={20} align="justify">
				Antes de siquiera mencionarte una lista de beneficios creo que
				lo principal es contar con una herramienta digital que te
				permite tener autoridad en internet y que tus posibles clientes
				estén al tanto de tu existencia, pero mucho más allá de tener un
				sitio web aquí te cuento sobre los principales beneficios:
			</Text>

			<Col xsHeight={270} mdHeight={700} mdPh={20}>
				<Image
					borderRadius={10}
					fit="cover"
					url={ux}
					name="Diseño UI & UX"
					width="100%"
				/>
			</Col>

			<Text xsSize={12} opacity={0.5} pt={10}>
				Imagen utilizada de{" "}
				<Text
					type="link"
					href="https://www.pexels.com/es-es/foto/persona-escribiendo-en-papel-blanco-3471423/"
					color="context"
				>
					pexels.com
				</Text>
			</Text>

			<Title type="h3" mv={20}>
				1.- Contribuye a mejorar la usabilidad y experiencia de usuario.
			</Title>

			<Text mb={20} align="justify">
				Cuando hablamos de las mejoras en usabilidad y experiencia de
				usuario nos referimos como su nombre lo indica: a la experiencia
				que tiene el usuario al navegar sobre nuestro sitio web,
				aplicación, sistema etc. Y a la facilidad de uso del mismo.
			</Text>

			<Text mb={20} align="justify">
				Esto es muy importante porque influye directamente en la
				decisión de compra de tus posibles clientes. Es por esto que uno
				de tus objetivos debe ser, hacer sentir cómodos a los usuarios
				dentro de tu sitio. Para esto, la web debe ser intuitiva y fácil
				de utilizar.
			</Text>
			<Text mb={20} align="justify">
				Este es uno de los beneficios si no es que el más importante que
				aporta el desarrollo web.
			</Text>

			<Title type="h3" mv={20}>
				2.- Posicionamiento SEO.
			</Title>

			<Flex flexWrap="wrap">
				<Col xsWidth="100%" mdWidth="50%">
					<Text mb={20} align="justify">
						Hablando de beneficios importantes, para que tus
						posibles clientes puedan ver tu sitio web internet y
						encontrarte fácilmente el gigante de Internet Google,
						necesita que cada sitio sin excepción alguna siga
						ciertas directivas de desarrollo que aseguran el
						correcto escaneo de tu sitio web por parte de sus
						robots, lo que permite que tu sitio web se muestre en
						los resultados de búsqueda de los usuarios, pero
						tranquilo que una correcta implementación de desarrollo
						web soluciona estos dolores de cabeza por ti.
					</Text>

					<Text mb={20} align="justify">
						Si eres un desarrollador web es probable que te interese
						mejorar tus habilidades en desarrollo web para cumplir
						con las mejores prácticas que son requeridas por Google
						o si eres una persona líder de negocios o empresas y
						requieres un sitio web atractivo puedes contratar un
						desarrollador web Freelance u obtener la enorme cantidad
						de beneficios que una Agencia de Marketing Digital puede
						aportarle a tu negocio incluyendo el posicionamiento
						SEO.
					</Text>
				</Col>
				<Col xsWidth="100%" mdWidth="50%">
					<Col height={350} mdPh={20}>
						<Image
							borderRadius={10}
							fit="cover"
							url={seo}
							name="estrategias de seo"
							width="100%"
						/>
					</Col>
					<Text xsSize={12} opacity={0.5} pt={10} mb={20}>
						Imagen utilizada de{" "}
						<Text
							type="link"
							href="https://www.pexels.com/es-es/foto/tres-baldosas-de-scrabble-en-blanco-y-negro-sobre-una-superficie-de-madera-marron-270637/"
							color="context"
						>
							pexels.com
						</Text>
					</Text>
				</Col>
			</Flex>

			<Text mb={20} align="justify">
				No entraré mucho en detalles sobre porque deberías de elegir una
				Agencia de Marketing vs Desarrollador Freelance, ese tema lo
				dejaré para otro post, así como tampoco explicaré mucho sobre
				este beneficio de posicionamiento SEO, que estaremos tocando el
				tema en el siguiente post.
			</Text>

			<Text mb={20} align="justify">
				Sin embargo, el posicionamiento SEO es el segundo beneficio más
				importante, debido que si el desarrollo web se aplica de manera
				correcta tu sitio web aparecerá en los primeros resultados de
				búsqueda y por consiguiente tus posibles clientes te encontrarán
				a ti primero que a tu competencia.
			</Text>

			<Col padding={20} borderRadius={10} mb={30} background="#f6f6f6">
				<i>
					Sabías que más del 95% de los usuarios en internet hace clic
					en los primeros 5 enlaces de un resultado de búsqueda, el
					resto los ignoran o son muy pocos los que se atreven a
					navegar más allá de eso, e aquí la importancia de un buen
					desarrollo y el poderoso beneficio que ofrece.
				</i>
			</Col>

			<Title type="h3" mv={20}>
				3.- Desarrollo a la medida.
			</Title>

			<Text mb={20} align="justify">
				Desde mi punto de vista personal y profesional hay un sin fin de
				sitios web regados por toda la red y he tenido el placer de
				trabajar y encontrarme con algunos proyectos que si te los
				muestro aquí en este post pensarás lo mismo que yo... Son copia
				el uno del otro, solo cambian de color, tienen otro logotipo,
				muy probablemente tiene funcionalidad que el otro sitio no tiene
				o en el caso más extremo tienen un Frankenstein para ofrecerle a
				sus clientes, lo he nombrado así porque cada sección de su sitio
				web es copia de más de 4 páginas web, toda una combinación de
				ideas diferentes y estilos diferentes que no cuadran con el
				objetivo del sitio web en cuestión
			</Text>

			<Text mb={20} align="justify">
				Otro de los beneficios es el desarrollo a la medida, depende de
				quien este a cargo del proyecto puede decidir hacer un
				Frankenstein o hacer una verdadera obra de arte, asignando un
				equipo especializado tanto en diseño como en desarrollo, y dejar
				en pocas palabras contento a su cliente, un desarrollo a la
				medida hace que un proyecto sea único en internet y se desmarque
				de la competencia, a esto sumándole un buen SEO puede
				posicionarse rapidamente en los primero lugares de Google.
			</Text>

			<Title type="h3" mv={20}>
				4.- Ahorro en inversiones y costos de la empresa.
			</Title>

			<Flex flexWrap="wrap">
				<Col xsWidth="100%" mdWidth="50%">
					<Col xsHeight={300} mdHeight={700} mdPh={20}>
						<Image
							borderRadius={10}
							fit="cover"
							url={inversion}
							name="estrategias de inversion"
							width="100%"
						/>
					</Col>
					<Text xsSize={12} opacity={0.5} pt={10} mb={20}>
						Imagen utilizada de{" "}
						<Text
							type="link"
							href="https://www.pexels.com/es-es/foto/mano-dinero-centavos-monedas-3943723/"
							color="context"
						>
							pexels.com
						</Text>
					</Text>
				</Col>
				<Col xsWidth="100%" mdWidth="50%">
					<Text mb={20} align="justify">
						Sea cual sea la finalidad con que necesites un sitio
						web, plataforma o sistema de gestión, un buen desarrollo
						puede ahorrarte mucho más que dolores de cabeza, me
						refiero a costos, utilidades entre otras cosas debido a
						la gran versatilidad que ofrece el desarrollo web vs
						software de escritorio y mucho más.
					</Text>

					<Text mb={20} align="justify">
						Te pongo un ejemplo sencillo de un cliente que tuvimos
						aquí en la agencia, aplicando correctamente las
						metodologías ágiles (este es otro tema para posteriores
						publicaciones), logramos para nuestro cliente un ahorro
						de más de 60% tanto en tiempo, logística y estrategias
						de Marketing debido al buen desarrollo aplicado para su
						caso en específico, y me permito la libertad de
						mencionar que el proyecto era un E-Commerce hecho y
						derecho, con diferentes métodos de pago, envíos por
						paquetería, sistema de inventario, fotografía etc.
					</Text>

					<Text mb={20} align="justify">
						De atender a sus clientes por Messenger y cerrar una
						venta cada 10 minutos en promedio, ahora con su
						E-Commerce esta cerrando ventas de manera automática con
						un promedio de 3 minutos por venta completada, eso es
						increíble, pero el desarrollo web por si sólo no logra
						estos resultados, también entra en escena el
						posicionamiento SEO, las estrategias de Marketing, el
						Branding de la empresa entre otros factores.
					</Text>

					<Text mb={20} align="justify">
						Tranquilo que aquí en Digital Booting contamos con un
						amplio espectro de servicios enfocados en el usuario
						final, nosotros nos encargamos de toda esa preocupación
						que puede asecharte si estás pensando lanzarte de lleno
						al mundo de los negocios digitales, si ya estás en este
						mundo puede que necesites ayuda, puedes contar con
						nosotros.
					</Text>
				</Col>
			</Flex>

			<Text mv={30} align="justify">
				Sólo me resta agradecerte el tiempo de leer este post y espero
				haber despertado tu curiosidad, haber resuelto algunas de tus
				inquietudes y abierto algunas otras nuevas, me gustaría ayudarte
				con ellas.
			</Text>

			<Text mb={20} align="justify">
				En Digital Booting desarrollamos a la medida de las necesidades
				de nuestros clientes, puedes cotizar con nosotros tu proyecto,
				podemos garantizarte que cuentas con todos los beneficios que
				aquí te estoy exponiendo.
			</Text>
		</>
	);
};

export default RenderPost;
