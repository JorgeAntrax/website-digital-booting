import React from "react";
import {
	Text,
	Title,
	Center,
	Image,
	List,
	ListItem,
	Svg,
	Flex,
	Col,
} from "@components";
import { useWidth } from "@hooks";

import iamkt from "./assets/ia-en-marketing.jpg";
import tec from "./assets/tec.jpeg";
import tecm from "./assets/tecm.jpeg";
import estadistics from "./assets/estadistics.mp4";

const RenderPost = () => {
	const { isMobile } = useWidth();
	return (
		<>
			<Text mb={20} align="justify">
				¿Te has preguntado alguna vez cómo la inteligencia artificial
				puede mejorar tus campañas de marketing digital? Si la respuesta
				es sí, este artículo te interesa. Y si la respuesta es no,
				también deberías seguir leyendo, porque{" "}
				<Text type="strong">
					la inteligencia artificial es una tecnología que está
					revolucionando el mundo del marketing
				</Text>{" "}
				y que puede ofrecerte grandes ventajas competitivas.
			</Text>

			<Text mb={20} align="justify">
				La inteligencia artificial (IA) es la capacidad de las máquinas
				de imitar el comportamiento y el razonamiento humano, mediante
				algoritmos y datos. Gracias a ella,{" "}
				<Text type="strong">
					las máquinas pueden aprender, analizar, predecir y optimizar
					diversas tareas
				</Text>{" "}
				que antes solo podían realizar las personas.
			</Text>

			<Text mb={20} align="justify">
				El marketing digital no es ajeno a los beneficios de la
				inteligencia artificial. Cuando usamos las redes sociales,
				compramos online o manejamos IoT, se genera gran cantidad de
				datos personales y de comportamiento. Su procesamiento y
				análisis abre la puerta a nuevas aplicaciones de la IA en
				marketing digital.
			</Text>

			<Title type="h2" mv={20}>
				¿Qué aplicaciones tiene la inteligencia artificial en el
				marketing digital?
			</Title>

			<Center>
				<Image
					url={iamkt}
					width="100%"
					xsHeight={260}
					mdHeight={600}
					borderRadius={10}
					fit="cover"
					name="aplicaciones de la IA en marketing"
				/>
			</Center>

			<Text xsSize={12} opacity={0.5} pt={10}>
				Imagen generada con{" "}
				<Text
					type="link"
					target="_blank"
					href="https://www.bing.com/images/create/please-create-a-hyperrealistic-image-of-a-robotic-/6441cc7c1ecb4c3ca675a27c2f63ac0d?id=8JNZgdcclttE1QuiZ%2bCtVQ%3d%3d&view=detailv2&idpp=genimg&FORM=GCRIDP&mode=overlay"
					color="context"
				>
					inteligencia artificial
				</Text>
			</Text>

			<Text mb={20} mt={40} align="justify">
				La inteligencia artificial puede mejorar tus campañas de
				marketing digital en varios aspectos, como los siguientes:
			</Text>

			<List>
				<ListItem className="pb:1">
					<Text type="strong">Segmentación:</Text> La IA te permite
					segmentar a tu público objetivo de forma más precisa y
					personalizada, en función de sus características, intereses,
					comportamientos y preferencias. Así puedes ofrecerles
					contenidos y ofertas más relevantes y atractivos para ellos.
				</ListItem>
				<ListItem className="pb:1">
					<Text type="strong">Personalización:</Text> La IA te permite
					adaptar tu mensaje y tu oferta a cada usuario, en función de
					su contexto, su historial y su intención. Así puedes crear
					experiencias únicas y satisfacer sus necesidades y
					expectativas.
				</ListItem>
				<ListItem className="pb:1">
					<Text type="strong">Optimización:</Text> La IA te permite
					optimizar tu presupuesto y tu rendimiento, al elegir los
					canales, los formatos, los momentos y las pujas más
					adecuados para cada campaña. Así puedes maximizar tu retorno
					de la inversión y evitar el desperdicio de recursos.
				</ListItem>
				<ListItem className="pb:1">
					<Text type="strong">Predicción:</Text> La IA te permite
					predecir el comportamiento y las reacciones de tus usuarios,
					al analizar sus patrones y tendencias. Así puedes
					anticiparte a sus necesidades y deseos, y ofrecerles
					soluciones proactivas y oportunas.
				</ListItem>
			</List>

			<Title type="h2" mv={20}>
				¿Qué beneficios te ofrece la inteligencia artificial en el
				marketing digital?
			</Title>

			<Text mb={20} mt={40} align="justify">
				Años atrás era casi imposible pensar que la IA pudiera darle
				súper-poderes a las herramientas digitales más utilizadas por
				los marketeros, sin embargo su reciente avance principalmente en
				los modelos de IA generativa ha permitido la creación de
				herramientas digitales capaces de predecir y evaluar distintos
				aspectos clave de un usuario como las emociones que este expresa
				ante un anuncio, sus respuestas, entre muchos otros factores.
				Ahora, la inteligencia artificial puede mejorar tus campañas de
				marketing digital proporcionando beneficios que antes requerían
				de intervención humana, a continuación te muestro sólo algunos
				beneficios que han venido a caer como bandeja de oro a los
				marketeros, agencias y creadores de contenido:
			</Text>

			<List>
				<ListItem className="pb:1">
					<Text type="strong">Mayor eficiencia:</Text> La IA te
					permite automatizar y agilizar procesos que antes requerían
					mucho tiempo y esfuerzo humano. Así puedes ahorrar costes y
					recursos, y dedicarte a tareas más creativas y estratégicas.
				</ListItem>
				<ListItem className="pb:1">
					<Text type="strong">Mayor efectividad:</Text> La IA te
					permite mejorar la calidad y la relevancia de tus acciones
					de marketing. Así puedes aumentar el engagement, la
					conversión y la fidelización de tus clientes.
				</ListItem>
				<ListItem className="pb:1">
					<Text type="strong">Mayor competitividad:</Text> La IA te
					permite diferenciarte de tu competencia y posicionarte como
					una marca innovadora y líder en tu sector. Así puedes captar
					la atención y la confianza de tu público objetivo.
				</ListItem>
				<ListItem className="pb:1">
					<Text type="strong">Predicción:</Text> La IA te permite
					predecir el comportamiento y las reacciones de tus usuarios,
					al analizar sus patrones y tendencias. Así puedes
					anticiparte a sus necesidades y deseos, y ofrecerles
					soluciones proactivas y oportunas.
				</ListItem>
			</List>

			<Title type="h2" mv={20}>
				¿Qué estadísticas respaldan el uso de la inteligencia artificial
				en el marketing digital?
			</Title>

			<Flex flexWrap="wrap" mt={40}>
				<Col
					xswidth="100%"
					mdWidth="60%"
					style={{ order: isMobile ? 5 : -1 }}
				>
					<Text mb={20} align="justify">
						No hay duda alguna de que la inteligencia artificial ha
						llegado para quedarse, su popularidad y aceptación viene
						de la mano del chatbot conversacional Chat GPT que ha
						asombrado a todo el mundo, uno de los principales
						sectores en donde la IA ya es participe en el día a día
						es en la web!, con diversos e interesantes proyectos
						como{" "}
						<Text
							type="link"
							color="context"
							target="_blank"
							href="https://www.copy.ai
"
						>
							Copy.ai
						</Text>
						,{" "}
						<Text
							type="link"
							color="context"
							target="_blank"
							href="https://www.synthesia.io"
						>
							Synthesia
						</Text>
						,{" "}
						<Text
							type="link"
							color="context"
							target="_blank"
							href="https://designer.microsoft.com"
						>
							Microsoft Designer
						</Text>
						, entre otros, la mayoría de estas herramientas
						enfocadas en el Marketing digital.
					</Text>

					<Text mb={20} align="justify">
						El uso de la inteligencia artificial en el marketing
						digital está respaldado por varias estadísticas que
						demuestran su impacto positivo en los resultados
						empresariales. Algunas de ellas son las siguientes:
					</Text>

					<List>
						<ListItem className="pb:1">
							El gasto mundial en sistemas cognitivos y de IA
							alcanzó los 50.100 millones de dólares en 2020
						</ListItem>
						<ListItem className="pb:1">
							El 44% de las empresas que han implementado la IA
							informan una reducción de los costes empresariales
						</ListItem>
						<ListItem className="pb:1">
							El 40% de las empresas que han implementado la IA
							informan un aumento de los ingresos.
						</ListItem>
						<ListItem className="pb:1">
							El 63% de los consumidores prefieren comprar en
							sitios web que utilizan recomendaciones
							personalizadas basadas en IA.
						</ListItem>
						<ListItem className="pb:1">
							El 80% de los profesionales del marketing afirman
							que la IA ha mejorado su productividad.
						</ListItem>
					</List>

					<Text mb={20} align="justify">
						Con el reciente crecimiento exponencial que han tenido
						estas herramientas de IA, podemos esperar mejores
						estadísticas en el segundo semestre del 2023 y bastantes
						novedades, una de las principales novedades es que en{" "}
						<Text type="strong">Digital Booting</Text> ya estamos
						integrando esta tecnología en cada una de nuestras
						soluciones con el objetivo de brindar mejores resultados
						para todos nuestros clientes.
					</Text>
				</Col>
				<Col
					xswidth="100%"
					mdWidth="40%"
					style={{
						order: isMobile ? -1 : 5,
						marginBottom: isMobile ? "40px" : "0",
					}}
				>
					<Center>
						{isMobile && (
							<video
								loop
								muted
								style={{
									width: "100%",
									maxWidth: "390px",
									height: "640px",
									borderRadius: "20px",
									objectFit: "cover",
								}}
							>
								<source src={estadistics} type="video/mp4" />
								Tu navegador no admite el elemento{" "}
								<code>video</code>.
							</video>
						)}
						{!isMobile && (
							<video
								loop
								muted
								style={{
									width: "100%",
									maxWidth: "390px",
									height: "740px",
									borderRadius: "20px",
									objectFit: "cover",
								}}
							>
								<source src={estadistics} type="video/mp4" />
								Tu navegador no admite el elemento{" "}
								<code>video</code>.
							</video>
						)}
					</Center>
				</Col>
			</Flex>

			<Title type="h2" mt={40} mb={20}>
				¿Cómo puedes empezar a usar la inteligencia artificial en el
				marketing digital?
			</Title>

			<Text mb={20} mt={40} align="justify">
				Talvez no te interese el ¿Cómo? si no los beneficios, para este
				caso en particular puedes comunicarte con nuestro equipo de
				expertos y en conjunto crearemos estrategias digitales para que
				tu negocio pueda comenzar a aprovechar los beneficios de la
				inteligencia artificial.
			</Text>
			<Text mb={20} align="justify">
				Si estas aquí en la eterna búsqueda del conocimiento y el
				¿Porqué? de las cosas te contare a continuación cuales son
				nuestros pasos recomendados a seguir para incursionar en temas
				de inteligencia artificial aplicada al mundo del marketing:
			</Text>

			<List>
				<ListItem className="pb:1">
					<Text type="strong">
						Define tus objetivos y tu estrategia
					</Text>
					: Antes de implementar la IA, debes tener claro qué quieres
					conseguir y cómo vas a medirlo. Así podrás elegir las
					herramientas y las técnicas más adecuadas para tu caso.
				</ListItem>
				<ListItem className="pb:1">
					<Text type="strong">Investiga y analiza tus datos</Text>: La
					IA se alimenta de datos, por lo que debes asegurarte de
					tener una buena cantidad y calidad de ellos. Para ello,
					debes recopilar, limpiar, organizar y analizar tus datos,
					tanto internos como externos.
				</ListItem>
				<ListItem className="pb:1">
					<Text type="strong">
						Elige las herramientas y las plataformas de IA
					</Text>
					: Existen muchas opciones en el mercado para usar la IA en
					el marketing digital, desde aplicaciones específicas hasta
					soluciones integrales. Debes elegir las que mejor se adapten
					a tus necesidades, presupuesto y nivel de conocimiento.
				</ListItem>
				<ListItem className="pb:1">
					<Text type="strong">Prueba y optimiza tus campañas</Text>:
					Una vez que hayas implementado la IA, debes monitorizar y
					evaluar los resultados de tus campañas. Así podrás detectar
					las fortalezas y las debilidades, y hacer los ajustes
					necesarios para mejorar tu rendimiento, como recomendación
					la IA debería ayudarte con todo esto, para que tu solamente
					tomes decisiones de negocio.
				</ListItem>
			</List>

			<Title type="h2" mv={20}>
				Tecnologías de Inteligencia Artificial emergentes en 2023 y 2024
			</Title>

			<Center>
				<Image
					url={isMobile ? tecm : tec}
					width="100%"
					height={500}
					borderRadius={10}
					fit={"cover"}
					name="tecnologias emergentes"
				/>
			</Center>

			<Text xsSize={12} opacity={0.5} pt={10}>
				Imagen generada con{" "}
				<Text
					type="link"
					target="_blank"
					href="https://designer.microsoft.com"
					color="context"
				>
					inteligencia artificial
				</Text>
			</Text>

			<Text mb={20} mt={40} align="justify">
				El 2023 es el año de la inteligencia artificial, en su primer
				semestre ya han visto la luz diversas herramientas basadas en
				tecnología de IA, la más popular sería{" "}
				<Text
					type="link"
					color="context"
					href="https://chat.openai.com"
					target="_blank"
				>
					<b>Chat-GPT</b>
				</Text>{" "}
				de la empresa sin fines de lucro{" "}
				<Text
					type="link"
					color="context"
					href="https://openai.com"
					target="_blank"
				>
					<b>Open AI</b>
				</Text>
				, pero claro, no es la única ni la última herramienta que
				intentará conquistar la red y el nicho de la inteligencia
				artificial, ya sea en marketing o en otros sectores de
				tecnología como los video juegos o la informática cuántica.
			</Text>

			<Text mb={20} align="justify">
				La inteligencia Artificial es una tecnología en constante
				evolución y que nos sorprende cada vez más con nuevas
				aplicaciones y avances. 
				<Text type="strong">
					¿Qué podemos esperar de la IA en lo que resta del 2023 y
					2024?
				</Text>{" "}
				Según el{" "}
				<Text
					type="link"
					color="context"
					href="https://www.technologyreview.com"
					target="_blank"
				>
					<b>MIT Technology Review</b>
				</Text>
				, estas son algunas de las tendencias que nos asombrarán en el
				próximo año y lo que resta del 2023:
			</Text>

			<List>
				<ListItem className="pb:1">
					<Text type="strong">Chatbots multipropósito</Text>: Los
					modelos de lenguaje que generan texto a partir de datos se
					están combinando con otras modalidades, como el
					reconocimiento de imágenes o vídeos. Esto permitirá crear
					chatbots capaces de manejar diferentes tipos de consultas y
					tareas, desde reservar un viaje hasta diagnosticar una
					enfermedad.
				</ListItem>
				<ListItem className="pb:1">
					<Text type="strong">Creación de imágenes con IA</Text>: Los
					modelos de IA que generan imágenes a partir de frases
					sencillas se están convirtiendo en potentes herramientas
					creativas y comerciales. Estos modelos pueden crear desde
					logos hasta retratos, pasando por paisajes o productos. Su
					aplicación al marketing puede ser muy variada, desde generar
					contenidos visuales atractivos hasta personalizar las
					ofertas según los gustos del usuario.
				</ListItem>
				<ListItem className="pb:1">
					<Text type="strong">Integración de la IA + Robótica</Text>:
					Proyectos y empresas como{" "}
					<Text
						type="link"
						href="https://www.bostondynamics.com"
						target="_blank"
						color="context"
					>
						<b>Boston Dynamics</b>
					</Text>{" "}
					ya comienzan a integrar tecnología de IA en sus robots, lo
					que les permite hablar y entender el lenguaje humano
					integrando Chat-GPT en sus sistemas.
				</ListItem>
				<ListItem className="pb:1">
					<Text type="strong">IA en medicina</Text>: La IA puede
					impactar positivamente la práctica de la medicina, ya sea
					acelerando el ritmo de la investigación o ayudando a los
					médicos a tomar mejores decisiones. Por ejemplo, los modelos
					de machine learning podrían usarse para observar los signos
					vitales de los pacientes que reciben cuidados intensivos y
					alertar a los médicos si aumentan ciertos factores de riesgo
				</ListItem>
			</List>

			<Title type="h2" mv={20}>
				Conclusión
			</Title>

			<Text mb={20} align="justify">
				La inteligencia artificial es una tecnología que puede mejorar
				tus campañas de marketing digital de forma significativa. Te
				permite segmentar, personalizar, optimizar y predecir el
				comportamiento de tus usuarios, y ofrecerles experiencias únicas
				y satisfactorias. Además, te ofrece beneficios como mayor
				eficiencia, efectividad y competitividad.
			</Text>
			<Text mb={20} align="justify">
				Si quieres empezar a usar la inteligencia artificial en el
				marketing digital, te invitamos a contactar con nosotros. En
				Digital Booting somos una agencia digital que ofrece soluciones
				de marketing basadas en inteligencia artificial para todo tipo
				de negocio. Podemos ayudarte a diseñar e implementar campañas
				exitosas que te permitan alcanzar tus objetivos y superar a tu
				competencia.
			</Text>
			<Text mb={20} xsWeight="bold" align="justify">
				No esperes más y da el salto a la inteligencia artificial.
				¡Contáctanos hoy mismo!
			</Text>
		</>
	);
};

export default RenderPost;
