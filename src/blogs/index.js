import BeneficiosDesarrolloWeb from "./beneficios-del-desarrollo-web-en-marketing-digital";
import MejoraMarketingIA from "./como-puede-la-ia-mejorar-tus-campanas-de-marketing-digital";
import ChatbotsIA from "./chatbots-con-ia-transformando-el-marketing-digital";

export default {
	"beneficios-del-desarrollo-web-en-marketing-digital":
		BeneficiosDesarrolloWeb,
	"como-puede-la-ia-mejorar-tus-campanas-de-marketing-digital":
		MejoraMarketingIA,
	"chatbots-con-ia-transformando-el-marketing-digital": ChatbotsIA,
};
