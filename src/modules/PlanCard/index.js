import React from "react";
import styled from "styled-components";
import { SuperCSS } from "@utils";

import { format } from "@utils";

import { Flex, Center, Image, Title, Text, CheckIcon } from "@components";

const Wrapper = styled.div`
	display: grid;
	width: 100%;
	grid-template-columns: 1fr;
	grid-template-rows: 1fr 40px;
	border-radius: 10px;
	padding: 1rem;
	box-shadow: 0 6px 15px -4px rgba(0, 0, 0, 0.2);

	justify-content: flex-start;

	ul {
		padding: 0;
		margin: 0;

		li {
			list-style: none;
			display: flex;
			align-items: flex-start;
			padding-bottom: 6px;

			span {
				margin-top: -2px;
				font-size: 14px;
			}

			&:first-child span {
				font-weight: 500;
			}
		}
	}

	${(props) => SuperCSS.hydrate(props)}
`;

const PlanCard = ({
	category,
	price,
	currency,
	url,
	theme,
	plan,
	featured = false,
	features,
	...props
}) => {
	let background =
		theme === "dark"
			? "#141D28"
			: theme === "darkover"
			? "#1a2635"
			: "white";
	let color = !["dark", "darkover"].includes(theme) ? "#141D28" : "white";

	if (featured) {
		background = theme === "dark" ? "context" : "primary";
		color = theme === "dark" ? "white" : "black";
	}

	return (
		<Wrapper background={background} color={color} {...props}>
			<Flex content="start" items="start" flexWrap="wrap">
				<Center>
					<Text align="center" mb={10}>
						{category.toUpperCase()}
					</Text>
				</Center>

				<Center>
					<Text fontWeight="bold" align="center" mdSize={24}>
						{format(price)} {currency}
					</Text>
				</Center>
				<Center>
					<Text align="center" mb={30} mdSize={12} xsOpacity="0.7">
						{plan}
					</Text>
				</Center>

				<ul style={{ marginBottom: 20 }}>
					{features.length &&
						features.map((feature, index) => (
							<li key={index}>
								<CheckIcon
									w={18}
									color={
										["dark", "darkover"].includes(theme)
											? "white"
											: "#141D28"
									}
								/>
								<Text type="span" ml={12}>
									{feature}
								</Text>
							</li>
						))}
				</ul>
			</Flex>

			{url && (
				<Center>
					<Text
						type="link"
						href={url}
						pt={20}
						align="center"
						weight="400"
						color={
							["dark", "darkover"].includes(theme)
								? "white"
								: featured
								? "black"
								: "context"
						}
						alignSelf="end"
					>
						+ Contáctanos
					</Text>
				</Center>
			)}
		</Wrapper>
	);
};

export default PlanCard;
