import React, { memo } from "react";
import { Col, Container, Flex, Image, Svg, Title, Text } from "@components";
import { Suscribe, RelatedPosts, Socios, Author } from "@containers/blog";
import { SubTitle } from "@modules";
import { useWidth } from "@hooks";

import heart from "@assets/heart.svg";
import timer from "@assets/timer.svg";
import tag from "@assets/tag.svg";
import share from "@assets/share.svg";

import twitter from "@assets/twittersvg.svg";
import whatsapp from "@assets/whatsapp.svg";
import facebook from "@assets/facebook.svg";

const handleShare = (type, slug) => {
	if (typeof window === "undefined") return;
	switch (type) {
		case "whatsapp":
			window.open(
				`https://api.whatsapp.com/send?text=https://digitalbooting.com/blog-de-marketing-digital/${slug}`,
			);
			break;
		case "twitter":
			window.open(
				`https://twitter.com/intent/tweet?url==https://digitalbooting.com/blog-de-marketing-digital/${slug}`,
			);
			break;
		case "facebook":
			window.open(
				`https://www.facebook.com/sharer/sharer.php?u=https://digitalbooting.com/blog-de-marketing-digital/${slug}`,
			);
			break;
	}
};

const RenderPostTemplate = ({ slug, post, seo }) => {
	const dimentions = useWidth();
	const { isMobile } = dimentions;

	return (
		<>
			<Flex background="#f6f6f6">
				<Container pv={40} xsMt={50} mdMt={0}>
					<Flex flexWrap="wrap" items="center">
						<Col
							xsWidth="100%"
							mdWidth="50%"
							xsPadding={20}
							mdPadding={40}
						>
							<SubTitle
								type="h1"
								color="context"
								align="left"
								mdSize={35}
								weight="normal"
								mdMaxw={600}
								mb={20}
							>
								{seo.title}
							</SubTitle>

							<Title type="h2" weight="300" size={16} mb={50}>
								{seo.description}
							</Title>

							<Text
								display="flex"
								items="center"
								size={16}
								mb={10}
							>
								<Svg mr={20} icon={heart} wsvg={24} /> +
								{seo.likes}K personas les gusta esto.
							</Text>
							<Text
								display="flex"
								items="center"
								size={16}
								mb={10}
							>
								<Svg mr={20} icon={timer} wsvg={24} />
								{seo.timereading} minutos de lectura.
							</Text>
							<Text
								display="flex"
								items="center"
								size={16}
								mb={10}
							>
								<Svg mr={20} icon={tag} wsvg={24} /> Categoria{" "}
								{seo.category}.
							</Text>

							{isMobile && (
								<Text
									display="inline-flex"
									items="center"
									size={16}
									mb={10}
								>
									<Svg mr={20} icon={share} wsvg={24} />{" "}
									{seo.shared} veces compartido
								</Text>
							)}
						</Col>
						<Col
							xsWidth="100%"
							mdWidth="50%"
							xsPadding={20}
							mdPadding={40}
							xsHeight={300}
							mdHeight={500}
						>
							<Image
								url={seo.cover}
								width="100%"
								fit="cover"
								borderRadius={10}
								boxShadow="0 15px 35px -10px rgba(0, 0, 0, 0.2)"
							/>
						</Col>
					</Flex>

					{!isMobile && (
						<Flex
							items="center"
							justify="between"
							xsPh={20}
							mdPh={40}
							mt={30}
						>
							<Col>
								<Text
									display="inline-flex"
									items="center"
									size={16}
									mb={10}
								>
									<Svg mr={20} icon={share} wsvg={24} />{" "}
									{seo.shared} veces compartido
								</Text>
							</Col>
							<Col>
								<Text
									display="inline-flex"
									items="center"
									size={16}
									mb={10}
								>
									Compartir blog:{" "}
									<Svg
										ml={20}
										icon={whatsapp}
										wsvg={24}
										onClick={() =>
											handleShare("whatsapp", slug)
										}
										cursor="pointer"
									/>
									<Svg
										ml={20}
										icon={twitter}
										wsvg={24}
										onClick={() =>
											handleShare("twitter", slug)
										}
										cursor="pointer"
									/>
									<Svg
										ml={20}
										icon={facebook}
										wsvg={24}
										onClick={() =>
											handleShare("facebook", slug)
										}
										cursor="pointer"
									/>
								</Text>
							</Col>
						</Flex>
					)}
				</Container>
			</Flex>
			<Container pv={50}>
				<Flex justify="center" ph={30}>
					<Col width="100%" mdMaxw={1200}>
						{post || null}
						<Author author={seo.author} blog={slug} />
					</Col>
				</Flex>
			</Container>

			<RelatedPosts exclude={seo.id} tag={seo.category} />

			<Socios />
			<Col mb={40}>
				<Suscribe />
			</Col>
		</>
	);
};

export default memo(RenderPostTemplate);
