import React, { memo } from "react";
import { Col } from "@components";
import { Suscribe, RelatedPosts, Socios } from "@containers/blog";
import { Banner } from "@modules";
import { Links } from "@constants";

const NotFound = () => {
	return (
		<>
			<Banner
				title="Articulo no encontrado"
				description="El articulo que estas buscando aún no existe, intenta leer uno de los post ya existentes que sea de tu interés."
				handleText="Ver todos los articulos"
				url={Links.$blog}
			/>

			<RelatedPosts exclude={null} interested={true} />
			<Socios />
			<Col mb={40}>
				<Suscribe />
			</Col>
		</>
	);
};

export default memo(NotFound);
