import React from "react";
import styled from "styled-components";
import { SuperCSS } from "@utils";

import { Center, Image, Title, Text } from "@components";

const Wrapper = styled.div`
	border-radius: 10px;
	padding: 2rem 1rem;
	box-shadow: 0 6px 15px -4px rgba(0, 0, 0, 0.2);

	${(props) => SuperCSS.hydrate(props)}
`;

const Card = ({ icon, title, description, url, theme, ...props }) => {
	const background = theme === "dark" ? "#141D28" : "white";
	const color = theme !== "dark" ? "#141D28" : "white";

	return (
		<Wrapper background={background} color={color} {...props}>
			<Center mb={10}>
				<Image name={title} url={icon} dimentions={80} />
			</Center>
			<Title align="center" type="h3" mb={20}>
				{title}
			</Title>
			<Text align="center" mb={10} height={130}>
				{description}
			</Text>

			{url && (
				<a href={url}>
					<Text
						pt={20}
						align="center"
						weight="400"
						color={theme === "dark" ? "white" : "context"}
					>
						+ Ver más
					</Text>
				</a>
			)}
		</Wrapper>
	);
};

export default Card;
