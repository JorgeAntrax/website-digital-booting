import React from "react";
import styled, { css } from "styled-components";
import { SuperCSS } from "@utils";

import { Center, Image, Title, Text, Button } from "@components";

const effectCard = css`
	box-shadow: 0 0px 0px 1px rgba(0, 0, 0, 0.05);
	transform: translateY(-20px);

	a {
		opacity: 1;
		pointer-events: all;
		bottom: -24px;
	}

	.tagged-blog {
		opacity: 1;
		transform: translate(-16px, -8px);
	}

	picture {
		transform: scale(0.94) translateY(-20px);
		box-shadow: 0 6px 15px -4px rgba(0, 0, 0, 0.2);
	}
`;

const Wrapper = styled.div`
	&,
	picture {
		width: 100%;
		position: relative;
		transition: all 200ms linear;
		border-radius: 10px;
	}

	a {
		transition: all 200ms linear;
		position: absolute;
		bottom: -60px;
		right: 2rem;

		pointer-events: none;
		opacity: 0;
	}

	@media screen and (max-width: 768px) {
		${effectCard}
		a {
			bottom: -48px;
		}
	}

	@media screen and (min-width: 768px) {
		&:hover {
			${effectCard}
		}
	}

	${(props) => SuperCSS.hydrate(props)}
`;

const Tag = styled.span`
	background-color: rgba(0, 0, 0, 0.5);
	display: inline-flex;
	padding: 4px 8px;
	border-radius: 2rem;
	position: absolute;
	top: 0.5rem;
	right: 0.5rem;
	z-index: 2;
	color: white;
	font-size: 12px;
	transition: all 200ms linear;
	opacity: 0;
	user-select: none;
	${(props) => SuperCSS.hydrate(props)}
`;

const CardBlog = ({
	thumbnail,
	title,
	description,
	slug,
	category,
	metakeyword,
	...props
}) => {
	const background = "white";
	const color = "#141D28";

	return (
		<Wrapper background={background} color={color} {...props}>
			<Tag className="tagged-blog">{category?.toUpperCase()}</Tag>
			<Center mb={10}>
				<Image
					name={title}
					url={thumbnail}
					width="100%"
					height={250}
					fit="cover"
				/>
			</Center>
			<Title type="h3" mb={20} ph="1rem" fontWeight={400}>
				{title}
			</Title>
			<Text mb={10} height={100} ph="1rem">
				{description}
			</Text>

			<Button
				type="link"
				href={`/blog-de-marketing-digital/${slug}`}
				theme="context"
			>
				Leer articulo
			</Button>
		</Wrapper>
	);
};

export default CardBlog;
