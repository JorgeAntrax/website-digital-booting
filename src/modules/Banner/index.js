import React from "react";
import { Flex, Title, Text, Button, Col } from "@components";

const Banner = ({ title, description, url, handleText, height = 500 }) => {
	return (
		<Flex
			$flexalign="center"
			$direction="column"
			$height={height}
			$ph={20}
			$xsMt={80}
			$mdMt={0}
		>
			<Col>
				<Title
					$color="context"
					$textTransform="uppercase"
					$xsSize={35}
					$mdSize={50}
					$align="center"
					type="h1"
				>
					{title}
				</Title>
			</Col>
			<Col $mt={20}>
				<Text $mdSize={22} $maxw={900} $align="center">
					{description}
				</Text>
			</Col>
			{handleText && (
				<Col $mt={40}>
					<Button type="link" href={url} theme="context">
						{handleText}
					</Button>
				</Col>
			)}
		</Flex>
	);
};

export default Banner;
