import React, { memo } from "react";
import { Flex, Col, Text, Image, Center } from "@components";

import { SubTitle } from "@modules";
import useTranslate from "@lang";

import angelcohc from "@assets/customers/angelcoh.png";
import chingon from "@assets/customers/chingon.png";
import cis from "@assets/customers/cis.png";
import crm from "@assets/customers/crm.svg";
import expomascotas from "@assets/customers/expomascotas.png";
import fersa from "@assets/customers/fersa.png";
import hoteles from "@assets/customers/hoteles.png";
import kohchelli from "@assets/customers/kohchelli.png";
import legna from "@assets/customers/legna.png";
import novo from "@assets/customers/novo2.png";
import opticas from "@assets/customers/opticas.svg";
import wald from "@assets/customers/wald.png";
import vgs from "@assets/customers/vgs.png";
import vita from "@assets/customers/vita.png";

const clients = [
	angelcohc,
	chingon,
	cis,
	crm,
	expomascotas,
	fersa,
	hoteles,
	kohchelli,
	novo,
	opticas,
	wald,
	vgs,
	legna,
	vita,
];

const Clientes = () => {
	const { trans } = useTranslate();
	return (
		<>
			<Flex flexWrap="wrap" items="center" pv="5rem">
				<Col xsWidth="100%" mdWidth="50%">
					<Flex items="center" justify="center">
						<Col mdMaxw={520} xsPadding={20} mdPadding={0}>
							<SubTitle
								mb={20}
								items="start"
								justify="center"
								align="left"
							>
								Clientes que confían en Digital{" "}
								<Text type="span" color="primary">
									Booting
								</Text>
								.
							</SubTitle>

							<Text>
								Así como nuestros clientes tu{" "}
								<Text type="strong">
									puedes confiar en que trabajaremos de manera
									eficiente y efectiva
								</Text>{" "}
								para impulsar tu negocio y que puedas lograr tus
								objetivos.
							</Text>
						</Col>
					</Flex>
				</Col>
				<Center flexWrap="wrap" xsWidth="100%" mdWidth="50%">
					{clients.map((client, index) => (
						<Image
							key={index}
							filter="grayscale(1)"
							mode="overlay"
							url={client}
							width={120}
							height={40}
							margin={20}
						/>
					))}
				</Center>
			</Flex>
		</>
	);
};

export default memo(Clientes);
