import { useEffect, useState } from "react";
import { useStorage } from "@storage/store";
// import { addOption } from "@services";
import { gql } from "@utils";

const categories = {
	brand: "createBrand",
	color: "createColor",
	size: "createSize",
	collection: "createCollection",
	material: "createMaterial",
	category: "createCategory",
	country: "createCountry",
};

const refCatalogs = {
	brand: "brands",
	color: "colors",
	size: "sizes",
	collection: "collections",
	material: "materials",
	category: "categories",
	country: "countries",
};

const addOption = () => {};
const useAddOptions = (category, onCancel, getValue) => {
	const {
		auth: { accessToken },
		catalogs,
		setCatalogs,
	} = useStorage();

	const [label, setLabel] = useState("");
	const [description, setDescription] = useState("");
	const [showError, setShowError] = useState(false);
	const [enableButton, setEnableButton] = useState(false);
	const [messageError, setMessageError] = useState("");

	const handleCreateOption = async () => {
		const mutator = categories[category];
		const query = gql`
			mutation {
				${mutator} (
					name: "${label}",
					description: "${description}"
				) {
					id
					name
				}
			}
		`;

		const [resolve, reject] = await addOption(query, accessToken);
		if (reject) {
			setMessageError("We were unable to register this option");
			setShowError(true);
			setTimeout(() => {
				setShowError(false);
			}, 5000);
			return;
		}

		const { id, name } = resolve.data[mutator];

		const list = refCatalogs[category];
		const newList = [...catalogs[list], { id, name, new: true }];

		setCatalogs({
			...catalogs,
			[list]: newList,
		});

		getValue &&
			getValue({
				id,
				name,
			});
	};

	const handleCancel = () => {
		getValue && getValue(null);
		onCancel({ id: null, name: null });
	};

	useEffect(() => {
		if (!label) {
			setDescription("");
			return;
		}

		setDescription(`Product ${category} ${label.toLowerCase()}`);
	}, [label]);

	useEffect(() => {
		setEnableButton(label.length && description.length);
	}, [label, description]);

	return {
		enableButton,
		label,
		setLabel,
		description,
		setDescription,
		showError,
		messageError,
		handleCreateOption,
		handleCancel,
	};
};

export default useAddOptions;
