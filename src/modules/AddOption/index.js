import React from "react";
import { Box, Flex, Col, Input, Button, Text } from "@components";
import useAddOptions from "./hooks";

const AddOption = ({ typeOption, category, getValue, onCancel }) => {
	const {
		enableButton,
		label,
		setLabel,
		description,
		setDescription,
		showError,
		messageError,
		handleCreateOption,
		handleCancel,
	} = useAddOptions(category, onCancel, getValue);

	return (
		<Box inline p="2rem" mt="1rem">
			<Col w="100%">
				<Flex w="100%" mb="1rem">
					{!showError && (
						<Text fs="18px" fw={500} family="oxygen">
							Add new {typeOption}
						</Text>
					)}
					{showError && (
						<Text
							fs="14px"
							fw={500}
							family="oxygen"
							color="#ff0050"
						>
							{messageError}
						</Text>
					)}
				</Flex>

				<Flex w="100%">
					<Col w="100%" gap="1rem">
						<Flex w="100%">
							<Input
								value={label}
								getValue={(v) => setLabel(v.toUpperCase())}
								placeholder={`A new ${typeOption}`}
								onlyNL
							/>
						</Flex>
						<Flex gap="1rem">
							{/* <Input
								value={description}
								getValue={(v) => setDescription(v)}
								placeholder={`Description for new ${typeOption}`}
								onlyNL
							/> */}

							<Flex w="100%" gap="1rem" justify="end">
								<Col>
									<Button
										square
										link
										onClick={() => handleCancel()}
									>
										Cancel
									</Button>
								</Col>
								<Col>
									<Button
										square
										secondary
										disabled={!enableButton}
										onClick={() => handleCreateOption()}
									>
										Add now
									</Button>
								</Col>
							</Flex>
						</Flex>
					</Col>
				</Flex>

				{/* <Flex style={{ padding: "0.5rem", borderLeft: "2px solid red", marginTop: "1rem" }}>
					<Text fs={12} fw={500} family="oxygen">
						<i>To continue you must click on the accept or cancel
						button</i>
					</Text>
				</Flex> */}
			</Col>
		</Box>
	);
};

export default AddOption;
