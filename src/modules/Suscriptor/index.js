import React, { useEffect, useRef } from "react";
import { Svg } from "@src/components";
import { Wrapper } from "./styles";

import asset from "@assets/gradient-effect.svg";

const Suscriptor = ({ effectSize = 800, children, ...props }) => {
	const elementRef = useRef(null);
	const containerRef = useRef(null);

	const handleMouseMove = (event) => {
		const element = elementRef.current;
		const container = containerRef.current;

		if (element) {
			const { x, y } = useMousePosition(container, event);
			const _x = x - effectSize / 2;
			const _y = y - effectSize / 2;

			element.style.transform = `translate(${_x}px,${_y}px)`;
		}
	};

	useEffect(() => {
		containerRef.current.addEventListener("mousemove", handleMouseMove);
		return () => {
			containerRef.current.removeEventListener(
				"mousemove",
				handleMouseMove
			);
		};
	}, []);

	const useMousePosition = (elemento, evt) => {
		const ClientRect = elemento.getBoundingClientRect();
		return {
			x: Math.round(evt.clientX - ClientRect.left),
			y: Math.round(evt.clientY - ClientRect.top),
		};
	};

	return (
		<Wrapper ref={containerRef} {...props}>
			<div
				ref={elementRef}
				className="suscriptor-asset"
				style={{ zIndex: 1, width: effectSize, height: effectSize }}
			>
				<Svg icon={asset} wsvg={effectSize} />
			</div>
			<div style={{ position: "relative", zIndex: 2 }}>{children}</div>
		</Wrapper>
	);
};

export default Suscriptor;
