import styled from "styled-components";
import { SuperCSS } from "@utils";

const Wrapper = styled.div`
	background-color: rgba(171, 235, 255, 0.15);
	overflow: hidden;
	width: 100%;
	height: auto;
	border-radius: 15px;
	backdrop-filter: blur(6px);
	padding: 2rem;
	display: block;
	position: relative;

	.suscriptor-asset {
		transform-origin: center;
		transition: transform 100ms linear;
		position: absolute;
		pointer-events: none;
	}

	${(props) => SuperCSS.hydrate(props)}
`;

export { Wrapper };
