import React, { useState, useEffect } from "react";
import { Slideritem, SliderWrapper } from "./styles";

import {
	Button,
	Center,
	Col,
	Container,
	Flex,
	Image,
	Svg,
	Text,
} from "@components";
import { SubTitle } from "@modules";

import circle from "@assets/circleAsset.svg";
import square from "@assets/squareAsset.svg";

import iconRight from "@assets/circleArrowRight.svg";
import iconLeft from "@assets/circleArrowLeft.svg";
import { useWidth } from "@hooks";

const Slider = ({ data }) => {
	const { isMobile } = useWidth();
	const [active, setActive] = useState(0);

	useEffect(() => {
		const timer = setInterval(() => {
			setActive((prevActive) =>
				prevActive >= data.sliders.length - 1 ? 0 : prevActive + 1
			);
		}, 8000);
		return () => clearInterval(timer);
	}, [data]);

	const handleClick = (state) => {
		setActive((prevActive) => {
			switch (state) {
				case "prev":
					return prevActive < 0
						? data.sliders.length - 1
						: prevActive - 1;
					break;

				default:
					return prevActive >= data.sliders.length - 1
						? 0
						: prevActive + 1;
					break;
			}
		});
	};

	return (
		<Container pb={50}>
			<Center xsMb={30} mdMb={60} ph={20}>
				<SubTitle align="center" items="center" mdMaxw={900}>
					{data.title}
				</SubTitle>
			</Center>
			<Flex items="center" justify="center">
				<Svg
					icon={iconLeft}
					wsvg={60}
					xsTransform="translate(160px, 180px)"
					mdTransform="translate(80px, -20px)"
					style={{ zIndex: 2, cursor: "pointer" }}
					onClick={() => handleClick("prev")}
				/>
				<Flex
					items="center"
					xsPh={20}
					mdPh={0}
					pb={40}
					xsMaxw="100%"
					mdMaxw={1000}
					height={560}
				>
					<SliderWrapper
						itemList={data.sliders.length}
						marginLeft={`-${active}00%`}
					>
						{data.sliders.length &&
							data.sliders.map((slide, index) => (
								<Col
									key={index}
									transform={
										active !== index ? "scale(0.8)" : ""
									}
									pointerEvents={
										active !== index ? "none" : ""
									}
									flex={1}
								>
									<Slideritem>
										<Svg
											icon={square}
											wsvg={140}
											position="absolute"
											mdTop={-40}
											xsTop={-60}
											mdLeft={-20}
											xsLeft={-40}
										/>
										<Svg
											icon={circle}
											wsvg={140}
											position="absolute"
											mdBottom={-60}
											xsBottom={-100}
											right={"30%"}
										/>
										<Flex
											flexWrap="wrap"
											items="center"
											color="white"
										>
											<Col
												xsWidth="100%"
												mdWidth="60%"
												mdPadding={20}
											>
												<SubTitle
													type="h3"
													mb={30}
													align="left"
													lineColor="primary"
													xsSize={20}
												>
													{slide.title}
												</SubTitle>
												<Text>{slide.content}</Text>

												{slide.url && (
													<Col>
														<Button theme="primary">
															{slide.buttonText}
														</Button>
													</Col>
												)}
											</Col>
											{!isMobile && (
												<Col
													xsWidth="100%"
													mdWidth="40%"
													padding={20}
												>
													<Center>
														<Image
															url={slide.asset}
															dimentions={230}
														/>
													</Center>
												</Col>
											)}
										</Flex>
									</Slideritem>
								</Col>
							))}
					</SliderWrapper>
				</Flex>
				<Svg
					icon={iconRight}
					wsvg={60}
					xsTransform="translate(-160px, 180px)"
					mdTransform="translate(-80px, -20px)"
					style={{ zIndex: 2, cursor: "pointer" }}
					onClick={() => handleClick("next")}
				/>
			</Flex>
		</Container>
	);
};

export default Slider;
