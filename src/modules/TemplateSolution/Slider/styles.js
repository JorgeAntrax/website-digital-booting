import styled from "styled-components";
import { SuperCSS } from "@utils";

const SliderWrapper = styled.div`
	position: relative;
	display: flex;
	min-width: ${({ itemList }) => itemList}00%;
	height: 100%;

	transition: margin-left 400ms ease;
	${(props) => SuperCSS.hydrate(props)}
`;

const Slideritem = styled.div`
	position: relative;
	overflow: hidden;
	box-shadow: 0 6px 30px -8px rgba(0, 0, 0, 0.3);
	height: 100%;
	width: 100%;

	display: flex;
	align-items: center;
	align-content: center;

	padding: 0 40px;
	border-radius: 10px;

	@media screen and (min-width: 768px) {
		border-radius: 20px;
		padding: 0 80px;
	}

	background-color: #19232f;

	&,
	a,
	button {
		transition: all 200ms linear;
	}

	a,
	button {
		transform: translateY(100%);
		opacity: 0;
		pointer-events: none;
	}

	&:hover {
		transform: translateY(-20px);

		a,
		button {
			pointer-events: all;
			transform: translateY(30px);
			opacity: 1;
		}
	}

	${(props) => SuperCSS.hydrate(props)}
`;

export { Slideritem, SliderWrapper };
