import React from "react";
import { Flex, Col, Text, Button, Container, Image } from "@components";
import { Banner, SubTitle, Card } from "@modules";

import { Links } from "@constants";
import useTranslate from "@lang";

import user from "@assets/services/user.png";
import results from "@assets/services/results.png";
import connect from "@assets/services/connect.png";
import { useWidth } from "@hooks";

const features = [
	{
		icon: user,
		title: "Trae a un conocido",
		description: (
			<Text>
				Trae a un conocido contigo, si ambos se registran en nuestro
				programa de asociados y firman contrato independiente con
				nosotros,{" "}
				<Text type="strong">
					tú recibes un 25% de descuento en tu primer mes de
					facturación
				</Text>
				.
			</Text>
		),
	},
	{
		icon: connect,
		title: "¿Te gustaría el descuento en tu contrato?",
		description: (
			<Text>
				Trae contigo a 3 personas que se registren en nuestro programa y
				firmen contrato independiente con nosotros,{" "}
				<Text type="strong">
					tú recibes un 25% de descuento sobre el total de tu factura
				</Text>{" "}
				y no sobre el primer mes de servicio.
			</Text>
		),
	},
	{
		icon: results,
		title: "Ganas tú y ganamos nosotros",
		description: (
			<Text>
				Unete a nuestro programa y recomienda nuestras soluciones, si
				alguno de tus referidos firma contrato con nosotros tu{" "}
				<Text type="strong">
					recibes del 5 al 20% de comisión sobre el total de la
					factura
				</Text>{" "}
				, consulta las bases del programa{" "}
				<Text type="link" color="context" href={Links.$afiliateds}>
					aquí
				</Text>
				.
			</Text>
		),
	},
];

const Socios = () => {
	const { isMobile } = useWidth();
	const { trans } = useTranslate();

	return (
		<Container xsPadding={30} mdPadding={40}>
			<Flex items="center" flexWrap="wrap">
				<Col xsWidth="100%" mdWidth="50%" pv={30}>
					<Flex items="center" justify="center">
						<Col mdMaxw={520}>
							<SubTitle
								mb={20}
								items={isMobile ? "center" : "start"}
								justify="center"
								align={isMobile ? "center" : "left"}
							>
								Recibe un{" "}
								<Text type="span" color="context">
									25% de descuento
								</Text>{" "}
								en cualquiera de nuestras soluciones
							</SubTitle>

							{!isMobile && (
								<Flex justify="start">
									<Button
										mt="3rem"
										type="link"
										href={Links.$afiliateds}
										theme="context"
									>
										Quiero comenzar
									</Button>
								</Flex>
							)}
						</Col>
					</Flex>
				</Col>
				<Col xsWidth="100%" mdWidth="50%" pv={30}>
					<Flex items="center" justify="center">
						<Col mdMaxw={560}>
							{!isMobile &&
								features.map(
									({ title, icon, description }, key) => (
										<Flex
											width="100%"
											padding="1rem"
											key={key}
										>
											<Col width={50}>
												<Image
													url={icon}
													dimentions={50}
												/>
											</Col>
											<Col flex={1} pl={30}>
												<Text mdSize={18} mb={20}>
													{title}
												</Text>
												<Col mb={20}>{description}</Col>
											</Col>
										</Flex>
									)
								)}

							{isMobile &&
								features.map(
									({ title, icon, description }, key) => (
										<Flex
											flexWrap="wrap"
											width="100%"
											padding={"1rem"}
											key={key}
										>
											<Flex flexalign="center" md={20}>
												<Image
													url={icon}
													dimentions={60}
												/>
											</Flex>
											<Col width="100%" mt={20}>
												<Text
													mdSize={20}
													mb={20}
													align="center"
												>
													{title}
												</Text>
												<Text align="justify" mb={20}>
													{description}
												</Text>
											</Col>
										</Flex>
									)
								)}
						</Col>
					</Flex>
				</Col>

				{isMobile && (
					<Flex justify="center">
						<Button
							mt="3rem"
							type="link"
							href={Links.$afiliateds}
							theme="context"
						>
							Quiero comenzar
						</Button>
					</Flex>
				)}
			</Flex>
		</Container>
	);
};

export default Socios;
