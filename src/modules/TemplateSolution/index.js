import React from "react";
import { Banner, Clientes, SubTitle, PlanCard } from "@modules";
import { Center, Flex, Col, Text, Button, Image, Container } from "@components";
import Socios from "./Socios";

import { useWidth } from "@hooks";

import asset from "@assets/programa2.jpg";
import Asesoria from "./Asesoria";
import Slider from "./Slider";

const TemplateSolution = ({ data }) => {
	const { isMobile } = useWidth();
	if (!data) return null;

	return (
		<>
			<Banner
				height={500}
				title={data.header.title}
				description={data.header.subtitle}
				handleText={data.header.buttonText}
				url={data.header.url}
			/>

			<Container>
				<Flex flexWrap="wrap" items="center" minHeight={600} pb={40}>
					<Col xsWidth="100%" mdWidth="50%" mb={30}>
						<Center xsPh={20} mdPh={40}>
							<Image
								url={data.intro.image.url}
								name={data.intro.image.name}
								width="100%"
								height="100%"
							/>
						</Center>
					</Col>
					<Center
						xsWidth="100%"
						mdWidth="50%"
						xsPh={20}
						mdPh={0}
						mb={30}
					>
						<Col mdMaxw={600} mdPh={40}>
							<SubTitle align="left" mb={30}>
								{data.intro.title}
							</SubTitle>

							<Text>{data.intro.content}</Text>

							{data.intro.url && (
								<Col mt={30}>
									<Button
										type="link"
										theme="context"
										href={data.intro.url}
									>
										{data.intro.buttonText}
									</Button>
								</Col>
							)}
						</Col>
					</Center>
				</Flex>
			</Container>

			<Col background="#f6f6f6">
				<Container xsPadding={20}>
					<Flex
						flexWrap="wrap"
						items="center"
						minh={600}
						xsPv={40}
						mdPv={0}
					>
						<Center xsWidth="100%" mdWidth="50%" pv={20}>
							<Col mdMaxw={600} mdPh={40}>
								<SubTitle align="left" mb={30}>
									{data.tecnologies.title}
								</SubTitle>

								<Text>{data.tecnologies.content}</Text>

								{data.tecnologies?.url && (
									<Col mt={30}>
										<Button
											type="link"
											theme="context"
											href={data.tecnologies?.url}
										>
											{data.tecnologies?.buttonText}
										</Button>
									</Col>
								)}
							</Col>
						</Center>
						<Center xsWidth="100%" mdWidth="50%" pv={20}>
							<Center mdMaxw={600} flexWrap="wrap">
								{data.tecnologies.gallery.length &&
									data.tecnologies.gallery.map(
										(image, index) => (
											<Image
												key={index}
												url={image}
												dimentions={60}
												margin={20}
												borderRadius={10}
											/>
										)
									)}
							</Center>
						</Center>
					</Flex>
				</Container>
			</Col>

			<Container pv={80}>
				<Center xsMb={40} mdMb={0}>
					<SubTitle align="center" items="center" mdMaxw={900} ph={20}>
						{data.whatdb.title}
					</SubTitle>
				</Center>

				{data.whatdb.content?.section1 && (
					<Flex
						flexWrap="wrap"
						items="center"
						minHeight={600}
						xsPh={20}
						mdPh={0}
						pb={40}
					>
						<Center
							xsWidth="100%"
							mdWidth="50%"
							pv={20}
							style={{ order: isMobile ? 5 : -1 }}
						>
							<Col mdMaxw={600} mdPh={40}>
								{/* {data.whatdb.content.section1.title && (
									<SubTitle align="left" mb={30}>
										{data.whatdb.content.section1.title}
									</SubTitle>
								)} */}
								<Text>
									{data.whatdb.content.section1.content}
								</Text>

								{data.whatdb.content.section1?.url && (
									<Col mt={30}>
										<Button
											type="link"
											theme="context"
											href={
												data.whatdb.content.section1.url
											}
										>
											{
												data.whatdb.content.section1
													.buttonText
											}
										</Button>
									</Col>
								)}
							</Col>
						</Center>
						<Col
							xsWidth="100%"
							mdWidth="50%"
							pv={20}
							style={{ order: isMobile ? -1 : 5 }}
						>
							<Center mdPadding={40}>
								<Image
									url={data.whatdb.content.section1.image.url}
									name={
										data.whatdb.content.section1.image.name
									}
									width="100%"
									height="100%"
								/>
							</Center>
						</Col>
					</Flex>
				)}

				{data.whatdb.content?.section2 && (
					<Flex
						flexWrap="wrap"
						items="center"
						minHeight={600}
						xsMt={40}
						mdMt={0}
						xsPh={20}
						mdPh={0}
						pb={40}
					>
						<Col xsWidth="100%" mdWidth="50%" pv={20}>
							<Center mdPadding={40}>
								<Image
									url={data.whatdb.content.section2.image.url}
									name={
										data.whatdb.content.section2.image.name
									}
									width="100%"
									height="100%"
								/>
							</Center>
						</Col>
						<Center xsWidth="100%" mdWidth="50%" pv={20}>
							<Col mdMaxw={600} mdPh={40}>
								<SubTitle align="left" mb={30}>
									{data.whatdb.content.section2.title}
								</SubTitle>
								<Text>
									{data.whatdb.content.section2.content}
								</Text>

								{data.whatdb.content.section2?.url && (
									<Col mt={30}>
										<Button
											type="link"
											theme="context"
											href={
												data.whatdb.content.section2.url
											}
										>
											{
												data.whatdb.content.section2
													.buttonText
											}
										</Button>
									</Col>
								)}
							</Col>
						</Center>
					</Flex>
				)}

				{data.whatdb.content?.section3 && (
					<Flex
						flexWrap="wrap"
						items="center"
						minHeight={600}
						xsPh={20}
						mdPh={0}
						pb={40}
					>
						<Center
							xsWidth="100%"
							mdWidth="50%"
							pv={20}
							style={{ order: isMobile ? 5 : -1 }}
						>
							<Col mdMaxw={600} mdPh={40}>
								<SubTitle align="left" mb={30}>
									{data.whatdb.content.section3.title}
								</SubTitle>
								<Text>
									{data.whatdb.content.section3.content}
								</Text>

								{data.whatdb.content.section3?.url && (
									<Col mt={30}>
										<Button
											type="link"
											theme="context"
											href={
												data.whatdb.content.section3.url
											}
										>
											{
												data.whatdb.content.section3
													.buttonText
											}
										</Button>
									</Col>
								)}
							</Col>
						</Center>
						<Col
							xsWidth="100%"
							mdWidth="50%"
							pv={20}
							style={{ order: isMobile ? -1 : 5 }}
						>
							<Center mdPadding={40}>
								<Image
									url={data.whatdb.content.section3.image.url}
									name={
										data.whatdb.content.section3.image.name
									}
									width="100%"
									height="100%"
								/>
							</Center>
						</Col>
					</Flex>
				)}
			</Container>

			<Slider data={data.solutions} />

			<Asesoria />

			<Container>
				<Clientes />
			</Container>

			{data?.pymes && (
				<Container pv={40}>
					<Center mb={20} ph={20}>
						<SubTitle align="center" items="center" mdMaxw={900}>
							{data.pymes.title}
						</SubTitle>
					</Center>
					<Center mb={40} ph={20}>
						<Text align="center" items="center" mdMaxw={900}>
							{data.pymes?.description}
						</Text>
					</Center>

					<Flex
						style={{
							scrollBehavior: "smooth",
							scrollSnapType: "x mandatory",
							scrollSnapAlign: "center",
						}}
						overflowX="auto"
						xsJustify="start"
						mdJustify="center"
						minHeight={400}
						xsPh={20}
						mdPh={0}
						xsPv={20}
						mdPv={0}
					>
						{data.pymes.cards.length &&
							data.pymes.cards.map((card, index) => (
								<PlanCard
									xsMinw="90%"
									mdMinw="auto"
									mdMaxw={340}
									key={index}
									xsMargin={10}
									mdMargin={20}
									minHeight={460}
									{...card}
								/>
							))}
					</Flex>
				</Container>
			)}

			{data?.empresas && (
				<Flex background="#141D28" mv={40} justify="center">
					<Container pv={80}>
						<Center mb={20}>
							<SubTitle
								color="white"
								align="center"
								items="center"
								mdMaxw={900}
								lineColor="primary"
							>
								{data.empresas.title}
							</SubTitle>
						</Center>

						<Center mb={40}>
							<Text
								color="white"
								align="center"
								items="center"
								mdMaxw={900}
							>
								{data.empresas?.description}
							</Text>
						</Center>

						<Flex
							style={{
								scrollBehavior: "smooth",
								scrollSnapType: "x mandatory",
								scrollSnapAlign: "center",
							}}
							overflowX="auto"
							xsJustify="start"
							mdJustify="center"
							minHeight={400}
							xsPh={20}
							mdPh={0}
							xsPv={20}
							mdPv={0}
						>
							{data.empresas.cards.length &&
								data.empresas.cards.map((card, index) => (
									<PlanCard
										key={index}
										xsMinw="90%"
										mdMinw="auto"
										mdMaxw={340}
										xsMargin={10}
										mdMargin={20}
										minHeight={460}
										{...card}
									/>
								))}
						</Flex>
					</Container>
				</Flex>
			)}

			{data?.mantenimiento && (
				<Container pv={40}>
					<Center mb={20} ph={20}>
						<SubTitle align="center" items="center" mdMaxw={900}>
							{data.mantenimiento.title}
						</SubTitle>
					</Center>

					<Center mb={40} ph={20}>
						<Text align="center" items="center" mdMaxw={900}>
							{data.mantenimiento?.description}
						</Text>
					</Center>

					<Flex
						style={{
							scrollBehavior: "smooth",
							scrollSnapType: "x mandatory",
							scrollSnapAlign: "center",
						}}
						overflowX="auto"
						xsJustify="start"
						mdJustify="center"
						minHeight={400}
						xsPh={20}
						mdPh={0}
						xsPv={20}
						mdPv={0}
					>
						{data.mantenimiento.cards.length &&
							data.mantenimiento.cards.map((card, index) => (
								<PlanCard
									key={index}
									xsMinw="90%"
									mdMinw="auto"
									mdMaxw={340}
									xsMargin={10}
									mdMargin={20}
									minHeight={460}
									{...card}
								/>
							))}
					</Flex>
				</Container>
			)}

			<Socios />

			<Center mb={50}>
				<Image url={asset} mdMaxw={900} />
			</Center>
		</>
	);
};

export default TemplateSolution;
