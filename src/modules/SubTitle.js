import React from "react";
import { Flex, Title, Line, Col } from "@components";
import { $primary, $context } from "@constants/colors";
// import linea from "@assets/linea.svg";

const colors = {
	primary: $primary,
	context: $context,
};

const SubTitle = ({
	children,
	align = "center",
	type = "h2",
	weight,
	lineColor,
	...props
}) => {
	return (
		<Flex direction="column" {...props}>
			<Title
				type={type}
				mdSize={35}
				align={align}
				weight={weight || "bold"}
			>
				{children}
			</Title>

			<Line width={60} height={20} color={colors[lineColor]} />
		</Flex>
	);
};

export default SubTitle;
