import React from "react";
import styled from "styled-components";
import { SuperCSS } from "@utils";

import { Image, Title, Text, Button, Flex } from "@components";

const Wrapper = styled.div`
	display: block;
	border-radius: 10px;
	overflow: hidden;
	box-shadow: 0 6px 15px -4px rgba(0, 0, 0, 0.2);
	position: relative;

	transition: transform 600ms ease;

	background-color: black;

	picture {
		width: 100%;
		height: 100%;
		position: absolute;
		top: 0;
		left: 0;
		z-index: 1;
		pointer-events: none;
		transition: all 400ms ease-out;
	}

	a {
		transition: all 300ms ease-out;
		position: relative;
	}

	@media screen and (min-width: 768px) {
		a {
			opacity: 0;
			pointer-events: none;
			transform: translateY(300px);
		}

		&:hover {
			transform: translateY(-10px);

			.overlay {
				backdrop-filter: blur(6px);
			}
			picture {
				transform: scale(1.2);
			}

			a {
				pointer-events: all;
				opacity: 1;
				transform: translateY(0);
			}
		}
	}

	${(props) => SuperCSS.hydrate(props)}
`;

const Content = styled.div`
	display: flex;
	flex-direction: column;

	align-items: center;
	justify-content: center;
	position: relative;
	z-index: 3;

	width: 100%;
	height: 100%;

	padding: 2rem 1rem;
	${(props) => SuperCSS.hydrate(props)}
`;

const Overlay = styled.div`
	display: block;
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	z-index: 2;
	pointer-events: none;
	transition: backdrop-filter 100ms linear;

	background-color: rgba(0, 0, 0, 0.3);
	${(props) => SuperCSS.hydrate(props)}
`;

const BlurCard = ({
	icon,
	title,
	description,
	url,
	source,
	tag,
	theme,
	...props
}) => {
	const color = theme !== "dark" ? "#141D28" : "white";

	return (
		<Wrapper color={color} {...props}>
			<Image fit="cover" url={source} />
			<Overlay className="overlay" />
			<Content>
				<Text mdSize={14} mb={10}>
					{tag || ""}
				</Text>
				<Title
					align="center"
					type="h3"
					mb={20}
					mdMaxw="80%"
					mdSize={28}
					weight="400"
				>
					{title}
				</Title>
				<Text align="center" mb={30} mdMaxw="80%">
					{description}
				</Text>
				{url && (
					<Flex justify="center">
						<Button type="link" href={url} theme="context">
							Leer articulo
						</Button>
					</Flex>
				)}
			</Content>
		</Wrapper>
	);
};

export default BlurCard;
