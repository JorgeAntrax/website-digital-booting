import React from "react";
import { Links } from "@constants";
import { Text, image, Svg, Title } from "@components";
import { SubTitle } from "@modules";

import marketing from "@assets/marketing.svg";
import persons from "@assets/persons.svg";
import designthink from "@assets/designthink.svg";
import neuromkt from "@assets/neuromkt.svg";

import wordpress from "@assets/tecnologies/wordpress.png";
import wix from "@assets/tecnologies/wix.png";
import elementor from "@assets/tecnologies/elementor.png";
import figma from "@assets/tecnologies/figma.png";
import html from "@assets/tecnologies/html.png";
import css from "@assets/tecnologies/css.png";
import laravel from "@assets/tecnologies/laravel.png";
import vue from "@assets/tecnologies/vue.png";
import react from "@assets/tecnologies/react.png";
import javascript from "@assets/tecnologies/javascript.png";
import angular from "@assets/tecnologies/angular.png";
import miro from "@assets/tecnologies/miro.png";
import asana from "@assets/tecnologies/asana.png";
import analitics from "@assets/tecnologies/analitics.png";
import semrush from "@assets/tecnologies/semrush.png";
import hootsuite from "@assets/tecnologies/hootsuite.png";
import gpt from "@assets/tecnologies/gpt.jpg";

import landing from "@assets/solutions/landing.png";

export default {
	header: {
		title: "Agencia IA Marketing con Inteligencia Artificial",
		subtitle: (
			<>
				Ofrecemos soluciones personalizadas de marketing con
				inteligencia artificial (IA) que te ayudarán a aumentar tu
				presencia en línea, atraer a desconocidos y convertirlos en
				clientes cualificados para tu negocio.
			</>
		),
		buttonText: "Quiero una auditoria gratis",
		url: Links.$contact,
	},
	intro: {
		title: (
			<>
				Tu agencia de marketing digital con IA en{" "}
				<Text type="span" color="context">
					méxico
				</Text>
			</>
		),
		content: (
			<>
				Nuestro objetivo en <Text type="strong">Digital Booting</Text>{" "}
				es ayudarte a crecer tu negocio en línea. Para lograrlo,
				utilizamos <Text type="strong">soluciones innovadoras</Text> de
				marketing digital que aprovechan todo el potencial de la{" "}
				<Text type="strong">inteligencia artificial</Text>(IA) para
				aumentar tu presencia en línea y convertir a desconocidos en
				clientes cualificados para tu empresa.
			</>
		),
		buttonText: "Quiero una asesoria",
		url: Links.$contact,
		image: {
			name: "agencia de marketing en méxico",
			url: marketing,
		},
	},
	tecnologies: {
		title: "Tecnologias de marketing digital",
		content: (
			<>
				Nuestro equipo de expertos altamente capacitados en diversas{" "}
				<Text type="strong" color="context">
					tecnologías de marketing digital
				</Text>{" "}
				son capaces de crear nuevas estrategias basadas en inteligencia
				artificial y la creatividad humana, logrando aumentar tu
				presencia en linea. Esto se traduce en ventas para tu empresa,
				así como{" "}
				<Text type="strong" color="context">
					retornos de inversión en publicidad,
				</Text>{" "}
				entre otras ventajas.
			</>
		),
		gallery: [
			gpt,
			wordpress,
			wix,
			elementor,
			figma,
			html,
			css,
			laravel,
			vue,
			react,
			javascript,
			angular,
			miro,
			asana,
			analitics,
			semrush,
			hootsuite,
		],
	},
	whatdb: {
		title: (
			<>
				¿Qué hace diferente a{" "}
				<Text type="span" color="context">
					Digital Booting
				</Text>{" "}
				de otras agencias de marketing digital?
			</>
		),
		content: {
			section1: {
				title: (
					<>
						Estrategias basadas en{" "}
						<Text type="span" color="context">
							Inteligencia artificial
						</Text>
					</>
				),
				content: (
					<>
						Creamos estrategias innovadoras que utilizan{" "}
						<Text type="strong" color="context">
							tecnologías de IA
						</Text>{" "}
						para{" "}
						<Text type="strong">
							automatizar y optimizar tareas de marketing{" "}
						</Text>
						como la segmentación de clientes, la creación de
						contenidos, y la conversión de desconocidos en clientes
						cualificados.
					</>
				),
				buttonText: "Quiero una asesoria gratis",
				url: Links.$contact,
				image: {
					url: persons,
					name: "atracción de clientes",
				},
			},
			section2: {
				title: (
					<>
						Neuro-marketing +{" "}
						<Text type="span" color="context">
							Inteligencia artificial
						</Text>
					</>
				),
				content: (
					<>
						En <Text type="strong">Digital Booting</Text> ofrecemos
						soluciones innovadoras basadas en el{" "}
						<Text type="strong" color="context">
							neuromarketing y la inteligencia artificial
						</Text>
						. Utilizamos herramientas de IA tecnológicas para
						analizar el comportamiento de tus clientes y ofrecerles
						experiencias personalizadas. Con nuestra ayuda, podrás{" "}
						<Text type="strong">
							impulsar tu negocio y alcanzar tus objetivos de
							marketing
						</Text>
						.
					</>
				),
				image: {
					url: neuromkt,
					name: "automatizacion IA",
				},
			},
			section3: {
				title: (
					<>
						Utilizamos la metodolgía{" "}
						<Text type="span" color="context">
							Design Thinking
						</Text>
					</>
				),
				content: (
					<>
						Pensamos primero en el usuario centrando nuestras{" "}
						<Text type="strong" color="context">
							estrategias de segmentación de mercado
						</Text>
						, utilizando herramientas avanzadas de inteligencia
						artificial para{" "}
						<Text type="strong">
							analizar el comportamiento de tus clientes y
							ofrecerles experiencias personalizadas
						</Text>
						.
					</>
				),
				image: {
					url: designthink,
					name: "metodologia design thinking",
				},
			},
		},
	},
	solutions: {
		title: (
			<>
				Nuestras soluciones en{" "}
				<Text type="span" color="context">
					marketing digital
				</Text>
			</>
		),
		sliders: [
			{
				title: "Marketing de contenidos",
				content: (
					<>
						<Text type="strong" color="primary">
							El Marketing de contenidos
						</Text>{" "}
						es ofrecerte a ti y a tu audiencia contenido valioso y
						relevante. Así, podrás atraer su atención, generar
						confianza y crear una relación duradera con ellos y con
						tu negocio. ¿Te gustaría saber más?
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Facebook, Instagram y WhatsApp Ads",
				content: (
					<>
						te ayudamos con la{" "}
						<Text type="strong" color="primary">
							creación y administración
						</Text>{" "}
						de campañas en Facebook, Instagram y WhatsApp mediante
						herramientas y recursos de IA que te permiten optimizar
						tu presupuesto y tu rendimiento. ¿Te gustaría saber más?
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Marketing emocional + IA",
				content: (
					<>
						Potenciamos el Marketing emocional con IA para ofrecerle
						a tu audiencia
						<Text type="strong" color="primary">
							anuncios personalizados y efectivos
						</Text>{" "}
						que conecten con sus emociones y necesidades. ¿Te
						gustaría saber más?
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Marketing basado en IA predictiva",
				content: (
					<>
						Mediante analisis predictivo con inteligencia artificial{" "}
						<Text type="strong" color="primary">
							anticipamos las necesidades
						</Text>{" "}
						de tus posibles clientes, aprendiendo de ellos para
						mejorar el rendimiento de neustras estrategias de
						marketing.
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Neuro-Marketing",
				content: (
					<>
						<Text type="strong" color="primary">
							Neuro Marketing
						</Text>{" "}
						es ofrecerte a ti y a tu audiencia anuncios que
						estimulen su cerebro y sus emociones. Así, podrás
						entender mejor sus necesidades, motivaciones y
						preferencias.
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Marketing de IA conversacional",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
		],
	},
	// pymes: {
	// 	title: (
	// 		<>
	// 			Soluciones para{" "}
	// 			<Text type="span" color="context">
	// 				PyMEs y Emprendedores
	// 			</Text>
	// 		</>
	// 	),
	// 	description: (
	// 		<>
	// 			consolidamos y fidelizamos tu marca utilizando tecnologia de IA
	// 			con estrategias de marketing innovadoras enfocadas para PyMES y
	// 			emprendedores.
	// 		</>
	// 	),
	// 	cards: [
	// 		{
	// 			theme: "white",
	// 			category: "Inicial",
	// 			price: "2599",
	// 			currency: "MXN",
	// 			plan: "Pago mensual + IVA",
	// 			url: Links.$contact,
	// 			features: [
	// 				"4 Publicaciones al mes",
	// 				"4 Copys de publicaciones al mes",
	// 				"1 Campaña de pago al mes",
	// 				"Conversión de leads via WhatsApp",
	// 				"Optimización de campaña con IA",
	// 				"Insights basados en analisis IA",
	// 				"Reporte mensual",
	// 			],
	// 		},
	// 		{
	// 			theme: "dark",
	// 			category: "Básico",
	// 			price: "4599",
	// 			currency: "MXN",
	// 			plan: "Pago mensual + IVA",
	// 			url: Links.$contact,
	// 			features: [
	// 				"Todo el paquete inicial",
	// 				"+ 4 publicaciones adicionales",
	// 				"+ 4 copys de publicaciones",
	// 				"+ 1 Campaña adicional",
	// 				"Analisis de público objetivo",
	// 				"Segmentación de usuarios",
	// 			],
	// 		},
	// 		{
	// 			theme: "white",
	// 			category: "Profesional",
	// 			price: "7599",
	// 			currency: "MXN",
	// 			plan: "Pago mensual + IVA",
	// 			url: Links.$contact,
	// 			features: [
	// 				"Todo el paquete básico",
	// 				"+ 4 publicaciones extra",
	// 				"+ 4 copys extra",
	// 				"+ 1 Campaña adicional",
	// 				"Analisis de desempeño predictivo con IA",
	// 				"Segmentación avanzada utilizando IA",
	// 			],
	// 		},
	// 		{
	// 			theme: "dark",
	// 			category: "Flexible",
	// 			price: "Flexible",
	// 			currency: "Plan flexible",
	// 			plan: "Cotiza solo lo necesario",
	// 			featured: true,
	// 			url: Links.$contact,
	// 			features: [
	// 				"Plan esclusivo",
	// 				"Diseñamos la estrategia perfecta para ti, justo a la medida de tus necesidades.",
	// 			],
	// 		},
	// 	],
	// },
	// empresas: {
	// 	title: (
	// 		<>
	// 			Soluciones para{" "}
	// 			<Text type="span" color="primary">
	// 				Startups y empresas
	// 			</Text>
	// 		</>
	// 	),
	// 	description: (
	// 		<>
	// 			Si buscas expandir tu mercado actual e incrementar tus ventas,
	// 			estas estrategias de marketing basadas en IA son perfectas para
	// 			startups y empresas.
	// 		</>
	// 	),
	// 	cards: [
	// 		{
	// 			theme: "white",
	// 			category: "Inicial",
	// 			price: "8599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			plan: "Pago mensual + IVA",
	// 			features: [
	// 				"16 publicaciones",
	// 				"16 copys de publicaciones",
	// 				"4 Campañas de pago al mes",
	// 			],
	// 		},
	// 		{
	// 			theme: "darkover",
	// 			category: "Básico",
	// 			price: "14599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			plan: "Pago mensual + IVA",
	// 			features: [
	// 				"16 publicaciones",
	// 				"16 copys de publicaciones",
	// 				"4 Campañas de pago al mes",
	// 			],
	// 		},
	// 		{
	// 			theme: "white",
	// 			category: "Profesional",
	// 			price: "16599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			plan: "Pago mensual + IVA",
	// 			features: [
	// 				"16 publicaciones",
	// 				"16 copys de publicaciones",
	// 				"4 Campañas de pago al mes",
	// 			],
	// 		},
	// 		{
	// 			theme: "darkover",
	// 			category: "Negocios",
	// 			price: "12599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			plan: "Pago mensual + IVA",
	// 			features: [
	// 				"16 publicaciones",
	// 				"16 copys de publicaciones",
	// 				"4 Campañas de pago al mes",
	// 			],
	// 		},
	// 	],
	// },
	// mantenimiento: {
	// 	title: (
	// 		<>
	// 			Paquetes de{" "}
	// 			<Text type="span" color="context">
	// 				Mantenimiento
	// 			</Text>{" "}
	// 			para marketing digital
	// 		</>
	// 	),
	// 	cards: [
	// 		{
	// 			theme: "white",
	// 			category: "Inicial",
	// 			price: "1599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "dark",
	// 			category: "Básico",
	// 			price: "3599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "white",
	// 			category: "Profesional",
	// 			price: "6599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "dark",
	// 			category: "Negocios",
	// 			price: "9599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 	],
	// },
};
