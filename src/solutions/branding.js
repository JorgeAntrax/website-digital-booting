import React from "react";
import { Links } from "@constants";
import { Text, image, Svg, Title } from "@components";
import { SubTitle } from "@modules";

import brand from "@assets/brand.svg";
import brand2 from "@assets/brand.jpg";
import brand3 from "@assets/brand2.svg";
import neuromkt from "@assets/neuromkt.svg";

import wordpress from "@assets/tecnologies/wordpress.png";
import wix from "@assets/tecnologies/wix.png";
import elementor from "@assets/tecnologies/elementor.png";
import figma from "@assets/tecnologies/figma.png";
import html from "@assets/tecnologies/html.png";
import css from "@assets/tecnologies/css.png";
import laravel from "@assets/tecnologies/laravel.png";
import vue from "@assets/tecnologies/vue.png";
import react from "@assets/tecnologies/react.png";
import javascript from "@assets/tecnologies/javascript.png";
import angular from "@assets/tecnologies/angular.png";
import miro from "@assets/tecnologies/miro.png";
import asana from "@assets/tecnologies/asana.png";
import analitics from "@assets/tecnologies/analitics.png";
import semrush from "@assets/tecnologies/semrush.png";
import hootsuite from "@assets/tecnologies/hootsuite.png";

import landing from "@assets/solutions/landing.png";

export default {
	header: {
		title: "Diseño y creación de Marca con IA en México",
		subtitle: (
			<>
				Utilizamos tecnologías avanzadas, como la inteligencia
				artificial, para crear una identidad de marca única y atractiva
				que conecte con tu público objetivo.
			</>
		),
		buttonText: "Quiero una auditoria gratis",
		url: Links.$contact,
	},
	intro: {
		title: (
			<>
				Nos conectamos con tu marca a un nivel{" "}
				<Text type="span" color="context">
					personal
				</Text>
			</>
		),
		content: (
			<>
				Analizamos cada detalle de tu negocio desde una perspectiva
				enfocada al usuario y{" "}
				<Text type="strong">utilizando tecnologías de IA</Text> para
				poder proyectar de una manera simple e impactante la visión y
				los valores de tu marca.
				<br />
				<br />
				En <Text type="strong">Digital Booting</Text> logramos que tu
				identidad digital empatice con todos aquellos desconocidos que
				están buscando tus servicios por internet, gracias a la{" "}
				<Text type="strong" color="context">
					capacidad de la IA para personalizar y mejorar la
					experiencia del usuario
				</Text>
				.
			</>
		),
		image: {
			name: "agencia de diseño en méxico",
			url: brand,
		},
	},
	tecnologies: {
		title: (
			<>
				Tecnologias de diseño y{" "}
				<Text type="span" color="context">
					creación de marca
				</Text>
				.
			</>
		),
		content: (
			<>
				Nuestro equipo de expertos altamente capacitados en diversas{" "}
				<Text type="strong" color="context">
					tecnologías de diseño
				</Text>{" "}
				son capaces de crear conceptos de marca impresionantes basados
				en inteligencia artificial y la creatividad humana, logrando que
				tu <Text type="strong">identidad digital</Text> empatice con
				todos aquellos usuarios que estan buscando tus soluciones por
				internet.
			</>
		),
		gallery: [
			wordpress,
			wix,
			elementor,
			figma,
			html,
			css,
			laravel,
			vue,
			react,
			javascript,
			angular,
			miro,
			asana,
			analitics,
			semrush,
			hootsuite,
		],
	},
	whatdb: {
		title: (
			<>
				¿Qué hace diferente a{" "}
				<Text type="span" color="context">
					Digital Booting
				</Text>{" "}
				de otras agencias de diseño gráfico?
			</>
		),
		content: {
			section1: {
				content: (
					<>
						Además de{" "}
						<Text type="strong">diseñar un logo bonito</Text> que
						represente la visión y los valores de tu negocio,
						tambien desarrollamos el naming, tu sitio web
						profesional y todas las estrategias de posicionamiento
						SEO, Marketing Digital y Social Media.
						<br />
						<br />
						Como{" "}
						<Text type="strong">
							agencia de branding y diseño de identidad
							corporativa
						</Text>{" "}
						tambien registramos tu marca ante las autoridades IMPI
						(Instituto Mexicano de la Propiedad Industrial).
					</>
				),
				buttonText: "Quiero una asesoria gratis",
				url: Links.$contact,
				image: {
					url: brand2,
					name: "construimos mas que una marca",
				},
			},
			section2: {
				title: (
					<>
						Tú estrategia desde el{" "}
						<Text type="span" color="context">
							primer contacto
						</Text>
						.
					</>
				),
				content: (
					<>
						Creamos tu estrategia de marca pensando en como va a
						desarrollarse el proyecto a largo plazo, trabajamos en
						cada detalle de tu marca para hacerla única e impactante
						utilizando a nuestro favor{" "}
						<Text type="strong">
							tecnologia de inteligencia artificial
						</Text>
						.
						<br />
						<br /> Nos importa que tu nueva identidad digital tenga
						esa atracción que posee cualquier marca exitosa.
					</>
				),
				image: {
					url: brand3,
					name: "estrategia de marca",
				},
			},
		},
	},
	solutions: {
		title: (
			<>
				Nuestras soluciones en diseño gráfico y{" "}
				<Text type="span" color="context">
					creación de marca
				</Text>
			</>
		),
		sliders: [
			{
				title: "Registro de marca ante IMPI",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Diseño basado en IA creativa",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Creación de identidad visual",
				url: Links.$contact,
			},
			{
				title: "Re-branding profesional",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
		],
	},
	// pymes: {
	// 	title: (
	// 		<>
	// 			Soluciones para{" "}
	// 			<Text type="span" color="context">
	// 				PyMEs y Emprendedores
	// 			</Text>
	// 		</>
	// 	),
	// 	cards: [
	// 		{
	// 			theme: "white",
	// 			category: "Inicial",
	// 			price: "3599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UI de logotipo",
	// 				"Manual de indentidad",
	// 				"Papeleria corporativa",
	// 			],
	// 		},
	// 		{
	// 			theme: "dark",
	// 			category: "Básico",
	// 			price: "5599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"4 Bloques de contenido",
	// 				"4 Bloques de contenido",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "white",
	// 			category: "Profesional",
	// 			price: "8599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"Hasta 800 palabras",
	// 				"Hasta 800 palabras",
	// 				"Hasta 800 palabras",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "dark",
	// 			category: "Negocios",
	// 			price: "10599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 	],
	// },
	// empresas: {
	// 	title: (
	// 		<>
	// 			Soluciones para{" "}
	// 			<Text type="span" color="primary">
	// 				Startups y empresas
	// 			</Text>
	// 		</>
	// 	),
	// 	cards: [
	// 		{
	// 			theme: "white",
	// 			category: "Inicial",
	// 			price: "4599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "darkover",
	// 			category: "Básico",
	// 			price: "8599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "white",
	// 			category: "Profesional",
	// 			price: "12599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "darkover",
	// 			category: "Negocios",
	// 			price: "15599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 	],
	// },
	// mantenimiento: {
	// 	title: (
	// 		<>
	// 			Paquetes de{" "}
	// 			<Text type="span" color="context">
	// 				diseño
	// 			</Text>{" "}
	// 			asistido mensualmente
	// 		</>
	// 	),
	// 	cards: [
	// 		{
	// 			theme: "white",
	// 			category: "Inicial",
	// 			price: "1599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "dark",
	// 			category: "Básico",
	// 			price: "3599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "white",
	// 			category: "Profesional",
	// 			price: "6599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "dark",
	// 			category: "Negocios",
	// 			price: "9599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 	],
	// },
};
