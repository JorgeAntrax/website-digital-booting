import React from "react";
import { Links } from "@constants";
import { Text, image, Svg, Title } from "@components";
import { SubTitle } from "@modules";

import webdesign from "@assets/programa2.jpg";
import g1 from "@assets/g1.svg";
import g2 from "@assets/g2.jpg";
import gg2 from "@assets/g2.svg";

import wordpress from "@assets/tecnologies/wordpress.png";
import wix from "@assets/tecnologies/wix.png";
import elementor from "@assets/tecnologies/elementor.png";
import figma from "@assets/tecnologies/figma.png";
import html from "@assets/tecnologies/html.png";
import css from "@assets/tecnologies/css.png";
import laravel from "@assets/tecnologies/laravel.png";
import vue from "@assets/tecnologies/vue.png";
import react from "@assets/tecnologies/react.png";
import javascript from "@assets/tecnologies/javascript.png";
import angular from "@assets/tecnologies/angular.png";
import miro from "@assets/tecnologies/miro.png";
import asana from "@assets/tecnologies/asana.png";
import analitics from "@assets/tecnologies/analitics.png";
import semrush from "@assets/tecnologies/semrush.png";
import hootsuite from "@assets/tecnologies/hootsuite.png";

import landing from "@assets/solutions/landing.png";

export default {
	header: {
		title: "Diseño Web",
		subtitle: (
			<>
				Somos una agencia de diseño de páginas web en México con
				presencia en Estados unidos que tiene la capacidad de crear para
				tu negocio una{" "}
				<Text type="strong">presencia en línea atractiva</Text> y
				adaptable a las{" "}
				<Text type="strong">tendencias del mercado</Text>.
			</>
		),
		buttonText: "Quiero una auditoria gratis",
		url: Links.$contact,
	},
	intro: {
		title: (
			<>
				Tu agencia de diseño web en{" "}
				<Text type="span" color="context">
					méxico
				</Text>
			</>
		),
		content: (
			<>
				Nuestra agencia de{" "}
				<Text type="strong">diseño de páginas web en México</Text>{" "}
				ofrece soluciones digitales para asegurar que tu negocio tenga
				una{" "}
				<Text type="strong" color="context">
					presencia en línea atractiva y efectiva
				</Text>
				. Nuestro equipo de expertos en diseño y desarrollo web
				trabajará contigo para crear un sitio web que refleje tu marca y
				atraiga a tus clientes potenciales.
			</>
		),
		buttonText: "Quiero una asesoria",
		url: Links.$contact,
		image: {
			name: "agencia de diseño web",
			url: g1,
		},
	},
	tecnologies: {
		title: "Tecnologias de diseño web",
		content: (
			<>
				Nuestro equipo de expertos altamente capacitados en diversas{" "}
				<Text type="strong" color="context">
					tecnologías de marketing digital y diseño web
				</Text>{" "}
				son capaces de darle vida a tu empresa en la web logrando
				experiencias únicas para tus posibles clientes. Esto se traduce
				en ventas para tu empresa, así como{" "}
				<Text type="strong" color="context">
					ahorro en inversiones de publicidad
				</Text>{" "}
				entre otras ventajas.
			</>
		),
		gallery: [
			wordpress,
			wix,
			elementor,
			figma,
			html,
			css,
			laravel,
			vue,
			react,
			javascript,
			angular,
			miro,
			asana,
			analitics,
			semrush,
			hootsuite,
		],
	},
	whatdb: {
		title: (
			<>
				¿Qué hace diferente a{" "}
				<Text type="span" color="context">
					Digital Booting
				</Text>{" "}
				de otras agencias de diseño web?
			</>
		),
		content: {
			section1: {
				title: (
					<>
						<Text type="span" color="context">
							Estudiamos y analizamos
						</Text>{" "}
						tu negocio
					</>
				),
				content: (
					<>
						Investigamos tu negocio, metas, objetivos y el sector en
						el que te desenvuelves, lo que nos permite saber como
						funciona tu{" "}
						<Text type="span" color="context">
							nicho de mercado
						</Text>
						, que es lo que busca tu comprador ideal{" "}
						<Text type="span" color="context">
							(Buyer Persona)
						</Text>{" "}
						y asi implementar la logística que tu sitio web
						necesita, desde el diseño gráfico hasta desarrollo y{" "}
						<Text type="span" color="context">
							posicionamiento SEO
						</Text>
						.
					</>
				),
				buttonText: "Quiero una asesoria gratis",
				url: Links.$contact,
				image: {
					url: g2,
					name: "paginas web",
				},
			},
			section2: {
				title: (
					<>
						Somos una agencia de{" "}
						<Text type="span" color="context">
							Desarrollo Ágil
						</Text>
					</>
				),
				content: (
					<>
						En todo momento{" "}
						<Text type="strong">eres parte del proceso</Text>, te
						llevamos de la mano con nuestro equipo de expertos
						durante toda la{" "}
						<Text type="span" color="context">
							planificación, diseño, prototipado y desarrollo de
							tu página web
						</Text>
						, por lo que podrás ver mejoras incrementales en tu
						proyecto, esto nos permite adaptarnos a las necesidades
						del mercado y actuar de una manera rápida y eficiente.
					</>
				),
				image: {
					url: gg2,
					name: "desarrollo agil",
				},
			},
		},
	},
	solutions: {
		title: (
			<>
				Nuestras soluciones en{" "}
				<Text type="span" color="context">
					Diseño Web
				</Text>
			</>
		),
		sliders: [
			{
				title: "Desarrollo de Chatbots",
				content: (
					<>
						<Text type="strong" color="primary">
							integramos tecnología de IA
						</Text>{" "}
						en cada chatbot que desarrollamos para brindar una
						atención más personalizada a los visitantes de tu sitio
						web.
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "E-commerce inteligentes",
				content: (
					<>
						<Text type="strong" color="primary">
							Ofrece tus productos por internet
						</Text>{" "}
						al publico correcto con una tienda virtual y los
						beneficios de la inteligencia artificial.
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Desarrollo de sistemas CRM, CMS",
				content: (
					<>
						<Text type="strong" color="primary">
							Automatizamos el area digital de tu empresa
						</Text>{" "}
						desarrollando sistemas de control y automatización
						profesionales.
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Diseño de Landing Page",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes.
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Desarrollo web a la medida",
				content: (
					<>
						<Text type="strong" color="primary">
							Sin plantillas o templates
						</Text>
						, todos nuestros clientes cuentan con una página web
						completamente personalizada y única.
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Creación de contenido",
				content: (
					<>
						<Text type="strong" color="primary">
							Generamos y optimizamos
						</Text>{" "}
						todo el contenido de tu sitio web para lograr resultdos
						que impacten no solo a empresa, si no tambien a tus
						clientes.
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Mantenimiento integral",
				content: (
					<>
						<Text type="strong" color="primary">
							Cuidamos
						</Text>{" "}
						que la presencia digital de tu marca sea impecable con
						el mantenimiento y actualización adecuados.
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Desarrollo de web apps",
				content: (
					<>
						<Text type="strong" color="primary">
							Desarrollamos aplicaciones web
						</Text>{" "}
						que pueden facilmente comportarse como un aplicación
						movil, utilizando las mejores herramientas de desarrollo
						actual.
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
		],
	},
	pymes: {
		title: (
			<>
				Soluciones para{" "}
				<Text type="span" color="context">
					PyMEs y Emprendedores
				</Text>
			</>
		),
		description: (
			<>
				Estos paquetes son perfectos para PyMEs y emprendedores que
				buscan tener una presencia en linea y un sitio web profesional
				en un corto plazo.
			</>
		),
		cards: [
			{
				theme: "white",
				category: "Inicial",
				price: "4999",
				currency: "MXN",
				plan: "Pago único + IVA",
				url: Links.$contact,
				features: [
					"1 UX Homepage",
					"10 Imagenes de stock",
					"Hasta 500 palabras",
					"Hasta 4 bloques de contenido",
					"1 formulario de contacto",
					"Link a redes sociales",
					"Entrega de 1 a 2 semanas",
				],
			},
			{
				theme: "dark",
				category: "Básico",
				price: "6999",
				currency: "MXN",
				plan: "Pago único + IVA",
				url: Links.$contact,
				features: [
					"Todo el plan inicial",
					"1 UX de sección interna",
					"Hasta 6 bloques de contenido",
					"Optimización SEO básica",
					"Google Maps estático",
					"1 Cuenta de correo personalizada",
					"Sección de aviso de privacidad",
				],
			},
			{
				theme: "white",
				category: "Profesional",
				price: "9999",
				currency: "MXN",
				plan: "Pago único + IVA",
				url: Links.$contact,
				features: [
					"Todo el plan básico",
					"3 UX de secciones internas",
					"3 Cuentas de correo profesional",
					"Google Maps interactivo",
					"20 Imagenes de stock",
					"Integración con Google analitics",
					"Entrega de 2 a 4 semanas",
				],
			},
			{
				theme: "dark",
				category: "Flexible",
				price: "Flexible",
				currency: "Plan flexible",
				plan: "Cotiza solo lo necesario",
				featured: true,
				url: Links.$contact,
				features: [
					"Plan exclusivo",
					"Este es nuestro paquete con más alta demanda debido a su flexibilidad que ofrecemos para PyMES o emprendedores, incluyendo solo lo necesario en la cotización final.",
				],
			},
		],
	},
	empresas: {
		title: (
			<>
				Soluciones para{" "}
				<Text type="span" color="primary">
					Startups y empresas
				</Text>
			</>
		),
		description: (
			<>
				Estos paquetes son ideales para startups y empresas que
				necesitan un solucion e infraestructura digital más avanzada y
				con mayor rendimiento.
			</>
		),
		cards: [
			{
				theme: "white",
				category: "Startup",
				price: "19999",
				currency: "MXN",
				plan: "Pago único + IVA",
				url: Links.$contact,
				features: [
					"1 UX Website",
					"Hasta 30 Imagenes de stock",
					"Hasta 1000 palabras por sección",
					"Hasta 6 bloques de contenido",
					"Hasta 4 secciones internas",
					"1 formulario de contacto",
					"Optimización SEO intermedio",
					"Integración con Google analitics",
					"6 Cuentas de correo profesional",
					"Entrega de 3 a 4 semanas",
				],
			},
			{
				theme: "darkover",
				category: "Avanzado",
				price: "29999",
				currency: "MXN",
				plan: "Pago único + IVA",
				url: Links.$contact,
				features: [
					"Todo el paquete startup",
					"Hasta 6 secciones internas",
					"Optimización SEO avanzada",
					"1 formulario de contacto",
					"Hasta 40 imagenes de Stock",
					"Chatbot de atencion al cliente",
					"Entrega de 4 a 6 semanas",
				],
			},
			{
				theme: "white",
				category: "Premium",
				price: "39999",
				currency: "MXN",
				plan: "Pago único + IVA",
				url: Links.$contact,
				features: [
					"Todo el paquete avanzado",
					"Hasta 8 secciones internas",
					"Login y registro de usuarios",
					"Dashboard administrativo",
					"Integración con CRM CMS, etc.",
					"Chatbot + IA",
				],
			},
			{
				category: "Flexible",
				price: "Flexible",
				currency: "Plan flexible",
				plan: "Cotiza solo lo necesario",
				featured: true,
				url: Links.$contact,
				features: [
					"Plan exclusivo",
					"Nuestro paquete personalizado de alto rendimiento es el más demandado por startups y empresas, te ofrecemos solo lo necesario para tus objetivos de negocio.",
				],
			},
		],
	},
	mantenimiento: {
		title: (
			<>
				Paquetes de{" "}
				<Text type="span" color="context">
					Mantenimiento
				</Text>{" "}
				en páginas web
			</>
		),
		description: (
			<>
				Contar con una buena agencia de marketing con paquetes de
				mantenimiento en diseño y desarrollo web es vital para cuidar
				que la presencia digital de tu marca sea impecable
			</>
		),
		cards: [
			{
				theme: "white",
				category: "Inicial",
				price: "2999",
				currency: "MXN",
				plan: "Pago mensual + IVA",
				url: Links.$contact,
				features: [
					"1 Respaldo por mes",
					"Soporte tecnico via Zoom",
					"Monitoreo de seguridad",
					"Optimización de formularios",
					"2 Actualizaciones de contenido",
				],
			},
			{
				theme: "dark",
				category: "Básico",
				price: "6999",
				currency: "MXN",
				plan: "Pago mensual + IVA",
				url: Links.$contact,
				features: [
					"Todo el paquete inicial",
					"Optimización de rendimiento",
					"2 Respaldos por mes",
					"Soporte vía WhatsApp",
					"4 Actualizaciones de contenido",
				],
			},
			{
				theme: "white",
				category: "Profesional",
				price: "9999",
				currency: "MXN",
				plan: "Pago mensual + IVA",
				url: Links.$contact,
				features: [
					"Todo el paquete básico",
					"Asistencia remota",
					"Reporte mensual",
					"Optimización de base de datos",
					"6 Actualizaciones de contenido",
				],
			},
			{
				theme: "dark",
				category: "Flexible",
				price: "Flexible",
				currency: "Plan flexible",
				plan: "Cotiza solo lo necesario",
				featured: true,
				url: Links.$contact,
				features: [
					"Plan exclusivo",
					"Creamos tu plan de mantenimiento a la medida de tus necesidades",
				],
			},
		],
	},
};
