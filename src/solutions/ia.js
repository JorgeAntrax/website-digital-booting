import React from "react";
import { Links } from "@constants";
import { Text, image, Svg, Title } from "@components";
import { SubTitle } from "@modules";

import ia1 from "@assets/ia1.webp";
import ia2 from "@assets/ia2.webp";
import ia3 from "@assets/ia3.webp";
import ia4 from "@assets/ia4.webp";

import wordpress from "@assets/tecnologies/wordpress.png";
import wix from "@assets/tecnologies/wix.png";
import elementor from "@assets/tecnologies/elementor.png";
import figma from "@assets/tecnologies/figma.png";
import html from "@assets/tecnologies/html.png";
import css from "@assets/tecnologies/css.png";
import laravel from "@assets/tecnologies/laravel.png";
import vue from "@assets/tecnologies/vue.png";
import react from "@assets/tecnologies/react.png";
import javascript from "@assets/tecnologies/javascript.png";
import angular from "@assets/tecnologies/angular.png";
import miro from "@assets/tecnologies/miro.png";
import asana from "@assets/tecnologies/asana.png";
import analitics from "@assets/tecnologies/analitics.png";
import semrush from "@assets/tecnologies/semrush.png";
import hootsuite from "@assets/tecnologies/hootsuite.png";
import gpt from "@assets/tecnologies/gpt.jpg";

import landing from "@assets/solutions/landing.png";

export default {
	header: {
		title: "Marketing con Inteligencia Artificial (IA)",
		subtitle: (
			<>
				Mejora tus estrategias de marketing con inteligencia artificial
				(IA), aquí encontrarás todo lo que necesitas saber sobre cómo la
				integración de tecnología de inteligencia artificial puede
				llevar tus estrategias de marketing al siguiente nivel.
			</>
		),
		buttonText: "Quiero una auditoria gratis",
		url: Links.$contact,
	},
	intro: {
		title: (
			<>
				la inteligencia artificial en{" "}
				<Text type="span" color="context">
					marketing
				</Text>
			</>
		),
		content: (
			<>
				El{" "}
				<Text type="strong" color="context">
					marketing basado en inteligencia artificial
				</Text>{" "}
				(IA) se define como la capacidad de una máquina para aprender y
				realizar tareas que normalmente requieren inteligencia humana,
				como el análisis de datos, la toma de decisiones y la resolución
				de problemas, esto se traduce en la posibilidad de{" "}
				<Text type="strong">
					automatizar procesos, personalizar contenido y mejorar la
					experiencia del usuario
				</Text>
				.
				<br />
				<br />
				¡Aprende más sobre cómo la IA puede mejorar tus estrategias de
				marketing!
			</>
		),
		buttonText: "Quiero saber más sobre IA",
		url: Links.$contact,
		image: {
			name: "marketing + ia",
			url: ia1,
		},
	},
	tecnologies: {
		title: (
			<>
				<Text type="span" color="context">
					Tecnologias de IA
				</Text>{" "}
				que ya integramos en Digital Booting
			</>
		),
		content: (
			<>
				Nuestro equipo de expertos altamente capacitados se encuentra en
				constante evolución para ofrecer a nuestros clientes los mejores
				resultados, integrando en nuestro amplio espectro de soluciones
				digitales{" "}
				<Text type="strong" color="context">
					estrategias basadas en inteligencia artificial
				</Text>
			</>
		),
		gallery: [
			gpt,
			wordpress,
			wix,
			elementor,
			figma,
			html,
			css,
			laravel,
			vue,
			react,
			javascript,
			angular,
			miro,
			asana,
			analitics,
			semrush,
			hootsuite,
		],
	},
	whatdb: {
		title: (
			<>
				¿Cómo puede la IA{" "}
				<Text type="span" color="context">
					mejorar tus estrategias
				</Text>{" "}
				de Marketing?
			</>
		),
		content: {
			section1: {
				content: (
					<>
						La IA puede mejorar tus estrategias de marketing de
						muchas maneras, desde el análisis de datos para
						identificar patrones y oportunidades hasta la
						personalización de contenido y la optimización de
						anuncios. La integración de la IA en tus servicios de
						marketing te permitirá{" "}
						<Text type="strong" color="context">
							mejorar la eficiencia, reducir los costos y aumentar
							los ingresos
						</Text>
						.
						<br />
						<br />
						¡Aprovecha los beneficios que la IA puede aportar a tus
						estrategias de marketing!
					</>
				),
				buttonText: "Quiero conocer más beneficios",
				url: Links.$contact,
				image: {
					url: ia2,
					name: "atracción de clientes",
				},
			},
			section2: {
				title: (
					<>
						Beneficios que ofrece la{" "}
						<Text type="span" color="context">
							Inteligencia artificial
						</Text>{" "}
						en Marketing
					</>
				),
				content: (
					<>
						La integración de la inteligencia artificial en tus
						estrategias de marketing te permitirá aprovechar la
						<Text type="strong">
							automatización para ahorrar tiempo y recursos
						</Text>
						, mejorar la personalización del contenido para tus
						clientes, optimizar tus campañas publicitarias para{" "}
						<Text type="strong">
							maximizar los resultados y obtener información
							valiosa
						</Text>{" "}
						para tomar decisiones más informadas.
					</>
				),
				image: {
					url: ia3,
					name: "automatizacion IA",
				},
			},
			section3: {
				title: (
					<>
						¿Cómo integramos la IA en{" "}
						<Text type="span" color="context">
							nuestras soluciones
						</Text>
						?
					</>
				),
				content: (
					<>
						Identificamos los procesos que pueden ser automatizados
						y los datos que pueden ser analizados para mejorar
						nuestras soluciones y estrategias, integrando
						herramientas de IA innovadoras para ofrecerte los
						mejores resultados y retornos de inversión.
						<br />
						<br />
						¡Contáctanos hoy mismo y platiquemos sobre cómo podemos
						llevar a tu negocio al siguiente nivel mediante nuestras
						soluciones digitales potenciadaas con IA!
					</>
				),
				buttonText: "Quiero conversar",
				url: Links.$contact,
				image: {
					url: ia4,
					name: "metodologia design thinking",
				},
			},
		},
	},
	solutions: {
		title: (
			<>
				Nuestras soluciones que integran{" "}
				<Text type="span" color="context">
					inteligencia artificial
				</Text>
			</>
		),
		sliders: [
			{
				title: "Marketing de contenidos",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Marketing emocional + IA",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Marketing basado en IA predictiva",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Neuro-Marketing",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Marketing de IA conversacional",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Posicionamiento SEO + Analisis predictivo de IA",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Páginas Web",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
		],
	},
};
