import React from "react";
import { Links } from "@constants";
import { Text, image, Svg, Title } from "@components";
import { SubTitle } from "@modules";

import seobanner from "@assets/seobanner.svg";
import seo from "@assets/seo2.svg";
import seo1 from "@assets/seo1.svg";
import designthink from "@assets/designthink.svg";

import figma from "@assets/tecnologies/figma.png";
import html from "@assets/tecnologies/html.png";
import css from "@assets/tecnologies/css.png";
import laravel from "@assets/tecnologies/laravel.png";
import vue from "@assets/tecnologies/vue.png";
import react from "@assets/tecnologies/react.png";
import javascript from "@assets/tecnologies/javascript.png";
import angular from "@assets/tecnologies/angular.png";
import miro from "@assets/tecnologies/miro.png";
import asana from "@assets/tecnologies/asana.png";
import analitics from "@assets/tecnologies/analitics.png";
import semrush from "@assets/tecnologies/semrush.png";
import hootsuite from "@assets/tecnologies/hootsuite.png";

import landing from "@assets/solutions/landing.png";

export default {
	header: {
		title: "Agencia de SEO con Inteligencia Artificial (IA) en México",
		subtitle: (
			<>
				Integramos estrategias de posicionamiento SEO basadas en
				inteligencia artificial (IA) para colocar tu negocio en las
				primeras posiciones de los buscadores de internet.
			</>
		),
		buttonText: "Quiero una auditoria gratis",
		url: Links.$contact,
	},
	intro: {
		title: (
			<>
				Tu agencia de SEO con{" "}
				<Text type="span" color="context">
					IA
				</Text>{" "}
				en{" "}
				<Text type="span" color="context">
					méxico
				</Text>
			</>
		),
		content: (
			<>
				Nuestro objetivo en <Text type="strong">Digital Booting</Text>{" "}
				es ayudarte a crecer tu negocio en línea. Para lograrlo,
				utilizamos <Text type="strong">soluciones innovadoras</Text> de
				posicionamiento en buscadores (SEO).
				<br />
				<br />
				Aprovechando todo el potencial de la{" "}
				<Text type="strong">inteligencia artificial</Text> para que tu
				negocio o empresa aparezca en las primeras posiciones de Google.
			</>
		),
		buttonText: "Quiero una asesoria",
		url: Links.$contact,
		image: {
			name: "agencia de seo en mexico",
			url: seobanner,
		},
	},
	tecnologies: {
		title: "Tecnologias de Posicionamiento SEO",
		content: (
			<>
				Nuestro equipo de expertos altamente capacitados en diversas{" "}
				<Text type="strong" color="context">
					tecnologías de SEO
				</Text>{" "}
				son capaces de crear nuevas estrategias basadas en inteligencia
				artificial, logrando poco a poco posicionar tu empresa en los
				primeros resultados de búsqueda.
			</>
		),
		gallery: [
			figma,
			html,
			css,
			laravel,
			vue,
			react,
			javascript,
			angular,
			miro,
			asana,
			analitics,
			semrush,
			hootsuite,
		],
	},
	whatdb: {
		title: (
			<>
				¿Qué hace diferente a{" "}
				<Text type="span" color="context">
					Digital Booting
				</Text>{" "}
				de otras agencias de SEO?
			</>
		),
		content: {
			section1: {
				title: (
					<>
						Estrategias basadas en{" "}
						<Text type="span" color="context">
							Inteligencia artificial
						</Text>
					</>
				),
				content: (
					<>
						Creamos estrategias innovadoras que utilizan tecnologías
						de IA. Estas tecnologías{" "}
						<Text type="strong" color="context">
							predicen y analizan el contenido
						</Text>{" "}
						de tu sitio web y el de tu público objetivo. Así,
						optimizamos el contenido relevante que los usuarios
						buscan y{" "}
						<Text type="strong">
							mejoramos tu posición en los resultados de búsqueda
						</Text>
						.
					</>
				),
				image: {
					url: seo,
					name: "atracción de clientes",
				},
			},
			section2: {
				title: (
					<>
						Aplicamos SEO estrategico de{" "}
						<Text type="span" color="context">
							conversión
						</Text>
						!
					</>
				),
				content: (
					<>
						Trabajamos el posicionamiento SEO enfocando toda la
						estrategía en el usuario y sus necesidades.
						<br />
						<br />
						<Text type="strong" color="context">
							Te ofrecemos una auditoría gratis de SEO
						</Text>{" "}
						para que conozcas el estado actual de tu sitio web y las
						áreas de mejora en las que podemos trabajar.
					</>
				),
				buttonText: "Quiero una auditoria gratis",
				url: Links.$contact,
				image: {
					url: seo1,
					name: "seo estrategico de conversion",
				},
			},
		},
	},
	solutions: {
		title: (
			<>
				Nuestras soluciones en{" "}
				<Text type="span" color="context">
					posicionamiento SEO
				</Text>
			</>
		),
		sliders: [
			{
				title: "Auditorias basadas en IA",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "SEO on-page",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Analisis predictivo de contenidos",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Engagement SEO",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Marketing basado en IA predictiva",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Neuro-Marketing",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Marketing de IA conversacional",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
		],
	},
	// pymes: {
	// 	title: (
	// 		<>
	// 			Soluciones para{" "}
	// 			<Text type="span" color="context">
	// 				PyMEs y Emprendedores
	// 			</Text>
	// 		</>
	// 	),
	// 	cards: [
	// 		{
	// 			theme: "white",
	// 			category: "Inicial",
	// 			price: "1599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "dark",
	// 			category: "Básico",
	// 			price: "3599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"4 Bloques de contenido",
	// 				"4 Bloques de contenido",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "white",
	// 			category: "Profesional",
	// 			price: "6599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"Hasta 800 palabras",
	// 				"Hasta 800 palabras",
	// 				"Hasta 800 palabras",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "dark",
	// 			category: "Negocios",
	// 			price: "7599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 	],
	// },
	// empresas: {
	// 	title: (
	// 		<>
	// 			Soluciones para{" "}
	// 			<Text type="span" color="primary">
	// 				Startups y empresas
	// 			</Text>
	// 		</>
	// 	),
	// 	cards: [
	// 		{
	// 			theme: "white",
	// 			category: "Inicial",
	// 			price: "2599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "darkover",
	// 			category: "Básico",
	// 			price: "4599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "white",
	// 			category: "Profesional",
	// 			price: "9599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "darkover",
	// 			category: "Negocios",
	// 			price: "12599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 	],
	// },
	// mantenimiento: {
	// 	title: (
	// 		<>
	// 			Paquetes de{" "}
	// 			<Text type="span" color="context">
	// 				Mantenimiento
	// 			</Text>{" "}
	// 			SEO
	// 		</>
	// 	),
	// 	cards: [
	// 		{
	// 			theme: "white",
	// 			category: "Inicial",
	// 			price: "1599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "dark",
	// 			category: "Básico",
	// 			price: "3599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "white",
	// 			category: "Profesional",
	// 			price: "6599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "dark",
	// 			category: "Negocios",
	// 			price: "9599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 	],
	// },
};
