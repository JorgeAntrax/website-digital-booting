import React from "react";
import { Links } from "@constants";
import { Text, image, Svg, Title } from "@components";
import { SubTitle } from "@modules";

import marketing from "@assets/marketing.svg";
import socialm2 from "@assets/socialm2.jpg";
import socialm3 from "@assets/socialm3.svg";
import socialm4 from "@assets/socialm4.webp";
import neuromkt from "@assets/neuromkt.svg";

import elementor from "@assets/tecnologies/elementor.png";
import figma from "@assets/tecnologies/figma.png";
import html from "@assets/tecnologies/html.png";
import css from "@assets/tecnologies/css.png";
import laravel from "@assets/tecnologies/laravel.png";
import vue from "@assets/tecnologies/vue.png";
import react from "@assets/tecnologies/react.png";
import javascript from "@assets/tecnologies/javascript.png";
import angular from "@assets/tecnologies/angular.png";
import miro from "@assets/tecnologies/miro.png";
import asana from "@assets/tecnologies/asana.png";
import analitics from "@assets/tecnologies/analitics.png";
import semrush from "@assets/tecnologies/semrush.png";
import hootsuite from "@assets/tecnologies/hootsuite.png";

import landing from "@assets/solutions/landing.png";

export default {
	header: {
		title: "Marketing para redes sociales en méxico",
		subtitle: (
			<>
				Diseñamos y optimizamos estrategias de contenido para tus redes
				sociales, mediante soluciones innovadoras de IA para fortalecer
				tu relacion con tu publico objetivo.
			</>
		),
		buttonText: "Quiero una auditoria gratis",
		url: Links.$contact,
	},
	intro: {
		title: (
			<>
				Tu agencia de marketing para{" "}
				<Text type="span" color="context">
					redes sociales
				</Text>
			</>
		),
		content: (
			<>
				Nuestro objetivo en <Text type="strong">Digital Booting</Text>{" "}
				es ayudarte a crecer tu negocio en línea. Para lograrlo,
				utilizamos <Text type="strong">soluciones innovadoras</Text>{" "}
				como el marketing emocional, entre otras que aprovechan todo el
				potencial de la{" "}
				<Text type="strong">inteligencia artificial</Text> para aumentar
				tu presencia en redes sociales y convertir a desconocidos en
				clientes para tu empresa.
			</>
		),
		image: {
			name: "agencia de marketing en méxico",
			url: marketing,
		},
	},
	tecnologies: {
		title: (
			<>
				Tecnologias de marketing para{" "}
				<Text type="span" color="context">
					redes sociales
				</Text>
			</>
		),
		content: (
			<>
				Nuestro equipo de expertos altamente capacitados en diversas{" "}
				<Text type="strong" color="context">
					tecnologías enfocadas a redes sociales
				</Text>{" "}
				son capaces de crear nuevas estrategias basadas en inteligencia
				artificial, logrando aumentar tu engagement en redes sociales.
				Esto se traduce en ventas para tu empresa, así como{" "}
				<Text type="strong" color="context">
					retornos de inversión en publicidad,
				</Text>{" "}
				entre otras ventajas.
			</>
		),
		gallery: [
			elementor,
			figma,
			html,
			css,
			laravel,
			vue,
			react,
			javascript,
			angular,
			miro,
			asana,
			analitics,
			semrush,
			hootsuite,
		],
	},
	whatdb: {
		title: (
			<>
				¿Qué hace diferente a{" "}
				<Text type="span" color="context">
					Digital Booting
				</Text>{" "}
				de otras agencias de social media?
			</>
		),
		content: {
			section1: {
				content: (
					<>
						Aprovechamos diversas metodologías de captación social,
						como el{" "}
						<Text type="strong" color="context">
							social design thinking, neuro marketing, inbound
							marketing e ingeniería social
						</Text>{" "}
						integrando herramientas de IA creativas,
						conversacionales, etc.
						<br />
						<br />
						Nuestra misión es captar al público objetivo correcto,
						fidelizar a aquellos usuarios que se convierten en tus
						clientes mediante nuestras estrategías y finalmente{" "}
						<Text type="strong">
							fortalecer la relación entre tus clientes y tu
							negocio
						</Text>
						.
					</>
				),
				buttonText: "Quiero una asesoria gratis",
				url: Links.$contact,
				image: {
					url: socialm2,
					name: "fidelizacion de clientes",
				},
			},
			section2: {
				title: (
					<>
						Tus redes activas en{" "}
						<Text type="span" color="context">
							todo momento
						</Text>
					</>
				),
				content: (
					<>
						Nuestras estrategias de contenido estan basadas en
						tecnologias de IA y supervisadas por expertos en el
						tema, desde el primer momento cuentas con un{" "}
						<Text type="strong" color="context">
							calendario de contenidos mensual
						</Text>{" "}
						bien definido, de manera que podrás visualizar en todo
						momento que contenido será publicado en tus medios
						sociales, no dejamos descuidadas e inactivas tus redes
						sociales.
					</>
				),
				image: {
					url: socialm3,
					name: "calendario de contenido",
				},
			},
			section3: {
				title: (
					<>
						Integramos Marketing emocional +{" "}
						<Text type="span" color="context">
							inteligencia artificial
						</Text>
					</>
				),
				content: (
					<>
						Con inteligencia artificial y marketing emocional,{" "}
						<Text type="strong">
							nos ponemos en la piel del usuario
						</Text>
						. Medimos las emociones que despiertan nuestras
						estrategias en tu audiencia y encontramos la mejor
						manera de conectar con ellos.
						<br />
						<br />
						Somos una agencia de marketing y social media que piensa
						distinto al resto.
					</>
				),
				image: {
					url: socialm4,
					name: "marketing emocional + IA",
				},
			},
		},
	},
	solutions: {
		title: (
			<>
				Nuestras soluciones en{" "}
				<Text type="span" color="context">
					redes sociales
				</Text>
			</>
		),
		sliders: [
			{
				title: "Creación de contenido estrategico",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Administración de redes sociales",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Integración de IA conversacional",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Automatización de flujos de conversión",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Chatbots sociales",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Tiendas integradas",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Instagram Ads",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
			{
				title: "Facebook Ads",
				content: (
					<>
						<Text type="strong" color="primary">
							Traducimos tu inversión en prospectos cualificados
						</Text>{" "}
						dispuestos a darte su información para posteriormente
						convertirlos en clientes..
					</>
				),
				asset: landing,
				buttonText: "Quiero más información",
				url: Links.$contact,
			},
		],
	},
	// pymes: {
	// 	title: (
	// 		<>
	// 			Soluciones para{" "}
	// 			<Text type="span" color="context">
	// 				PyMEs y Emprendedores
	// 			</Text>
	// 		</>
	// 	),
	// 	cards: [
	// 		{
	// 			theme: "white",
	// 			category: "Inicial",
	// 			price: "1599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "dark",
	// 			category: "Básico",
	// 			price: "3599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"4 Bloques de contenido",
	// 				"4 Bloques de contenido",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "white",
	// 			category: "Profesional",
	// 			price: "6599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"Hasta 800 palabras",
	// 				"Hasta 800 palabras",
	// 				"Hasta 800 palabras",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "dark",
	// 			category: "Negocios",
	// 			price: "7599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 	],
	// },
	// empresas: {
	// 	title: (
	// 		<>
	// 			Soluciones para{" "}
	// 			<Text type="span" color="primary">
	// 				Startups y empresas
	// 			</Text>
	// 		</>
	// 	),
	// 	cards: [
	// 		{
	// 			theme: "white",
	// 			category: "Inicial",
	// 			price: "3599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "darkover",
	// 			category: "Básico",
	// 			price: "5599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "white",
	// 			category: "Profesional",
	// 			price: "8599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "darkover",
	// 			category: "Negocios",
	// 			price: "10599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 	],
	// },
	// mantenimiento: {
	// 	title: (
	// 		<>
	// 			Paquetes de{" "}
	// 			<Text type="span" color="context">
	// 				Mantenimiento y administración
	// 			</Text>{" "}
	// 			de redes sociales
	// 		</>
	// 	),
	// 	cards: [
	// 		{
	// 			theme: "white",
	// 			category: "Inicial",
	// 			price: "1599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "dark",
	// 			category: "Básico",
	// 			price: "3599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "white",
	// 			category: "Profesional",
	// 			price: "6599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 		{
	// 			theme: "dark",
	// 			category: "Negocios",
	// 			price: "9599",
	// 			currency: "MXN",
	// 			url: Links.$contact,
	// 			features: [
	// 				"1 UX Landing page",
	// 				"Hasta 800 palabras",
	// 				"4 Bloques de contenido",
	// 				"1 formulario de contacto",
	// 			],
	// 		},
	// 	],
	// },
};
