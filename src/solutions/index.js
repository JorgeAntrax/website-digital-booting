export { default as WebDesign } from "./webdesign";
export { default as MarketingDigital } from "./marketing";
export { default as CreacionMarca } from "./branding";
export { default as PosicionamientoSeo } from "./seo";
export { default as SocialMedia } from "./social";
export { default as InteligenciaArtificial } from "./ia";
