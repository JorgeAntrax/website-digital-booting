import {
	$black,
	$primary,
	$secondary,
	$grey,
	$tertiary,
	$context
} from "@constants/colors";

export default {
	around: "space-around",
	between: "space-between",
	start: "flex-start",
	end: "flex-end",
	primary: $primary,
	secondary: $secondary,
	tertiary: $tertiary,
	black: $black,
	grey: $grey,
	context: $context,
	light: "#f6f6f6"
};
