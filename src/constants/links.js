const Links = {
	$web: "/agencia-de-paginas-web",
	$mkt: "/agencia-marketing-digital",
	$privacy: "/aviso-de-privacidad",
	$terms: "/terminos-y-condiciones",
	$contact: "/contacto",
	$branding: "/creacion-de-marca-con-ia",
	$afiliateds: "/gana-comisiones-con-nuestro-programa-de-afiliados",
	$thanks: "/gracias-por-contactarnos",
	$homepage: "/",
	$social: "/marketing-en-redes-sociales",
	$intelligence: "/marketing-inteligencia-artificial",
	$seo: "/seo-con-inteligencia-artificial",
	$blog: "/blog-de-marketing-digital",
	$work: "/unete-a-nuestro-equipo",
	$wpLink:
		"https://wa.me/+5215548067069/?text=Quiero%20contratar%20alguno%20de%20sus%20servicios%20para%20mi%20negocio",
	$fbLink: "https://www.facebook.com/DigitalBootingOficial",
	$igLink: "https://www.instagram.com/agenciadigitalbooting",
};

export default Links;
