const $black = "#141D28";
const $primary = "#02f8c0";
const $secondary = "#02838a";
const $grey = "#43605b";
const $tertiary = "#43605b";
const $context = "#394CF8";

export { $black, $primary, $secondary, $grey, $tertiary, $context };
