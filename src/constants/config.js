const CONTEXT = "dbstore";
const BREAKPOINTS = [320, 640, 980, 1366, 1520];
const BREAKPOINTS_PREFIX = ["xs", "sm", "md", "lg", "xl"];

export { CONTEXT, BREAKPOINTS, BREAKPOINTS_PREFIX };
