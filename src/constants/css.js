import { BREAKPOINTS, BREAKPOINTS_PREFIX } from "@constants";

String.prototype.toKebabCase = function () {
	return this.replace(/([a-z0-9]|(?=[A-Z]))([A-Z])/g, "$1-$2").toLowerCase();
};

const mapDynamicProps = (props, alias = {}, values = {}) => {
	let styles = "";
	Object.keys(props).forEach((prop) => {
		const cssProp = alias[prop] || prop?.toKebabCase();

		let value =
			typeof props[prop] === "number" ? `${props[prop]}px` : props[prop];

		value = values[value] || value;

		const ArrayProps = Array.isArray(cssProp) ? [...cssProp] : [cssProp];

		ArrayProps.forEach((property) => {
			styles += `${property}: ${value};`;
		});
	});
	return styles;
};

const addResponsiveSupport = (props, alias = {}, values = {}) => {
	let styles = "";
	BREAKPOINTS.forEach((breakpoint, index) => {
		const name = BREAKPOINTS_PREFIX[index];

		const breakpointProps = Object.keys(props).filter((key) =>
			key.startsWith(name)
		);

		if (breakpointProps.length > 0) {
			styles += `@media (min-width: ${breakpoint}px) {`;
			breakpointProps.forEach((prop) => {
				const cleanProp = prop.slice(2).toLowerCase();
				const cssProp = alias[cleanProp] || cleanProp;
				let value =
					typeof props[prop] === "number"
						? `${props[prop]}px`
						: props[prop];
				value = values[value] || value;

				const ArrayProps = Array.isArray(cssProp)
					? [...cssProp]
					: [cssProp];

				ArrayProps.forEach((property) => {
					styles += `${property}: ${value};`;
				});
			});
			styles += "}";
		}
	});
	return styles;
};

const hexToRGB = (hex, alpha) => {
	hex = hex.replace(/#/g, "");
	var r = parseInt(hex.slice(0, 2), 16),
		g = parseInt(hex.slice(2, 4), 16),
		b = parseInt(hex.slice(4, 6), 16);

	if (alpha) {
		return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
	} else {
		return "rgb(" + r + ", " + g + ", " + b + ")";
	}
};

export { mapDynamicProps, addResponsiveSupport, hexToRGB };
