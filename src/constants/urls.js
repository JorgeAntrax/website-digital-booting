const DEVELOPMENT_DOMAIN = `http://127.0.0.1:8000`;

const ENV =
	process.env.GATSBY_CONFIG_ENV !== undefined
		? process.env.GATSBY_CONFIG_ENV
		: "prod";

const DOMAIN =
	process.env.GATSBY_URL_BASE !== undefined
		? process.env.GATSBY_URL_BASE
		: DEVELOPMENT_DOMAIN;

const PUBLIC_DOMAIN =
	process.env.GATSBY_PUBLIC_DOMAIN !== undefined
		? process.env.GATSBY_PUBLIC_DOMAIN
		: null;
		
const STORE_DOMAIN =
	process.env.GATSBY_STORE_DOMAIN !== undefined
		? process.env.GATSBY_STORE_DOMAIN
		: null;

const ENABLE_ENCRYPTION =
	process.env.GATSBY_ENCRYPTION_API !== undefined
		? process.env.GATSBY_ENCRYPTION_API
		: false;

const PUBLIC_STRIPE_KEY =
	process.env.GATSBY_STRIPE_PUBLIC_KEY !== undefined
		? process.env.GATSBY_STRIPE_PUBLIC_KEY
		: NULL;

const DEEPL_AUTH =
	process.env.GATSBY_DEEPL_AUTH !== undefined
		? process.env.GATSBY_DEEPL_AUTH
		: NULL;

const BASE_URL = `${DOMAIN}`;

const URLS = {
	UTM: `${BASE_URL}/utm/add`,
	SUSCRIBE: `${BASE_URL}/leads`,
	CREATE_AFILIADO: `${BASE_URL}/afiliados/registro`,
	RECENT_BLOGS: `${BASE_URL}/blogs/recents`,
	FEATURE_BLOGS: `${BASE_URL}/blogs/featured`,
	RECENT_BLOGS_BY: `${BASE_URL}/blogs/recentsBy`,
	BLOG_BY_SLUG: `${BASE_URL}/blogs/getBySlug`,
	BLOGS: `${BASE_URL}/blogs/getAll`,
	VALIDATE_AFILIATED_CODE: `${BASE_URL}/validate-afiliatedCode`,
	GET_AFFILIATED_CODE: `${BASE_URL}/afiliatedCode`,
	GET_MARKDOWN_BLOG: (slug, name) => `${STORE_DOMAIN}/blogs/${slug}/${name}.html`
};

export {
	BASE_URL,
	URLS,
	ENABLE_ENCRYPTION,
	PUBLIC_STRIPE_KEY,
	DEEPL_AUTH,
	PUBLIC_DOMAIN,
};
